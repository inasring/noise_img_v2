from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import GlobalAveragePooling2D
from keras.utils import multi_gpu_model

def make_trainable(model,trainable):
    for layer in model.layers:
        layer.trainable = trainable

def net_model(network,input_shape,output_size=None,
                weights=None,include_original_top=False,
                global_average_pooling=False,
                hidden_dense=None,train_fc_only=False):
    train_convolutional = not train_fc_only
    from keras.models import Model
    resnet50 = "ResNet50"
    vgg16    = "VGG16"
    availableNets = [resnet50,vgg16]
    if network not in availableNets:
        print("Network not available")
        raise ValueError
    if network == resnet50:
        from keras.applications import ResNet50
        applications_model = ResNet50
    if network == vgg16:
        from keras.applications import VGG16
        applications_model = VGG16

    if (output_size is not None) and include_original_top:
        print("Ignoring include_top because output_size is set")
    orig_model = applications_model(include_top=include_original_top, weights=weights,input_shape=input_shape)
    intermediate_out = orig_model.output
    make_trainable(orig_model,train_convolutional)
    #Options
    # 1.Original network: GAP=False, include_original_top=True
    # 2.GAP   > Dense   : GAP=True
    # 3.Dense > Dense
    if include_original_top:
        model = orig_model
        model_intermediate = Model(inputs=orig_model.input,outputs=intermediate_out)
        return model,model_intermediate
    if global_average_pooling:
        x = orig_model.output
        gap_out = GlobalAveragePooling2D()(x)
        intermediate_out = gap_out
        x = gap_out
        if hidden_dense is not None:
            x = Dense(hidden_dense,activation="tanh")(x)
        if output_size is not None:
            predictions = Dense(output_size)(x)
        else:
            predictions = x
    else:
        x = orig_model.output
        if (hidden_dense is not None) or (output_size is not None):
            x = Flatten()(x)
        if hidden_dense is not None:
            x = Dense(hidden_dense,activation="tanh")(x)
        if output_size is not None:
            predictions = Dense(output_size)(x)
        else:
            predictions = x
    model = Model(inputs=orig_model.input,outputs=predictions)
    model_intermediate = Model(inputs=orig_model.input,outputs=intermediate_out)
    parallel_model = multi_gpu_model(model,gpus=2)
    # return model,model_intermediate
    return parallel_model,model_intermediate

if __name__ == "__main__":
    input_shape = (240,240,3)
    model,model_intermediate = net_model(network="ResNet50",input_shape=input_shape,output_size=1,
                                        weights="imagenet",include_original_top=False,
                                        global_average_pooling=True)
    import numpy as np
    data   = np.random.random(input_shape)
    data   = data[np.newaxis]
    pred_1 = model.predict(data)
    pred_2 = model_intermediate.predict(data)
    print(pred_1.shape)
    print(pred_2.shape)
