import numpy as np
np.random.seed(1234)

from keras.layers import Concatenate,Input, Dropout
from keras.layers.convolutional import Conv2D,AveragePooling2D
from keras.layers.core import Dense, Activation,Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import GlobalAveragePooling2D
from keras.models import Model
from keras.regularizers import l2



def bnReluConvDrop(depth, kernelx, kernely,droprate=0.,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001):
    def f(net):
        net = BatchNormalization(gamma_regularizer=l2(bnL2norm),beta_regularizer=l2(bnL2norm),momentum=0.9)(net)
        net = Activation('relu')(net)
        net = Conv2D(depth,(kernelx, kernely), padding='same', kernel_initializer='he_normal', strides=stride, use_bias=False,kernel_regularizer=l2(weight_decay))(net)
        if droprate>0.:
            net=Dropout(droprate)(net)
        return net
    return f


def DropAp(pool_size=(2,2),strides=(2,2), droprate = 0.0):
    def f(net):
        if droprate != 0.:
            net = Dropout(droprate)(net)
            # net = AveragePooling2D(pool_size, strides=strides, padding='valid', dim_ordering='default')(net)
            net = AveragePooling2D(pool_size, strides=strides, padding='valid')(net)
        return net
    return f


def denseBlock_layout(net,feature_map_n_list,n_filter,droprate=0.,weight_decay=1e-4):
    layer_list=[net]
    for i in range(len(feature_map_n_list)):
        net = bnReluConvDrop(feature_map_n_list[i], 3, 3,droprate=droprate,stride=(1,1),weight_decay=weight_decay)(net)
        layer_list.append(net)
        net      = Concatenate(axis=-1)(layer_list)
        n_filter+=feature_map_n_list[i]
    return net,n_filter

def dense_net(n_dense_blocks,input_shape,feature_map_n_list,n_filter_initial=16,droprate=0.2,bnL2norm=0.0001,l2_dense_penalization=0.0001,nb_classes=10,finalActivation='softmax',summary=False):
    n_filter = n_filter_initial
    img_input = Input(shape=input_shape) #shape=(img_channels, img_rows, img_cols)
    x = Conv2D(n_filter_initial,(3, 3), padding='same', kernel_initializer='he_normal', kernel_regularizer= l2(bnL2norm),use_bias=False)(img_input)
    for i in range(n_dense_blocks-1):
        x,n_filter = denseBlock_layout(x,feature_map_n_list,n_filter,droprate=droprate)
        x = BatchNormalization(gamma_regularizer=l2(bnL2norm),beta_regularizer=l2(bnL2norm),momentum=0.9)(x)
        x = Activation('relu')(x)
        x = Conv2D(n_filter,(1, 1), padding='same', kernel_initializer='he_normal', kernel_regularizer= l2(bnL2norm),activation='linear',use_bias=False)(x)
        x = DropAp(droprate=droprate)(x)

    x,n_filter = denseBlock_layout(x,feature_map_n_list,n_filter,droprate=droprate)

    x = BatchNormalization(gamma_regularizer=l2(bnL2norm),beta_regularizer=l2(bnL2norm),momentum=0.9)(x)
    x = Activation('relu')(x)
    x = GlobalAveragePooling2D()(x)
        #x = AveragePooling2D((8, 8), strides=(1, 1))(x)

    #x = Flatten()(x)
    #preds = Dense(nb_classes, activation='linear', W_regularizer=l2(l2_dense_penalization))(x)
    preds = Dense(nb_classes, activation=finalActivation, kernel_regularizer=l2(l2_dense_penalization))(x)

    model = Model(inputs=img_input, outputs=preds)
    if summary:
        model.summary()
    return model
