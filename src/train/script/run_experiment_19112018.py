import uuid
from copy import deepcopy
from datetime import datetime
from train.experiment import experiment
from locations import directories as d
from json_utils import json_utils
from dir_utils import dir_utils as du
from evaluation import report
# Salvar número de parâmetros

if __name__ == "__main__":
    # Experiment > predictions > latex
    # image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_2018-10-25.19:04:55.npy"
    # audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_2018-10-25.19:04:55.npy"
    # dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_2018-10-25.19:04:55.json"
    # image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_2018-10-31.23:01:26.npy"
    # audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_2018-10-31.23:01:26.npy"
    # dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_2018-10-31.23:01:26.json"

    # audio_dataset_name  = d.numpyDataset+"small_audio_dataset_2018-11-05.15:54:33.npy"
    # dataset_json_name   = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.json"
    # image_dataset_name  = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.npy"
    
    audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_2018-11-22.13:56:07.npy"
    dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_2018-11-22.13:56:07.json"
    image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_2018-11-22.13:56:07.npy"



    
    dataset_json = json_utils.load_json(dataset_json_name)

    ### Dataset
    dataset_dic = deepcopy(dataset_json)
    dataset_dic["dataset_json_name"]  = dataset_json_name

    SGDOptimizer  = "SGD"
    epoch_schedule = [30,50,70]
    lr_list = [0.01,0.001,0.0001,0.0001]
    lr_list_fc = [0.0001,0.00001,0.000001,0.000001]
    # epochs  = 5
    epochs  = 100
    # batch_size = 4 ## MUDAR PARA 32 QUANDO RODAR NA FEBE
    batch_size = 32
    train_id = str(uuid.uuid4()) #Use the same for a single experiment
    experiment_name = "sgd_adam_vgg_resnet_train_only_train_all"
    string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')

    ##### CONFIGURATION 1
    ### Network
    network_config_dic = {}
    network_config_dic["network"] = "ResNet50"
    network_config_dic["weights"] = "imagenet"
    network_config_dic["include_original_top"] = False
    network_config_dic["global_average_pooling"] = True
    network_config_dic["hidden_dense"] = 128
    network_config_dic["train_fc_only"]  = True #Network or traning config?
    # newtwork_config_dic["input_shape"] = input_shape ##Infer from input
    # newtwork_config_dic["output_size"] = output_size ##Infer from target

    ### Training
    training_config_dic = {}
    training_config_dic["n_train_videos"] = 3
    training_config_dic["optimizer"]      = SGDOptimizer
    training_config_dic["lr_list"]        = lr_list_fc
    training_config_dic["epoch_schedule"] = epoch_schedule
    training_config_dic["epochs"]         = epochs
    training_config_dic["batch_size"]     = batch_size
    training_config_dic["train_id"]       = train_id
    training_config_dic["experiment_name"]= experiment_name
    training_config_dic["string_date"]    = string_date
    training_config_dic["model_save_path"]  = "../model/" #Network or traning config?
    du.make_dir(training_config_dic["model_save_path"])
    model_unique_name = str(uuid.uuid4())
    training_config_dic["model_save_name"]  = "noiseimg_"+training_config_dic["optimizer"]\
                                                +"_"+network_config_dic["network"]+"_"\
                                                +"_"+model_unique_name+"_"\
                                                +string_date+".hdf5"
    training_config_dic["optimizer_config"]  = {'lr':0.01,'momentum':0.9,
                                                'decay':0.0,'nesterov':True}
    training_config_dic["loss"]    = "mean_squared_error"
    training_config_dic["metrics"] = ["mse"]

    ##Dataset cut position
    train_samples = dataset_dic["n_video_samples"][:training_config_dic["n_train_videos"]]
    cut = sum(train_samples)
    dataset_dic["cut"] = cut
    print("cut=",cut)

    ##### CONFIGURATION 2
    training_config_dic_2 = deepcopy(training_config_dic)
    network_config_dic_2  = deepcopy(network_config_dic)
    network_config_dic_2["network"] = "VGG16" ##Mudar chamada da classe VGG16?
    model_unique_name = str(uuid.uuid4())
    training_config_dic_2["model_save_name"]  = "noiseimg_"+training_config_dic_2["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_2["network"]+"_"\
                                                +string_date+".hdf5"

    ################# Adam optimizer
    ##### CONFIGURATION 3
    training_config_dic_3 = deepcopy(training_config_dic)
    network_config_dic_3  = deepcopy(network_config_dic)
    network_config_dic_3["network"] = "ResNet50" ##Mudar chamada da classe VGG16?
    training_config_dic_3["optimizer"]  = "Adam"
    training_config_dic_3["optimizer_config"]  = {'lr':0.001, 'beta_1':0.9,
                                                    'beta_2':0.999, 'epsilon':None,
                                                     'decay':0.0, 'amsgrad':False}
    model_unique_name = str(uuid.uuid4())
    training_config_dic_3["model_save_name"]  = "noiseimg_"+training_config_dic_3["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_3["network"]+"_"\
                                                +string_date+".hdf5"
    ##### CONFIGURATION 4
    training_config_dic_4 = deepcopy(training_config_dic)
    network_config_dic_4  = deepcopy(network_config_dic)
    network_config_dic_4["network"] = "VGG16" ##Mudar chamada da classe VGG16?
    model_unique_name = str(uuid.uuid4())
    training_config_dic_4["optimizer"]  = "Adam"
    training_config_dic_4["optimizer_config"]  = {'lr':0.001, 'beta_1':0.9,
                                                    'beta_2':0.999, 'epsilon':None,
                                                     'decay':0.0, 'amsgrad':False}
    training_config_dic_4["model_save_name"]  = "noiseimg_"+training_config_dic_4["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_4["network"]+"_"\
                                                +string_date+".hdf5"

    ########################### Train all
    ##### CONFIGURATION 5
    training_config_dic_5 = deepcopy(training_config_dic)
    network_config_dic_5  = deepcopy(network_config_dic)
    training_config_dic_5["lr_list"] = lr_list
    network_config_dic_5["train_fc_only"]  = False
    network_config_dic_5["weights"]  = None
    model_unique_name = str(uuid.uuid4())
    training_config_dic_5["model_save_name"]  = "noiseimg_"+training_config_dic_5["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_5["network"]+"_"\
                                                +string_date+".hdf5"
    ##### CONFIGURATION 6
    training_config_dic_6 = deepcopy(training_config_dic_2)
    network_config_dic_6  = deepcopy(network_config_dic_2)
    training_config_dic_6["lr_list"] = lr_list
    network_config_dic_6["train_fc_only"]  = False
    network_config_dic_6["weights"]  = None
    model_unique_name = str(uuid.uuid4())
    training_config_dic_6["model_save_name"]  = "noiseimg_"+training_config_dic_6["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_6["network"]+"_"\
                                                +string_date+".hdf5"
    ##### CONFIGURATION 7
    training_config_dic_7 = deepcopy(training_config_dic_3)
    network_config_dic_7  = deepcopy(network_config_dic_3)
    training_config_dic_7["lr_list"] = lr_list
    network_config_dic_7["train_fc_only"]  = False
    network_config_dic_7["weights"]  = None
    model_unique_name = str(uuid.uuid4())
    training_config_dic_7["model_save_name"]  = "noiseimg_"+training_config_dic_7["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_7["network"]+"_"\
                                                +string_date+".hdf5"

    ##### CONFIGURATION 8
    training_config_dic_8 = deepcopy(training_config_dic_4)
    network_config_dic_8  = deepcopy(network_config_dic_4)
    training_config_dic_8["lr_list"] = lr_list
    network_config_dic_8["train_fc_only"]  = False
    network_config_dic_8["weights"]  = None
    model_unique_name = str(uuid.uuid4())
    training_config_dic_8["model_save_name"]  = "noiseimg_"+training_config_dic_8["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_8["network"]+"_"\
                                                +string_date+".hdf5"

    ##### CONFIGURATION 9
    ### Test higher number of hidden neurons
    ### ResNet50
    training_config_dic_9 = deepcopy(training_config_dic)
    network_config_dic_9  = deepcopy(network_config_dic)
    network_config_dic_9["hidden_dense"] = 256
    model_unique_name = str(uuid.uuid4())
    training_config_dic_9["model_save_name"]  = "noiseimg_"+training_config_dic_9["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_9["network"]+"_"\
                                                +string_date+".hdf5"
    ### VGG16
    training_config_dic_10 = deepcopy(training_config_dic)
    network_config_dic_10  = deepcopy(network_config_dic)
    network_config_dic_10["network"] = "VGG16"
    network_config_dic_10["hidden_dense"] = 256
    model_unique_name = str(uuid.uuid4())
    training_config_dic_10["model_save_name"]  = "noiseimg_"+training_config_dic_10["optimizer"]\
                                                +"_"+model_unique_name+"_"\
                                                +"_"+network_config_dic_10["network"]+"_"\
                                                +string_date+".hdf5"


    experiment_log_name = training_config_dic["model_save_path"]\
                            +experiment_name+"_"+train_id+".json"

    ### Call
    network_config_list  = [network_config_dic,
                            network_config_dic_2,
                            network_config_dic_3,
                            network_config_dic_4,
                            network_config_dic_5,
                            network_config_dic_6,
                            network_config_dic_7,
                            network_config_dic_8,
                            network_config_dic_9,
                            network_config_dic_10]
    training_config_list = [training_config_dic,
                            training_config_dic_2,
                            training_config_dic_3,
                            training_config_dic_4,
                            training_config_dic_5,
                            training_config_dic_6,
                            training_config_dic_7,
                            training_config_dic_8,
                            training_config_dic_9,
                            training_config_dic_10]

    experiment(dataset_dic,
                training_config_list,
                network_config_list,
                experiment_log_name,
                predict_batch_size=4)
    ### Gen Latex
    summary_rename = {"Global Average Pooling":"GAP",
                        "Train FC only":"T_FC_O"}
    summary_columns=["Network","Optimizer",
                    "Global Average Pooling","Train FC only",
                    "FC","param_count","epochs",
                    "min_loss","min_val_loss"]
    save_name = "experiment_19112018.tex"
    save_figure_dir  = d.latex_19112018_figures
    du.make_dir(d.latex_19112018)
    du.make_dir(d.latex_19112018_figures)
    report_tables = report.report_tables_figures(experiment_log_name,
                            summary_rename,
                            summary_columns,
                            figure_dir=save_figure_dir,
                            save_fig=True)
    report.report_latex(report_tables,
                    figure_dir=save_figure_dir,
                    latex_dir=d.latex_19112018,
                    save_name=save_name,
                    save_tex=True)
