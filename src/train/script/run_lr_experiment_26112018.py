import uuid
import os
from copy import deepcopy
from datetime import datetime
import numpy as np
import pandas as pd
from train.experiment import experiment
from locations import directories as d
from json_utils import json_utils
from dir_utils import dir_utils as du
from evaluation import report

if __name__ == "__main__":

    audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_2018-11-22.13:56:07.npy"
    dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_2018-11-22.13:56:07.json"
    image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_2018-11-22.13:56:07.npy"

    save_name = "experiment_lr_26112018"
    save_figure_dir  = d.latex_26112018_figures
    latex_dir = d.latex_26112018


    dataset_json = json_utils.load_json(dataset_json_name)

    ### Dataset
    dataset_dic = deepcopy(dataset_json)
    dataset_dic["dataset_json_name"]  = dataset_json_name

    lr_array = np.array([0.1,0.1])
    SGDOptimizer  = "SGD"
    epoch_schedule = [30]
    epochs  = 15
    batch_size = 32
    all_summary_df = pd.DataFrame()
    train_id = str(uuid.uuid4()) #Use the same for a single experiment

    for experiment_index in range(3):
        experiment_name = "multiple_lr"
        save_name_many = save_name+"_"+str(experiment_index)+".tex"
        string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')

        ##### CONFIGURATION 1
        ### Network
        network_config_dic = {}
        network_config_dic["network"] = "ResNet50"
        network_config_dic["weights"] = "imagenet"
        network_config_dic["include_original_top"] = False
        network_config_dic["global_average_pooling"] = True
        network_config_dic["hidden_dense"] = 128
        network_config_dic["train_fc_only"]  = True #Network or traning config?

        ### Training
        training_config_dic = {}
        training_config_dic["n_train_videos"] = 3
        training_config_dic["optimizer"]      = SGDOptimizer
        training_config_dic["lr_list"]        = list(lr_array)
        training_config_dic["epoch_schedule"] = epoch_schedule
        training_config_dic["epochs"]         = epochs
        training_config_dic["batch_size"]     = batch_size
        training_config_dic["train_id"]       = train_id
        training_config_dic["experiment_name"]= experiment_name
        training_config_dic["string_date"]    = string_date
        training_config_dic["model_save_path"]  = "../model/" #Network or traning config?
        du.make_dir(training_config_dic["model_save_path"])
        model_unique_name = str(uuid.uuid4())
        training_config_dic["model_save_name"]  = "noiseimg_"+training_config_dic["optimizer"]\
                                                    +"_"+network_config_dic["network"]+"_"\
                                                    +"_"+model_unique_name+"_"\
                                                    +string_date+".hdf5"
        training_config_dic["optimizer_config"]  = {'lr':0.01,'momentum':0.9,
                                                    'decay':0.0,'nesterov':True}
        training_config_dic["loss"]    = "mean_squared_error"
        training_config_dic["metrics"] = ["mse"]

        ##Dataset cut position
        train_samples = dataset_dic["n_video_samples"][:training_config_dic["n_train_videos"]]
        cut = sum(train_samples)
        dataset_dic["cut"] = cut
        print("cut=",cut)

        ##### CONFIGURATION 2
        training_config_dic_2 = deepcopy(training_config_dic)
        network_config_dic_2  = deepcopy(network_config_dic)
        network_config_dic_2["network"] = "VGG16" ##Mudar chamada da classe VGG16?
        training_config_dic_2["lr_list"]= list(lr_array)
        model_unique_name = str(uuid.uuid4())
        training_config_dic_2["model_save_name"]  = "noiseimg_"+training_config_dic_2["optimizer"]\
                                                    +"_"+model_unique_name+"_"\
                                                    +"_"+network_config_dic_2["network"]+"_"\
                                                    +string_date+".hdf5"

        ################# Adam optimizer
        ##### CONFIGURATION 3
        training_config_dic_3 = deepcopy(training_config_dic)
        network_config_dic_3  = deepcopy(network_config_dic)
        network_config_dic_3["network"] = "ResNet50" ##Mudar chamada da classe VGG16?
        training_config_dic_3["optimizer"]  = "Adam"
        training_config_dic_3["optimizer_config"]  = {'lr':0.001, 'beta_1':0.9,
                                                        'beta_2':0.999, 'epsilon':None,
                                                         'decay':0.0, 'amsgrad':False}
        model_unique_name = str(uuid.uuid4())
        training_config_dic_3["model_save_name"]  = "noiseimg_"+training_config_dic_3["optimizer"]\
                                                    +"_"+model_unique_name+"_"\
                                                    +"_"+network_config_dic_3["network"]+"_"\
                                                    +string_date+".hdf5"
        ##### CONFIGURATION 4
        training_config_dic_4 = deepcopy(training_config_dic)
        network_config_dic_4  = deepcopy(network_config_dic)
        network_config_dic_4["network"] = "VGG16" ##Mudar chamada da classe VGG16?
        model_unique_name = str(uuid.uuid4())
        training_config_dic_4["optimizer"]  = "Adam"
        training_config_dic_4["optimizer_config"]  = {'lr':0.001, 'beta_1':0.9,
                                                        'beta_2':0.999, 'epsilon':None,
                                                         'decay':0.0, 'amsgrad':False}
        training_config_dic_4["model_save_name"]  = "noiseimg_"+training_config_dic_4["optimizer"]\
                                                    +"_"+model_unique_name+"_"\
                                                    +"_"+network_config_dic_4["network"]+"_"\
                                                    +string_date+".hdf5"


        experiment_log_name = training_config_dic["model_save_path"]\
                                +experiment_name+"_"+train_id+"_"\
                                +string_date+"_"+str(experiment_index)+".json"

        ### Call
        network_config_list  = [network_config_dic,
                                network_config_dic_2,
                                network_config_dic_3,
                                network_config_dic_4]
        training_config_list = [training_config_dic,
                                training_config_dic_2,
                                training_config_dic_3,
                                training_config_dic_4]

        network_config_list_copy  = deepcopy(network_config_list)
        training_config_list_copy = deepcopy(training_config_list)
        for it,network_config in enumerate(network_config_list_copy):
            network_config["global_average_pooling"]=False
            training_config_list_copy[it]["model_save_name"]=\
                        report.make_name(training_config_list_copy[it]["optimizer"],
                                        network_config["network"],
                                        string_date)
        network_config_list.extend(network_config_list_copy)
        training_config_list.extend(training_config_list_copy)
        experiment(dataset_dic,
                    training_config_list,
                    network_config_list,
                    experiment_log_name,
                    predict_batch_size=4)
        ### Gen Latex
        summary_rename = {"Global Average Pooling":"GAP",
                            "Train FC only":"T_FC","epochs":"Ep",
                            "Optimizer":"Opt","minimum_loss":"t_loss",
                            "minimum_val_loss":"v_loss"}
        summary_columns=["Network","Optimizer",
                        "Global Average Pooling","Train FC only",
                        "FC","lr_list","params","epochs",
                        "minimum_loss","minimum_val_loss",
                        "v_MSE","v_MAE"]
        du.make_dir(latex_dir)
        du.make_dir(save_figure_dir)

        report_tables = report.report_tables_figures(experiment_log_name,
                                summary_rename,
                                summary_columns,
                                figure_dir=save_figure_dir,
                                save_fig=True)
        report.report_latex(report_tables,
                        figure_dir=save_figure_dir,
                        latex_dir=latex_dir,
                        save_name=save_name_many,
                        save_tex=True,
                        add_summary=False)
        all_summary_df = all_summary_df.append(report_tables["summary"],ignore_index=True)
        lr_array /=10
    all_summary_df_str= all_summary_df.to_latex(index=True)
    summary_save_name="summary.tex"
    fname_full    = os.path.join(os.sep,latex_dir,summary_save_name)
    du.save_text(fname_full,all_summary_df_str)
