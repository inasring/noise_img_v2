import uuid
from copy import deepcopy
from datetime import datetime
from train.experiment import experiment
from locations import directories as d
from json_utils import json_utils
from dir_utils import dir_utils as du
from evaluation import report

if __name__ == "__main__":
    # Experiment > predictions > latex

    # image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_2018-10-25.19:04:55.npy"
    # audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_2018-10-25.19:04:55.npy"
    # dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_2018-10-25.19:04:55.json"
    # image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_2018-10-31.23:01:26.npy"
    # audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_2018-10-31.23:01:26.npy"
    # dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_2018-10-31.23:01:26.json"

    audio_dataset_name  = d.numpyDataset+"small_audio_dataset_2018-11-05.15:54:33.npy"
    dataset_json_name   = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.json"
    image_dataset_name  = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.npy"

    dataset_json = json_utils.load_json(dataset_json_name)

    ### Dataset
    dataset_dic = deepcopy(dataset_json)
    dataset_dic["dataset_json_name"]  = dataset_json_name

    SGDOptimizer  = "SGD"
    epoch_schedule = [15,25,45]
    # lr_list = [0.01,0.001,0.0001,0.0001]
    lr_list = [0.0001,0.00001,0.000001,0.000001]
    # epochs  = 50
    epochs  = 3
    batch_size = 16
    train_id = str(uuid.uuid4()) #Use the same for a single experiment
    experiment_name = "test_multi_dataset_version"
    string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')

    ### Network
    network_config_dic = {}
    network_config_dic["network"] = "ResNet50"
    network_config_dic["weights"] = "imagenet"
    network_config_dic["include_original_top"] = False
    network_config_dic["global_average_pooling"] = True
    network_config_dic["hidden_dense"] = 128
    network_config_dic["train_fc_only"]  = True #Network or traning config?
    # newtwork_config_dic["input_shape"] = input_shape ##Infer from input
    # newtwork_config_dic["output_size"] = output_size ##Infer from target

    ### Training
    training_config_dic = {}
    training_config_dic["n_train_videos"] = 3
    training_config_dic["optimizer"]      = SGDOptimizer
    training_config_dic["lr_list"]        = lr_list
    training_config_dic["epoch_schedule"] = epoch_schedule
    training_config_dic["epochs"]         = epochs
    training_config_dic["batch_size"]     = batch_size
    training_config_dic["train_id"]       = train_id
    training_config_dic["experiment_name"]= experiment_name
    training_config_dic["string_date"]    = string_date
    training_config_dic["model_save_path"]  = "../model/" #Network or traning config?
    du.make_dir(training_config_dic["model_save_path"])
    training_config_dic["model_save_name"]  = "noiseimg_"+training_config_dic["optimizer"]\
                                                +"_"+network_config_dic["network"]+"_"\
                                                +string_date+".hdf5"
    training_config_dic["optimizer_config"]  = {'lr':0.01,'momentum':0.9,
                                                'decay':0.0,'nesterov':True}
    training_config_dic["loss"]    = "mean_squared_error"
    training_config_dic["metrics"] = ["mse"]

    ##Dataset cut position
    train_samples = dataset_dic["n_video_samples"][:training_config_dic["n_train_videos"]]
    cut = sum(train_samples)
    dataset_dic["cut"] = cut
    print("cut=",cut)

    training_config_dic_2 = deepcopy(training_config_dic)
    network_config_dic_2  = deepcopy(network_config_dic)
    network_config_dic_2["network"] = "VGG16" ##Mudar chamada da classe VGG16?
    training_config_dic_2["model_save_name"]  = "noiseimg_"+training_config_dic_2["optimizer"]\
                                                +"_"+network_config_dic_2["network"]+"_"\
                                                +string_date+".hdf5"

    experiment_log_name = training_config_dic["model_save_path"]\
                            +experiment_name+"_"+train_id+".json"

    ### Call
    network_config_list  = [network_config_dic,network_config_dic_2]
    training_config_list = [training_config_dic,training_config_dic_2]
    experiment(dataset_dic,
                training_config_list,
                network_config_list,
                experiment_log_name)
    ### Gen Latex
    summary_rename = {"Global Average Pooling":"GAP",
                        "Train FC only":"T_FC","epochs":"Ep",
                        "Optimizer":"Opt","minimum_loss":"t_loss",
                        "minimum_val_loss":"v_loss"}
    summary_columns=["Network","Optimizer",
                    "Global Average Pooling","Train FC only",
                    "FC","params","epochs",
                    "minimum_loss","minimum_val_loss",
                    "v_MSE","v_MAE"]
    save_name = "example_fullgen_function.tex"
    save_figure_dir  = d.latex_test_figures

    report_tables = report.report_tables_figures(experiment_log_name,
                            summary_rename,
                            summary_columns,
                            figure_dir=save_figure_dir,
                            save_fig=True)
    report.report_latex(report_tables,
                    figure_dir=save_figure_dir,
                    latex_dir=d.latex_test,
                    save_name=save_name,
                    save_tex=True)
