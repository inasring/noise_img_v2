import uuid
import os
from copy import deepcopy
from datetime import datetime
import numpy as np
import pandas as pd
import telepot
from train.experiment import experiment
from locations import directories as d
from json_utils import json_utils
from dir_utils import dir_utils as du
from evaluation import report
from telegram import bot_utils


if __name__ == "__main__":
    #  3 Mudanças pra treino real:
    #      Mudar dataset pro dataset real
    #      Mudar epoch pra 70
    #      Comentar linha:
    #           network_config_list = [network_config_list[0]]
    #           training_config_list= [training_config_list[0]]

    # Small Dataset
    # audio_dataset_name  = d.numpyDataset+"small_audio_dataset_2018-11-05.15:54:33.npy"
    # dataset_json_name   = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.json"
    # image_dataset_name  = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.npy"

    # Real Dataset
    audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_f_0_b_300_2019-04-10.16:43:17.npy"
    dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_f_0_b_300_2019-04-10.16:43:17.json"
    image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_f_0_b_300_2019-04-10.16:43:17.npy"
    message = "Consertado preprocessamento. Treinamendo pra comparar como a tforward=0 afeta as predicoes"

    clipnorm = 0.1
    save_name = "experiment_lr_09122018_v1_clipnorm_"+str(clipnorm)+"_repetition_loadmodel"
    save_figure_dir  = d.latex_09122018_figures
    latex_dir = d.latex_09122018


    dataset_json = json_utils.load_json(dataset_json_name)

    ### Dataset
    dataset_dic = deepcopy(dataset_json)
    dataset_dic["dataset_json_name"]  = dataset_json_name

    lr_array = np.array([0.1,0.03,0.03*0.3 ])
    SGDOptimizer  = "SGD"
    epoch_schedule = [30,50]
    epochs  = 70
    batch_size = 32
    all_summary_df = pd.DataFrame()
    train_id = str(uuid.uuid4()) #Use the same for a single experiment
    latex_filenames = []
    for experiment_index in range(1):
        experiment_name = "multiple_lr"
        save_name_many = save_name+"_"+str(experiment_index)+".tex"
        save_name_many_full = os.path.join(os.sep,latex_dir,save_name_many)
        latex_filenames.append(save_name_many_full)
        string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')

        ##### CONFIGURATION 1
        ### Network
        network_config_dic = {}
        network_config_dic["network"] = "ResNet50"
        network_config_dic["weights"] = "imagenet"
        network_config_dic["include_original_top"] = False
        network_config_dic["global_average_pooling"] = True
        network_config_dic["hidden_dense"] = 128
        network_config_dic["train_fc_only"]  = True #Network or traning config?

        ### Training
        training_config_dic = {}
        training_config_dic["n_train_videos"] = 3
        training_config_dic["optimizer"]      = SGDOptimizer
        training_config_dic["lr_list"]        = list(lr_array)
        training_config_dic["epoch_schedule"] = epoch_schedule
        training_config_dic["epochs"]         = epochs
        training_config_dic["batch_size"]     = batch_size
        training_config_dic["train_id"]       = train_id
        training_config_dic["experiment_name"]= experiment_name
        training_config_dic["string_date"]    = string_date
        training_config_dic["model_save_path"]  = "../model/" #Network or traning config?
        du.make_dir(training_config_dic["model_save_path"])
        model_unique_name = str(uuid.uuid4())
        training_config_dic["model_id"] = model_unique_name
        training_config_dic["model_save_name"]  = "noiseimg_"+training_config_dic["optimizer"]\
                                                    +"_"+network_config_dic["network"]+"_"\
                                                    +"_"+model_unique_name+"_"\
                                                    +string_date+".hdf5"
        training_config_dic["optimizer_config"]  = {'lr':0.01,'momentum':0.9,
                                                    'decay':0.0,'nesterov':True,'clipnorm':clipnorm}
        training_config_dic["loss"]    = "mean_squared_error"
        training_config_dic["metrics"] = ["mse"]

        ##Dataset cut position
        train_samples = dataset_dic["n_video_samples"][:training_config_dic["n_train_videos"]]
        cut = sum(train_samples)
        dataset_dic["cut"] = cut
        print("cut=",cut)

        ##### CONFIGURATION 2
        training_config_dic_2 = deepcopy(training_config_dic)
        network_config_dic_2  = deepcopy(network_config_dic)
        network_config_dic_2["network"] = "VGG16" ##Mudar chamada da classe VGG16?
        training_config_dic_2["lr_list"]= list(lr_array)
        model_unique_name = str(uuid.uuid4())
        training_config_dic_2["model_id"] = model_unique_name
        training_config_dic_2["model_save_name"]  = "noiseimg_"+training_config_dic_2["optimizer"]\
                                                    +"_"+model_unique_name+"_"\
                                                    +"_"+network_config_dic_2["network"]+"_"\
                                                    +string_date+".hdf5"

        ################# Adam optimizer
        ##### CONFIGURATION 3
        training_config_dic_3 = deepcopy(training_config_dic)
        network_config_dic_3  = deepcopy(network_config_dic)
        network_config_dic_3["network"] = "ResNet50" ##Mudar chamada da classe VGG16?
        training_config_dic_3["optimizer"]  = "Adam"
        training_config_dic_3["optimizer_config"]  = {'lr':0.001, 'beta_1':0.9,
                                                        'beta_2':0.999, 'epsilon':None,
                                                         'decay':0.0, 'amsgrad':False,'clipnorm':clipnorm}
        model_unique_name = str(uuid.uuid4())
        training_config_dic_3["model_id"] = model_unique_name
        training_config_dic_3["model_save_name"]  = "noiseimg_"+training_config_dic_3["optimizer"]\
                                                    +"_"+model_unique_name+"_"\
                                                    +"_"+network_config_dic_3["network"]+"_"\
                                                    +string_date+".hdf5"
        ##### CONFIGURATION 4
        training_config_dic_4 = deepcopy(training_config_dic)
        network_config_dic_4  = deepcopy(network_config_dic)
        network_config_dic_4["network"] = "VGG16" ##Mudar chamada da classe VGG16?
        model_unique_name = str(uuid.uuid4())
        training_config_dic_4["optimizer"]  = "Adam"
        training_config_dic_4["optimizer_config"]  = {'lr':0.001, 'beta_1':0.9,
                                                        'beta_2':0.999, 'epsilon':None,
                                                         'decay':0.0, 'amsgrad':False,'clipnorm':clipnorm}
        training_config_dic_4["model_id"] = model_unique_name
        training_config_dic_4["model_save_name"]  = "noiseimg_"+training_config_dic_4["optimizer"]\
                                                    +"_"+model_unique_name+"_"\
                                                    +"_"+network_config_dic_4["network"]+"_"\
                                                    +string_date+".hdf5"


        experiment_log_name = training_config_dic["model_save_path"]\
                        +save_name+"_"+train_id+"_"\
                        +string_date+"_"+str(experiment_index)+".json"                                

        ### Call
        network_config_list  = [network_config_dic,
                                network_config_dic_2,
                                network_config_dic_3,
                                network_config_dic_4]
        training_config_list = [training_config_dic,
                                training_config_dic_2,
                                training_config_dic_3,
                                training_config_dic_4]

        network_config_list_copy  = deepcopy(network_config_list)
        training_config_list_copy = deepcopy(training_config_list)
        for it,network_config in enumerate(network_config_list_copy):
            network_config["global_average_pooling"]=False
            model_unique_name = str(uuid.uuid4())
            training_config_list_copy[it]["model_id"] = model_unique_name
            training_config_list_copy[it]["model_save_name"]=\
                        report.make_name(training_config_list_copy[it]["optimizer"],
                                        network_config["network"],
                                        string_date,
                                        model_unique_name)
        network_config_list.extend(network_config_list_copy)
        training_config_list.extend(training_config_list_copy)

        network_config_list_copy  = deepcopy(network_config_list)
        training_config_list_copy = deepcopy(training_config_list)
        for it,network_config in enumerate(network_config_list_copy):
            network_config["hidden_dense"]=None
            model_unique_name = str(uuid.uuid4())
            training_config_list_copy[it]["model_id"] = model_unique_name
            training_config_list_copy[it]["model_save_name"]=\
                        report.make_name(training_config_list_copy[it]["optimizer"],
                                        network_config["network"],
                                        string_date,
                                        model_unique_name)
        network_config_list.extend(network_config_list_copy)
        training_config_list.extend(training_config_list_copy)




        network_config_list_copy  = deepcopy(network_config_list)
        training_config_list_copy = deepcopy(training_config_list)
        for lr_iter in range(3):
            lr_array/=10.
            for it,network_config in enumerate(network_config_list_copy):
                training_config_list_copy[it]["lr_list"]= list(lr_array)
                model_unique_name = str(uuid.uuid4())
                training_config_list_copy[it]["model_id"] = model_unique_name
                training_config_list_copy[it]["model_save_name"]=\
                            report.make_name(training_config_list_copy[it]["optimizer"],
                                            network_config["network"],
                                            string_date,
                                            model_unique_name)
            network_config_list.extend(deepcopy(network_config_list_copy))
            training_config_list.extend(deepcopy(training_config_list_copy))

        # Mock experiment
        # network_config_list = [network_config_list[0]]
        # training_config_list= [training_config_list[0]]

        experiment(dataset_dic,
                    training_config_list,
                    network_config_list,
                    experiment_log_name,
                    predict_batch_size=4,
                    message=message)
        ### Gen uuid
        summary_rename = {"Global Average Pooling":"GAP",
                            "Train FC only":"T_FC","epochs":"Ep",
                            "Optimizer":"Opt","minimum_loss":"t_loss",
                            "minimum_val_loss":"v_loss"}
        summary_columns=["Network","Optimizer",
                        "Global Average Pooling","Train FC only",
                        "FC","lr_list","params","epochs",
                        "minimum_loss","minimum_val_loss",
                        "v_MSE","v_MAE"]
        du.make_dir(latex_dir)
        du.make_dir(save_figure_dir)
        print("Using experiment log file:", experiment_log_name)
        report_tables = report.report_tables_figures(experiment_log_name,
                                summary_rename,
                                summary_columns,
                                figure_dir=save_figure_dir,
                                max_loss=1e3,
                                save_fig=True)
        report.report_latex(report_tables,
                        figure_dir=save_figure_dir,
                        latex_dir=latex_dir,
                        save_name=save_name_many,
                        save_tex=True,
                        add_summary=False)
        all_summary_df = all_summary_df.append(report_tables["summary"],ignore_index=True)
        lr_array /=10

    all_summary_df_str= all_summary_df.to_latex(index=True)
    adjust_begin = '\\begin{adjustwidth}{-4cm}{}\n'
    adjust_end   = '\\end{adjustwidth}'
    all_summary_df_str = report.encapsulate(all_summary_df_str,adjust_begin,adjust_end)
    summary_save_name="summary.tex"
    summary_fname_full    = os.path.join(os.sep,latex_dir,summary_save_name)
    latex_filenames.append(summary_fname_full)
    du.save_text(summary_fname_full,all_summary_df_str)

    report_name     = "report"+"_"+string_date
    report_tex_name= report_name+".tex"
    report_pdf_name= report_name+".pdf"
    report_fname_full    = os.path.join(os.sep,latex_dir,report_tex_name)
    report_fname_full_pdf= os.path.join(os.sep,latex_dir,report_pdf_name)
    tex_main_report = report.main_report(latex_filenames,latex_dir)
    du.save_text(report_fname_full,tex_main_report)
    report_fname_full=os.path.realpath(report_fname_full)
    report_fname_full_pdf=os.path.realpath(report_fname_full_pdf)
    latex_dir_real   = os.path.realpath(latex_dir)
    report.pdflatex(report_fname_full,output_directory=latex_dir_real,verbose = True)


    ##Telegram Bot
    token = bot_utils.read_file(d.token_fname)
    myid = int(bot_utils.read_file(d.myid_fname))
    bot = telepot.Bot(token)
    bot_utils.retry_sendDocument(bot,myid,report_fname_full_pdf,
                                wait_time=180,retries=1000,ftype='pdf')
