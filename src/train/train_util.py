import importlib

def lr_schedule_maker(lr_list,epoch_schedule,verbose=False):
    def lr_schedule_func(epoch):
        inSchedule = False
        for i in range(len(epoch_schedule)):
            if epoch < epoch_schedule[i]:
                rate = lr_list[i]
                inSchedule = True
                break
        if not inSchedule:
            rate = lr_list[-1]
        if verbose:
            print("lr =",rate)
        return rate
    return lr_schedule_func

def get_optimizer(optimizer_name,kwargs):
    keras_optim = importlib.import_module('keras.optimizers')
    optimizer = getattr(keras_optim,optimizer_name)
    return optimizer(**kwargs)

if __name__ == "__main__":
    optimizer = "SGD"
    kwargs    = {'lr':0.01,'momentum':0.0,'decay':0.0,
                'nesterov':False}
    opt       = get_optimizer(optimizer,**kwargs)
    print(opt)
    optimizer = "Adam"
    kwargs    = {'lr':0.001, 'beta_1':0.9, 'beta_2':0.999,
                'epsilon':None, 'decay':0.0, 'amsgrad':False}
    opt       = get_optimizer(optimizer,**kwargs)
    print(opt)
