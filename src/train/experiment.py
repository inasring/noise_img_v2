import numpy as np
import tensorflow as tf
import json
from copy import deepcopy
from keras.callbacks import LearningRateScheduler,ModelCheckpoint,TerminateOnNaN
from keras.models import load_model
import keras.backend as K
from model.network_models import net_model
from train.train_util import lr_schedule_maker,get_optimizer
from json_utils import json_utils as jutils

def experiment(dataset_dic,training_config_list,
                network_config_list,experiment_log_name,
                predict_batch_size=32,
                message=''):
    experiment_log = {}
    experiment_log["message"]=message
    #Load Dataset > dataset json tem que ter videos que geraram ele
    print("Dataset info:")
    print(json.dumps(dataset_dic,indent=4))
    # exit(1)
    print ("loading audio...")
    audio_dataset = np.load(dataset_dic["audioDatasetName"]+".npy")
    print ("loading images...")
    image_dataset = np.load(dataset_dic["imageDatasetName"]+".npy")
    ##Number of videos for training must be at least as large as the whole dataset
    # assert(training_config_dic["n_train_videos"]<=len(dataset_dic["n_video_samples"]))
    # train_samples = dataset_dic["n_video_samples"][:training_config_dic["n_train_videos"]]
    #Cut dataset??
    train_inputs       = image_dataset[:dataset_dic["cut"]]
    train_targets      = audio_dataset[:dataset_dic["cut"]]
    validation_inputs  = image_dataset[dataset_dic["cut"]:]
    validation_targets = audio_dataset[dataset_dic["cut"]:]
    #preprocessing> data = (data-media)/desvio
    # training_mean = train_inputs.mean(axis=(0,1,2))
    # training_std  = train_inputs.std(axis=(0,1,2))
    # validation_mean = validation_inputs.mean(axis=(0,1,2))
    # validation_std  = validation_inputs.std(axis=(0,1,2))
    #
    # training_mean_target = train_targets.mean(axis=0)
    # training_std_target  = train_targets.std(axis=0)
    # validation_mean_target = validation_targets.mean(axis=0)
    # validation_std_target  = validation_targets.std(axis=0)
    print("Preprocessing training...")

    mean = np.array([103.939, 116.779, 123.68])
    std  = np.array([1.,1.,1.])
    # training_mean = mean
    # training_std  = std
    training_mean = train_inputs.mean(axis=(0,1,2))
    training_std  = train_inputs.std(axis=(0,1,2))
    validation_mean = validation_inputs.mean(axis=(0,1,2))
    validation_std  = validation_inputs.std(axis=(0,1,2))

    training_mean_target = train_targets.mean(axis=0)
    training_std_target  = train_targets.std(axis=0)
    validation_mean_target = validation_targets.mean(axis=0)
    validation_std_target  = validation_targets.std(axis=0)

    train_inputs -= training_mean
    train_inputs /= training_std
    validation_inputs -= training_mean
    validation_inputs /= training_std

    # train_targets-=training_mean_target
    # train_targets/=training_std_target
    # validation_targets-=training_mean_target
    # validation_targets/=training_std_target

    to_python_scalar = lambda x:x.item()
    to_python_scalar_array = lambda x:list(map(to_python_scalar,x))
    meansTrain = to_python_scalar_array(training_mean)
    stdsTrain  = to_python_scalar_array(training_std)
    meansValidation = to_python_scalar_array(validation_mean)
    stdsValidation  = to_python_scalar_array(validation_std)
    print(np.array(training_mean_target))
    print(np.array(training_mean_target.shape))
    # meansTrain_target = list (map(to_python_scalar,np.array(training_mean_target)))
    # stdsTrain_target  = list (map(to_python_scalar,np.array(training_std_target)))
    # meansValidation_target = list (map(to_python_scalar,np.array(validation_mean_target)))
    # stdsValidation_target  = list (map(to_python_scalar,np.array(validation_std_target)))
    meansTrain_target =  training_mean_target
    stdsTrain_target  =  training_std_target
    meansValidation_target =  validation_mean_target
    stdsValidation_target  =  validation_std_target

    dataset_dic["meansTrainX"] = meansTrain
    dataset_dic["stdTrainX"]   = stdsTrain
    dataset_dic["meansValidationX"] = meansValidation
    dataset_dic["stdValidationX"]   = stdsValidation
    dataset_dic["meansTrainY"] = meansTrain_target
    dataset_dic["stdTrainY"]   = stdsTrain_target
    dataset_dic["meansValidationY"]= meansValidation_target
    dataset_dic["stdValidationY"]  = stdsValidation_target

    print (train_inputs.shape)
    print (validation_inputs.shape)
    print (dataset_dic["n_video_samples"])
    experiment_log["dataset_dic"] = deepcopy(dataset_dic)
    # print("train mean:",train_inputs.mean(axis=(0,1,2)))
    # print("train std:",train_inputs.std(axis=(0,1,2)))
    # print("validation mean:",validation_inputs.mean(axis=(0,1,2)))
    # print("validation std:",validation_inputs.std(axis=(0,1,2)))
    experiment_log["training_config_list"] = []
    experiment_log["network_config_list"]  = []
    experiment_log["prediction_list"]      = []
    for counter,network_config_dic in enumerate(network_config_list):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        sess = tf.Session(config=config)
        K.set_session(sess)
        #Retorna dicionario? #Salvar model.json
        #Salvar mesmo experimento com o mesmo UUID
        # model,convolutional  = net_model(training_config)
        training_config_dic = training_config_list[counter]
        print("Training info:")
        print(json.dumps(training_config_dic,indent=4))
        ##Network building
        network_config_dic["input_shape"]=train_inputs[0].shape
        target_shape = train_targets[0].shape
        if bool(target_shape) ==False: #Case for array targets, shape = (N,) , not shape=(N,1)
            target_shape=1
        network_config_dic["output_size"]=target_shape
        print("Network input shape:",network_config_dic["input_shape"])
        print("Network output size:",network_config_dic["output_size"])

        # network = net_model(network = network_config_dic["network"],
        #                     input_shape=network_config_dic["input_shape"],
        #                     output_shape=network_config_dic["output_shape"],
        #                     weights=network_config_dic["weights"]
        #                         )
        network,intermediate_network = net_model(**network_config_dic)
        network_config_dic["param_count"] = network.count_params()
        network.summary()
        print(json.dumps(network_config_dic, indent=4, sort_keys=True))
        # for layer in network.layers:
        #     print(layer.name)
        #     print(layer.trainable)
        ## Train
        lr_schedule = lr_schedule_maker(training_config_dic["lr_list"],
                                        training_config_dic["epoch_schedule"],
                                        verbose=True)
        model_save_name_full = training_config_dic["model_save_path"]\
                                +training_config_dic["model_save_name"]
        lrate = LearningRateScheduler(lr_schedule)
        model_checkpoint = ModelCheckpoint(model_save_name_full,
                                            monitor='val_loss',
                                            verbose=1,save_best_only=True,
                                            save_weights_only=False,
                                            mode='auto')
        terminate_on_nan = TerminateOnNaN()
        callbacks = [lrate,model_checkpoint,terminate_on_nan]
        optimizer = get_optimizer(training_config_dic["optimizer"],
                                training_config_dic["optimizer_config"])
        network.compile(optimizer,
                        training_config_dic["loss"],
                        training_config_dic["metrics"])
        history = network.fit(x=train_inputs,
                        y=train_targets,
                        validation_data=(validation_inputs, validation_targets),
                        batch_size=training_config_dic["batch_size"],
                        epochs = training_config_dic["epochs"],
                        callbacks = callbacks)
        print(history.history.items())
        print("Loading checkpoint model...")
        # del network
        # checkpoint_model = load_model(model_save_name_full)
        network = load_model(model_save_name_full)
        print("Predicting...")
        # Multiplication by training_std_target and adding training_mean_target
        #   assumes that there is a target preprocessing
        train_predict = network.predict(train_inputs,batch_size=predict_batch_size)
        # train_predict*= training_std_target
        # train_predict+= training_mean_target
        val_predict = network.predict(validation_inputs,batch_size=predict_batch_size)
        # val_predict*= training_std_target
        # val_predict+= training_mean_target
        training_config_dic['v_mse']      = ((val_predict-validation_targets)**2).mean()
        training_config_dic['v_mae']      = (np.abs(val_predict-validation_targets)).mean()
        training_config_dic['t_mse']      = ((train_predict-train_targets)**2).mean()
        training_config_dic['t_mae']      = (np.abs(train_predict-train_targets)).mean()
        try:
            training_config_dic['loss']     = deepcopy(history.history['loss'])
            training_config_dic['val_loss'] = deepcopy(history.history['val_loss'])
        except KeyError:
            print("Caught KeyError, possibly caused by NaN from training")
            training_config_dic['loss']     = [np.nan]
            training_config_dic['val_loss'] = [np.nan]
        experiment_log["training_config_list"].append(deepcopy(training_config_dic))
        experiment_log["network_config_list"].append(deepcopy(network_config_dic))
        experiment_log["prediction_list"].append({"training":to_python_scalar_array(train_predict),
                                                "validation":to_python_scalar_array(val_predict)})
        K.clear_session() # Avoids memory leak at each iteration
    experiment_log["targets"] = {"training":to_python_scalar_array(train_targets),
                                "validation":to_python_scalar_array(validation_targets)}
    experiment_log["experiment_log_name"] = experiment_log_name
    jutils.save_json(experiment_log_name,experiment_log)
    # for training configuration

    #    model,convolutional  = generate_model(configuration)  #Retorna dicionario? #Salvar model.json
    #
    #    if train last only:
    #          data = convolutional.predict(dataset)
    #          data = data.reshape
    #    trained_model = model.train(data)    > save model,training curves
    #    training   = model.predict()         > save training   outputs
    #    if validation is not None:
    #        validation = model.predict()         > save validation outputs


if __name__ == "__main__":
    # Experiment > predictions > latex
    from json_utils import json_utils
    from locations import directories
    image_dataset_name = directories.numpyDataset+"image_experiment_videos_1638_2018-10-25.19:04:55.npy"
    audio_dataset_name = directories.numpyDataset+"audio_experiment_videos_1638_2018-10-25.19:04:55.npy"
    dataset_json_name  = directories.numpyDataset+"image_experiment_videos_1638_2018-10-25.19:04:55.json"
    dataset_json = json_utils.load_json(dataset_json_name)

    ### Dataset
    dataset_dic = deepcopy(dataset_json)
    dataset_dic["dataset_json_name"]  = dataset_json_name

    ### Network
    network_config_dic = {}
    network_config_dic["network"] = "VGG16"
    network_config_dic["weights"] = "imagenet"
    network_config_dic["include_original_top"] = False
    network_config_dic["global_average_pooling"] = True
    network_config_dic["hidden_dense"] = 128
    # newtwork_config_dic["input_shape"] = input_shape ##Infer from input
    # newtwork_config_dic["output_size"] = output_size ##Infer from target

    ### Training
    training_config_dic = {}
    training_config_dic["n_train_videos"] = 3
    #   "optimizer": SGDOptimizer,
    #   "epoch_schedule": epoch_schedule,
    #   "lr_list": lr_list,
    #   "epochs": epochs,
    #   "batch_size": batch_size,
    #   "train_id": train_id,
    #   "experiment_name": experiment_name,
    #   "train_last_only": True}

    ### Call
    network_config_list = [network_config_dic]
    experiment(dataset_dic,training_config_dic,network_config_list)
