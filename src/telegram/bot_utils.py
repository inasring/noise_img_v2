import telepot
import time
import urllib3

def read_file(filename):
    string=''
    with open(filename,'r') as fhandler:
        string = fhandler.read()
    string = ''.join(string.split('\n'))
    return string

def retry_sendDocument(bot,userid,fname,wait_time=1,retries=10,ftype='pdf'):
    modeSelection = {'pdf':'rb','txt':'r'}
    readmode = modeSelection[ftype]
    success = False
    with open(fname,readmode) as fhandler:
        print("Trying to send file...")
        for retry in range(1,retries+1):
            try:
                bot.sendDocument(userid,open(fname,readmode))
            except urllib3.exceptions.MaxRetryError:
                print("Failure to send file in try ",retry)
            else:
                print("file sent")
                success = True
            if success:
                break
            time.sleep(wait_time)
    if not success:
        print('file not sent after',retries,'retries')
    return success
