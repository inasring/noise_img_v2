from .bot_utils import read_file,retry_sendDocument
from locations import directories as d
import telepot

token = read_file(d.token_fname)
myid = int(read_file(d.myid_fname))
bot = telepot.Bot(token)
# bot = telepot.Bot(token)
# bot.sendDocument(myid,open("/home/mazza/gitlab/noise_img_v2/latex/latex_26112018_v4_small_dataset/report.pdf",'rb'))
fname = '/home/mazza/gitlab/noise_img_v2/latex/latex_26112018_v4_small_dataset/report.pdf'
retry_sendDocument(bot,myid,fname,wait_time=1,retries=10,ftype='pdf')
