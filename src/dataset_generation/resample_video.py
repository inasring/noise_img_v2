import skvideo.io
import time
import json
import subprocess
from locations import directories as dirs
from dir_utils import dir_utils as du

def downsampleVideo(video_filename,wTarget,hTarget,output_directory=None,output_filename=None,overwrite=True):
    # filename_extracted = filenameNoExtension(video_filename)
    # videogen = skvideo.io.vreader(video_filename)
    metadata = skvideo.io.ffprobe(video_filename)
    # print(metadata.keys())
    # print("AUDIO METADATA")
    if output_filename is None:
        output_filename = video_filename
    output_filename = du.filenameNoPath(output_filename)
    output_filename = output_directory+output_filename
    videoStr = 'video'
    width = "@width"
    height = "@height"
    # wTarget = int(metadata[videoStr][width]) //widthScale
    # hTarget = int(metadata[videoStr][height])//heightScale
    # ffmpeg -i input.avi -s 720x480 -c:a copy output.mkv
    command  = "ffmpeg "
    if overwrite:
        command += "-y"+" "
    else:
        command += "-n"+" "
    command += "-hide_banner"+" "
    command += "-i"+" "
    command += video_filename+" "           # input
    command += "-s"+" "+str(wTarget)+"x"+str(hTarget)+" "# output type
    command += "-c:a copy"+" "
    command += output_filename # output filename
    #filename does not change if output_filename is not required, only extension
    print (command)
    subprocess.call(command,shell=True)

if __name__ == "__main__":
    video_filename = "M2U00001.MPG"
    targetSizeW = 120
    targetSizeH = 120
    du.make_dir(dirs.resampled_120x120)
    video_filename = dirs.raw_videos+video_filename
    # downsampleVideo(video_filename,6,4,output_directory=dirs.resampled_120x120,output_filename=None)
    downsampleVideo(video_filename,targetSizeW,targetSizeH,
                    output_directory=dirs.resampled_120x120,output_filename=None)
