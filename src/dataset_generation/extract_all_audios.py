from .extract_audio import extractAudio
import glob
from locations import directories as dirs
from dir_utils import dir_utils as du
from tqdm import tqdm
import argparse

parser = argparse.ArgumentParser(description="Ignores existing audio files")
parser.add_argument("-i",action='store_true')#overwrite if set
args = parser.parse_args()
# dirs.raw_videos
# dirs.audio
MPG = '.MPG'
mp3 = '.mp3'
# print(glob.glob(dirs.raw_videos+'*'+MPG))
videoFilenames = glob.glob(dirs.raw_videos+'*'+MPG)
# audioFilenames = glob.glob(dirs.audio+'*'+mp3)
# get_fname = lambda x:filenameNoExtension(filenameNoPath(x))
# setVideos = set(map(get_fname,videoFilenames))
# setAudios = set(map(get_fname,audioFilenames))
# if args.i:
#     print("Ignoring existing audio files")
#     setToProcess = setVideos
# else:
#     setToProcess = setVideos-setAudios
# print (setToProcess)
# listToProcess = list(setToProcess)
# listToProcess = list(map(lambda x:dirs.raw_videos+x+MPG,listToProcess))
# for video_filename in listToProcess:
du.make_dir(dirs.audio)
overwrite = args.i
for video_filename in tqdm(videoFilenames):
    print (video_filename)
    extractAudio(video_filename,
        output_directory=dirs.audio,
        file_type="wav",
        output_filename=None,
        overwrite=overwrite)
