import skvideo.io
from locations import directories
from dir_utils import dir_utils
import scipy.io.wavfile as spWav
import numpy as np
from tqdm import tqdm

def get_video_rate(videoFilename):
    metadata      = skvideo.io.ffprobe(videoFilename)
    frameRateText = metadata["video"]["@avg_frame_rate"]
    separator     = "/"
    if frameRateText.find(separator):
        split         = frameRateText.split(separator)
        num           = float(split[0])
        den           = float(split[1])
        videoRateRead = num/den
    else:
        videoRateRead = float(frameRateText)
    return videoRateRead

def generateDataset(videoFilename,videoRate,
                    audioFilename,audioRate,
                    audioTimeForward,
                    audioTimeBackward,
                    subsampleTime):
    videoRateRead = get_video_rate(videoFilename)
    audioRateRead,audioArray =  spWav.read(audioFilename)
    if audioRate is None:#None means no audio rate provided
        audioRate = audioRateRead
    if videoRate is None:
        videoRate = videoRateRead
    videoArray = skvideo.io.vread(videoFilename)
    audioSampsBackwards  = audioTimeBackward*audioRate//1000
    audioSampsForwards   = audioTimeForward*audioRate//1000
    print("audioRate=",audioRate)
    print("audioSampsBackwards=",audioSampsBackwards)
    print("audioSampsForwards=",audioSampsForwards)
    maxAudioTimeDistance = max(audioSampsBackwards,audioSampsForwards)
    ignoreFrames         = int((maxAudioTimeDistance*videoRate)//1000+1)
    subSamples = round(subsampleTime/1000*videoRate)
    print("subSamples=",subSamples)
    dataRange            = range(ignoreFrames,len(videoArray)-ignoreFrames,subSamples)
    frameNumber          = len(dataRange)
    assert(frameNumber>0)
    # audioArray = audioArray.mean(axis=1)#Mean of all channels
    audioArray = audioArray.mean(axis=1)#Mean of all channels
    print("Any nan in audio dataset:",np.isnan(audioArray).any())

    print ("audioArray.shape=",audioArray.shape)
    # exit(1)
    imagesDatasetShape = tuple([frameNumber]) + videoArray.shape[1:]
    print ("imagesDatasetShape=",imagesDatasetShape)
    imagesDataset = np.empty(imagesDatasetShape)
    audioDatasetShape = tuple([frameNumber])
    audioDataset  = np.empty(audioDatasetShape)

    rateRatio = audioRate/videoRate
    print("rateRatio=",rateRatio)
    datasetIndex = 0
    for videoSampleIndex in tqdm(dataRange):
        imagesDataset[datasetIndex] = videoArray[videoSampleIndex]
        equivSample = videoSampleIndex*rateRatio
        audioStart = round(equivSample-audioSampsBackwards)
        audioEnd   = round(equivSample+audioSampsForwards)
        # if videoSampleIndex > len(videoArray)-5:
        #     print ("[audioStart,audioEnd]=",[audioStart,audioEnd])
        #     print ("[audioEnd-audioStart]=",[audioEnd-audioStart])
        #     print("videoSampleIndex=",videoSampleIndex)
        # meanSlice = np.mean(np.abs(audioArray[audioStart:audioEnd]))
        meanSlice = np.mean(np.square(audioArray[audioStart:audioEnd]))
        audioDataset[datasetIndex]  = np.log(meanSlice)
        if np.isnan(audioDataset[datasetIndex]):
            print("Nan found at index: ",videoSampleIndex, " with value:",meanSlice)
            print("Slice:",audioArray[audioStart:audioEnd])
        datasetIndex+=1

    return imagesDataset,audioDataset


def generate_joined_dataset(videoFilenameList,videoRateList,
                            audioFilenameList,audioRateList,
                            audioTimeForwardList,
                            audioTimeBackwardList,
                            subsampleTimeList):
        n_video_samples = []
        #Sanity check
        assert(len(videoFilenameList)==len(audioFilenameList))
        video_ret,audio_ret = generateDataset(videoFilenameList[0],videoRateList[0],
                                                audioFilenameList[0],audioRateList[0],
                                                audioTimeForwardList[0],
                                                audioTimeBackwardList[0],
                                                subsampleTimeList[0])
        n_video_samples.append(len(video_ret))
        for dataIndex in range(1,len(videoFilenameList)):
            video_dataset,audio_dataset = generateDataset(videoFilenameList[dataIndex],
                                            videoRateList[dataIndex],
                                            audioFilenameList[dataIndex],audioRateList[dataIndex],
                                            audioTimeForwardList[dataIndex],
                                            audioTimeBackwardList[dataIndex],
                                            subsampleTimeList[dataIndex])
            video_ret = np.concatenate((video_ret,video_dataset),axis=0)
            audio_ret = np.concatenate((audio_ret,audio_dataset),axis=0)
            n_video_samples.append(len(video_dataset))
        return video_ret,audio_ret,n_video_samples

def save_metadata_dataset(x_data_filename_nodir,
                          y_data_filename_nodir,
                          saveName,
                          timeForward,
                          timeBackward,
                          subsampleTime,
                          n_video_samples,
                          image_dataset_name,
                          audio_dataset_name,
                          save_dir =directories.numpyDataset,
                          verbose=False):
    import json
    if type(x_data_filename_nodir) is not list:
        x_data_filename_nodir = list(x_data_filename_nodir)
    if type(y_data_filename_nodir) is not list:
        y_data_filename_nodir = list(y_data_filename_nodir)
    if type(timeForward) is not list:
        timeForward = list(timeForward)
    if type(timeBackward) is not list:
        timeBackward = list(timeBackward)
    if type(subsampleTime) is not list:
        subsampleTime = list(subsampleTime)
    if type(n_video_samples) is not list:
        n_video_samples = list(n_video_samples)

    dataset_data = {
            "videoFiles":x_data_filename_nodir,
            "audioFiles":y_data_filename_nodir,
            "imageDatasetName":image_dataset_name,
            "audioDatasetName":audio_dataset_name,
            "timeForward":timeForward,
            "timeBackward":timeBackward,
            "subsampleTime":subsampleTime,
            "n_video_samples":n_video_samples
    }
    saveName = dir_utils.filenameNoPath(saveName)
    with open(save_dir+saveName+".json", 'w') as outfile:
        if verbose:
            print("Saving to ",outfile)
        json.dump(dataset_data, outfile)

def save_av_data(audio_data,
                video_data,
                image_dataset_name,
                audio_dataset_name,
                videoFilenameList,videoRateList,
                audioFilenameList,audioRateList,
                audioTimeForwardList,
                audioTimeBackwardList,
                subsampleTimeList,
                n_video_samples,
                save_dir      = directories.numpyDataset,
                save_metadata = True,
                append_time   = True,
                verbose=False):
        dir_utils.make_dir(save_dir)
        x_data_filename_nodir = image_dataset_name
        y_data_filename_nodir = audio_dataset_name
        if append_time:
                from datetime import datetime
                string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
                x_data_filename_nodir = x_data_filename_nodir+string_date
                y_data_filename_nodir = y_data_filename_nodir+string_date
        image_dataset_name = save_dir+x_data_filename_nodir
        audio_dataset_name = save_dir+y_data_filename_nodir
        print("Any nan in audio dataset Outside:",np.isnan(audio_data).any())
        if verbose:
            print("Saving audio dataset to ",audio_dataset_name,"...")
        np.save(audio_dataset_name,audio_data)
        if verbose:
            print("Saving video dataset to ",image_dataset_name,"...")
        np.save(image_dataset_name,video_data)
        metadata_name = image_dataset_name
        if save_metadata:
            save_metadata_dataset(videoFilenameList,
                                      audioFilenameList,
                                      metadata_name,
                                      audioTimeForwardList,
                                      audioTimeBackwardList,
                                      subsampleTimeList,
                                      n_video_samples,
                                      image_dataset_name,
                                      audio_dataset_name,
                                      save_dir =save_dir,
                                      verbose=verbose)

def save_dataset(image_dataset_name,
                    audio_dataset_name,
                    videoFilenameList,videoRateList,
                    audioFilenameList,audioRateList,
                    audioTimeForwardList,
                    audioTimeBackwardList,
                    subsampleTimeList,
                    save_dir=directories.numpyDataset,
                    save_metadata=True,
                    append_time=True,
                    verbose=False):
    video_data,audio_data,n_video_samples = generate_joined_dataset(
                                                videoFilenameList,videoRateList,
                                                audioFilenameList,audioRateList,
                                                audioTimeForwardList,
                                                audioTimeBackwardList,
                                                subsampleTimeList)
    save_av_data(audio_data,
                    video_data,
                    image_dataset_name,
                    audio_dataset_name,
                    videoFilenameList,videoRateList,
                    audioFilenameList,audioRateList,
                    audioTimeForwardList,
                    audioTimeBackwardList,
                    subsampleTimeList,
                    n_video_samples,
                    save_dir =save_dir,
                    save_metadata=save_metadata,
                    append_time=append_time,
                    verbose=verbose)

def make_args(video_names,video_dir, audio_dir,
                timeForward,timeBackward,subsampleTime,
                audioRate,videoRate,audio_ext=".wav"):
    #Assumes all videos in folder "video_dir"
    #Assumes all audios in folder "audio_dir"
    #Assumes all audios and videos have the same rate
    #Assumed audio filenames have the same name as the videos but with a different extension
    # For example:
    #  Video my_vid.MPG has respective audio my_vid.wav
    n_videos        = len(video_names)
    audioRates      = [audioRate]*n_videos
    videoRates      = [videoRate]*n_videos
    timesForward    = [timeForward]*n_videos
    timesBackward   = [timeBackward]*n_videos
    subsampleTimes  = [subsampleTime]*n_videos
    video_fullnames = []
    audio_fullnames = []
    for i in range(len(video_names)):
        video_fullnames.append(video_dir+video_names[i])
        audio_fullnames.append(audio_dir+dir_utils.filenameNoExtension(video_names[i])\
                            +audio_ext)

    ret = (video_fullnames,videoRates,
            audio_fullnames,audioRates,
            timesForward,timesBackward,
            subsampleTimes)
    return ret

if __name__ == "__main__":
    # video_filename = "M2U00002.MPG"
    # video_filename_1 = "M2U00001.MPG"
    # video_filename_2 = "M2U00007.MPG"
    # #Assumed all videos have been resampled
    # # video_filename_full = directories.resampled_120x120+video_filename
    # video_filename_full_1 = directories.resampled_240x240+video_filename_1
    # audio_filename_1 = dir_utils.filenameNoExtension(video_filename_1)+'.wav'
    # audio_filename_full_1 = directories.audio+audio_filename_1
    #
    # video_filename_2 = "M2U00007.MPG"
    # video_filename_full_2 = directories.resampled_240x240+video_filename_2
    # audio_filename_2 = dir_utils.filenameNoExtension(video_filename_2)+'.wav'
    # audio_filename_full_2 = directories.audio+audio_filename_2



    timeForward   = 300 # in ms
    timeBackward  = 300 # in ms
    subsampleTime = 30000 # in ms
    # subsampleTime = 1000 # in ms
    # subsampleTime = 1000/30
    audioRate     = None
    videoRate     = None

    # n_videos = 5
    # video_names = ["M2U0000"+str(i)+".MPG" for i in range(1,n_videos+1)]
    video_numbers = [1,6,3,8]
    # video_numbers= [1]
    video_names = ["M2U0000"+str(video_number)+".MPG" for video_number in video_numbers]
    video_numbers_concat = ''.join(map(str,video_numbers))
    image_dataset_name = "image_experiment_videos_"+video_numbers_concat+"_"
    audio_dataset_name = "audio_experiment_videos_"+video_numbers_concat+"_"
    print (video_names)
    args = make_args(video_names,directories.resampled_240x240,directories.audio,
                timeForward,timeBackward,subsampleTime=subsampleTime,
                audioRate=audioRate,videoRate=videoRate,audio_ext='.wav')
    print (args)



    # videoFilenameList = [video_filename_full_1,video_filename_full_2]
    # videoRateList     = len(videoFilenameList)*[None]
    # audioFilenameList = [audio_filename_full_1,audio_filename_full_2]
    # audioRateList     = len(audioFilenameList)*[None]
    # audioTimeForwardList   = len(audioFilenameList)*[timeForward]
    # audioTimeBackwardList  = len(audioFilenameList)*[timeBackward]
    # subsampleTimeList      = len(audioFilenameList)*[subsampleTime]
    save_dir = directories.numpyDataset
    save_metadata = True
    verbose  = True
    append_time = True

    # save_dataset("small_image_dataset_",
    #             "small_audio_dataset_",
    #             *args,
    #             save_dir=save_dir,
    #             save_metadata=save_metadata,
    #             append_time=append_time,
    #             verbose=verbose)


    save_dataset(image_dataset_name,
                audio_dataset_name,
                *args,
                save_dir=save_dir,
                save_metadata=save_metadata,
                append_time=append_time,
                verbose=verbose)

    # save_dataset(videoFilenameList,videoRateList,
    #                     audioFilenameList,audioRateList,
    #                     audioTimeForwardList,
    #                     audioTimeBackwardList,
    #                     subsampleTimeList,
    #                     save_dir=save_dir,
    #                     save_metadata=save_metadata,
    #                     verbose=verbose)
