import skvideo.io
import subprocess
from locations import directories as dirs
from dir_utils import dir_utils as du

def extractAudio(video_filename,output_directory=None,file_type="wav",output_filename=None,overwrite=True):
    filename_extracted = du.filenameNoExtension(video_filename)
    # videogen = skvideo.io.vreader(video_filename)
    metadata = skvideo.io.ffprobe(video_filename)
    # print(metadata.keys())
    # print("AUDIO METADATA")
    if output_filename is None:
        output_filename = filename_extracted
    output_filename = du.filenameNoPath(output_filename)
    if output_directory is None:
        output_directory = ''
    output_filename = output_directory+output_filename
    extension = file_type
    bit_rate_key = "@bit_rate"
    bit_rate     = metadata["audio"][bit_rate_key]
    command  = "ffmpeg "
    if overwrite:
        command += "-y"+" "
    else:
        command += "-n"+" "
    command += "-hide_banner"+" "
    command += "-i"+" "
    command += video_filename+" "           # input
    command += "-f"+" "+extension+" "       # output type
    command += "-ab"+" "+bit_rate+" "       # bitrate
    command += "-vn"+" "                    # no video
    command += output_filename+'.'+extension# output filename
    #filename does not change if output_filename is not required, only extension
    # print (command)
    subprocess.call(command,shell=True)

if __name__ == "__main__":
    video_filename = "M2U00001.MPG"
    video_filename = dirs.raw_videos+video_filename
    extractAudio(video_filename,output_directory=dirs.audio,
                file_type="wav",output_filename=None)
