import skvideo.io
import glob
from locations import directories as dirs
from dir_utils import dir_utils as du
from tqdm import tqdm
import argparse
from .resample_video import downsampleVideo

parser = argparse.ArgumentParser(description="Ignores existing video files")
parser.add_argument("-i",action='store_true')#overwrite if set
args = parser.parse_args()

MPG = '.MPG'
# print(glob.glob(dirs.raw_videos+'*'+MPG))
videoFilenames = glob.glob(dirs.raw_videos+'*'+MPG)
# audioFilenames = glob.glob(dirs.audio+'*'+mp3)
targetSizeW = 240
targetSizeH = 240

# videoStr = 'video'
# width = "@width"
# height = "@height"

du.make_dir(dirs.resampled_240x240)
# print(videoFilenames)
overwrite = args.i
print (overwrite)
print (type(overwrite))
videoFilenames.sort()
print (videoFilenames)
for video_filename in videoFilenames:
    print (video_filename)
    downsampleVideo(video_filename,targetSizeW,targetSizeH,
                    output_directory=dirs.resampled_240x240,
                    output_filename=None,
                    overwrite=overwrite)
