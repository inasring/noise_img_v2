from locations import directories
from dataset_generation.generate_dataset import make_args,save_dataset

timeForward   = 0 # in ms
timeBackward  = 300 # in ms
subsampleTime = 1000 # in ms
audioRate     = None
videoRate     = None

video_numbers = [1,6,3,8]
video_names = ["M2U0000"+str(video_number)+".MPG" for video_number in video_numbers]
video_numbers_concat = ''.join(map(str,video_numbers))
image_dataset_name = "image_experiment_videos_"+video_numbers_concat+"_"+"f_"+str(timeForward)+"_b_"+str(timeBackward)+"_"
audio_dataset_name = "audio_experiment_videos_"+video_numbers_concat+"_"+"f_"+str(timeForward)+"_b_"+str(timeBackward)+"_"
print (video_names)
args = make_args(video_names,directories.resampled_240x240,directories.audio,
            timeForward,timeBackward,subsampleTime=subsampleTime,
            audioRate=audioRate,videoRate=videoRate,audio_ext='.wav')
print (args)



save_dir = directories.numpyDataset
save_metadata = True
verbose  = True
append_time = True


save_dataset(image_dataset_name,
            audio_dataset_name,
            *args,
            save_dir=save_dir,
            save_metadata=save_metadata,
            append_time=append_time,
            verbose=verbose)