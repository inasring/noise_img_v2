from dataset_generation import generate_dataset as gd
from locations import directories as d
import os


timeForward   = 300 # in ms
timeBackward  = 300 # in ms
audioRate     = None
videoRate     = None

# video_numbers = [1,6,3,8]
video_numbers = [7]
video_names = ["M2U0000"+str(video_number)+".MPG" for video_number in video_numbers]
video_name_full = os.path.join(os.sep,d.resampled_240x240,video_names[0])
video_rate = gd.get_video_rate(video_name_full)
subsampleTime = 1000/video_rate # get all frames

video_numbers_concat = ''.join(map(str,video_numbers))
image_dataset_name = "image_experiment_videos_"+video_numbers_concat+"_"
audio_dataset_name = "audio_experiment_videos_"+video_numbers_concat+"_"
print (video_names)
args = gd.make_args(video_names,d.resampled_240x240,d.audio,
            timeForward,timeBackward,subsampleTime=subsampleTime,
            audioRate=audioRate,videoRate=videoRate,audio_ext='.wav')
print (args)



save_dir = d.numpyDataset
save_metadata = True
verbose  = True
append_time = True

gd.save_dataset(image_dataset_name,
                audio_dataset_name,
                *args,
                save_dir=save_dir,
                save_metadata=save_metadata,
                append_time=append_time,
                verbose=verbose)
