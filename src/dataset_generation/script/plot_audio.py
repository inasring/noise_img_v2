import matplotlib.pyplot as plt
import numpy as np
from locations import directories as d

filename_1   = d.numpyDataset+"audioDataset_f_300_b_300_M2U00001.npy"
filename_2   = d.numpyDataset+"audioDataset_f_300_b_300_M2U00002.npy"
filename_7   = d.numpyDataset+"audioDataset_f_300_b_300_M2U00007.npy"

data = np.load(filename_7)
n    = np.arange(len(data))
fig = plt.figure(1)
ax  = fig.add_subplot(111)
ax.plot(n,data)
plt.show()
