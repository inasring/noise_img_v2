import skvideo.io
import directories
import dir_utils
import matplotlib.pyplot as plt
import numpy as np
import scipy.io.wavfile as spWav

def movingAverage(signal,N):
    return np.convolve(signal,np.ones((N,))/N, mode='valid')

video_filename = "M2U00001.MPG"
np_extension= "npy"
audioNP = "audioDataset_"+dir_utils.filenameNoExtension(video_filename)+'.'+np_extension
audio_NP_full = directories.numpyDataset+audioNP
audio_filename_full = directories.audio+\
                dir_utils.filenameNoExtension(video_filename)+'.wav'
audio_NP_array = np.load(audio_NP_full)
sample = 2000
audio_NP_array = audio_NP_array[::sample]
NP_t  = sample*1/29.97*np.arange(len(audio_NP_array))
audioRate,audio_array = spWav.read(audio_filename_full)
audio_array = audio_array.mean(axis=1)
audio_array = movingAverage (audio_array,100)
audio_array = audio_array[::sample]
audio_array = np.log(np.abs(audio_array))
Wav_t = sample*1/audioRate*np.arange(len(audio_array))
print (audioRate)
# fig = plt.figure(1,figsize=(13,7))
# ax = fig.add_subplot(111)
# ax.plot(NP_t,audio_NP_array-audio_NP_array.mean(),color='blue')
# ax.plot(Wav_t,audio_array/10000,'k
from vispy.plot import Fig
fig = Fig()
ax_left = fig[0, 0]
audio_NP_array = (audio_NP_array).T
ax_left.plot(np.vstack((NP_t,audio_NP_array)).T,width=3, color='k',
                      xlabel='time')
print (Wav_t.shape)
print ((audio_array/10000).shape)
ax_left.plot(np.vstack((Wav_t,audio_array)).T,width=3, color='b',
                     xlabel='time')
