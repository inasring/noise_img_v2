import numpy as np
rand_state=1234
np.random.seed(rand_state)
import json
from keras.losses import mean_squared_error
from keras import metrics
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD,Adam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
import keras.backend as K
import sys
import csv
import copy

import train_util
from dense_net import dense_net
import directories
import dir_utils
import utils
sys.setrecursionlimit(10000)

dir_utils.make_dir(directories.modelDir)
dir_utils.make_dir(directories.resDir)

def make_model(model_name,input_shape,pretrained,configuration_dict):
    #Add input_shape as argument, and then add to training_data dict
    DenseNet_name = "DenseNet"
    VGG16_name    = "VGG16"
    ResNet50_name = "ResNet50"
    model_config = copy.deepcopy(configuration_dict)
    model_config["input_shape"] = input_shape
    # print("model_config=",model_config)
    if model_name == DenseNet_name and not pretrained:
        model = dense_net(n_dense_blocks=model_config["n_dense_blocks"],
                        input_shape=model_config["input_shape"],
                        feature_map_n_list=model_config["feature_map_n_list"],
                        n_filter_initial=model_config["n_filter_initial"],
                        droprate=model_config["droprate"],
                        bnL2norm=model_config["bnL2norm"],
                        l2_dense_penalization=model_config["l2_dense_penalization"],
                        nb_classes=model_config["nb_classes"],
                        finalActivation=model_config["finalActivation"],
                        summary=False)
    else:
        model_config["n_dense_blocks"] = None
        model_config["feature_map_n_list"] = None
        model_config["n_filter_initial"] = None
        model_config["droprate"] = None
        model_config["bnL2norm"] = None
        model_config["l2_dense_penalization"] = None
        model_config["nb_classes"] = None
        model_config["finalActivation"] = None
        model_config["depth"]=None            #Passar config da dense com esses parâmetros
        model_config["growthRate"]=None
    if pretrained:
        from keras.layers import Dense,Flatten
        # model_config["pretrainedName"] = model_name
        # model_config["lastLayerDenseSize"] = lastLayerDenseSize
        if model_name==VGG16_name:
            from keras.applications import VGG16
            # print('model_config["input_shape"]=',model_config["input_shape"])
            base_model = VGG16(include_top=False, weights='imagenet',input_shape=model_config["input_shape"])
            x = base_model.output
            x = Flatten()(x)
            x = Dense(model_config["lastLayerDenseSize"], activation='relu')(x)
            predictions = Dense(1, activation=model_config["finalActivation"])(x)
            model = Model(inputs=base_model.input,outputs=predictions)
        if model_name==ResNet50_name:
            from keras.applications import ResNet50
            from keras.layers       import UpSampling2D,Input
            upscaleFactor  = 2
            upscaled_shape = tuple([model_config["input_shape"][0]*upscaleFactor,
                                    model_config["input_shape"][1]*upscaleFactor,
                                    model_config["input_shape"][2]])
            base_model = ResNet50(include_top=False, weights='imagenet', input_shape=upscaled_shape)
            input_layer = Input(shape=model_config["input_shape"])
            x = UpSampling2D(size=(upscaleFactor,upscaleFactor))(input_layer)
            x = base_model(x)
            x = Flatten()(x)
            x = Dense(model_config["lastLayerDenseSize"], activation='relu')(x)
            predictions = Dense(1, activation=model_config["finalActivation"])(x)
            model = Model(inputs=input_layer,outputs=predictions)
    else:
        model_config["lastLayerDenseSize"] = None
        model_config["pretrainedName"] = None
    return model,model_config



def train_network(imageDatasetName,
                    audioDatasetName,
                    sample,
                    test_size,
                    modelName,
                    pretrained,
                    optimizer,
                    epoch_schedule,
                    lr_list,
                    data_augmentation,
                    epochs,
                    batch_size,
                    config_dict,
                    train_id,
                    experiment_name=None):
    #imageDatasetName: path to dataset
    sgd_optimizer = 'SGD'
    adam_optimizer= 'Adam'

    print("Loading dataset...")
    # audioDataset = np.load(directories.numpyDataset+audioDatasetName)
    # imageDataset = np.load(directories.numpyDataset+imageDatasetName,
    #                         mmap_mode='r')
    audioDataset = np.load(audioDatasetName)
    imageDataset = np.load(imageDatasetName,mmap_mode='r')

    audioDataset = audioDataset[:sample]
    imageDataset = imageDataset[:sample]
    print("Dataset loaded.")
    print("Image dataset shape =",imageDataset.shape)
    print("Audio dataset shape =",audioDataset.shape)

    print("Splitting dataset...")
    cutPosition  = sample-int(sample*test_size)
    assert(cutPosition<=sample)
    X_train      = imageDataset[:cutPosition]
    X_validation = imageDataset[cutPosition:]
    Y_train      = audioDataset[:cutPosition]
    Y_validation = audioDataset[cutPosition:]
    print("Dataset split.")
    del imageDataset

    X_train = X_train.astype('float32')
    X_validation = X_validation.astype('float32')
    Y_train = Y_train.astype('float32')
    Y_validation = Y_validation.astype('float32')
    #pixelMean = np.mean(X_train, axis=0)

    print("Evaluating dataset channelwise statistics")
    meansTrain,stdsTrain = utils.channels_MeanStd(X_train, dim_ordering='tf')
    print(Y_train)
    yStd  = Y_train.std()
    yMean = Y_train.mean()
    print("Preprocessing dataset...")
    X_train      = utils.pre_proc(X_train, meansTrain,
                           stdsTrain, dim_ordering='tf')
    print("mean = ",meansTrain)
    print("std=   ",stdsTrain)
    X_validation = utils.pre_proc(X_validation, meansTrain,
                            stdsTrain, dim_ordering='tf')
    print("K.image_dim_ordering()=",K.image_dim_ordering())
    print('X_train shape:', X_train.shape)
    print(X_train.shape[0], 'train samples')
    print(X_validation.shape[0], 'test samples')


    input_shape = X_train[0].shape
    print("input_shape=",input_shape)
    ##Make Model
    # config_dict["input_shape"] = input_shape
    model,model_config = make_model(modelName,input_shape,pretrained,config_dict)

    from datetime import datetime
    string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
    filename  = "noiseImg"+"_size_"+str(sample)+'_'+string_date
    filepath  = directories.modelDir+filename+".h5"
    results   = directories.resDir+filename+".results"
    model_checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1,
                                        save_best_only=True, save_weights_only=False,
                                         mode='auto')
    # early_stopping   = EarlyStopping(monitor='val_loss', patience=10, verbose=1, mode='auto')
    lr_schedule = train_util.lr_schedule_maker(lr_list,epoch_schedule,verbose=True)
    lrate = LearningRateScheduler(lr_schedule)
    if optimizer == sgd_optimizer:
        model.compile(SGD(lr=0.01, momentum=0.9, decay=0.0, nesterov=True), loss='mean_squared_error',
                      metrics=['mse'])
        callbacks = [lrate,model_checkpoint]
    if optimizer == adam_optimizer:
        model.compile(Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False),
                    loss=mean_squared_error,
                    metrics=[metrics.mse])
        #Use Adam fixed LR
        callbacks = [model_checkpoint]

    to_python_scalar = lambda x:x.item()
    meansTrain = list (map(to_python_scalar,meansTrain))
    stdsTrain  = list (map(to_python_scalar,stdsTrain))
    yMean      = to_python_scalar(yMean)
    yStd       = to_python_scalar(yStd)

    training_data = {
            "sample":sample,
            "date":string_date,
            "cutPosition":cutPosition,
            "random_state":rand_state,
            "imageDatasetName":imageDatasetName,
            "audioDatasetName":audioDatasetName,
            "meansTrainX":list(meansTrain),
            "stdTrainX":list(stdsTrain),
            "meansTrainY":yMean,
            "stdTrainY":yStd,
            "test_size":test_size,
            "modelPath":filepath,
            "data_augmentation":data_augmentation,
            "epochs":epochs,
            "batch_size":batch_size,
            "epoch_schedule":epoch_schedule,
            "lr_list":lr_list,
            "optimizer":optimizer,
            "experiment_name":experiment_name,
            "train_id":train_id,
            "modelName":modelName
    }

    all_training_data = {**training_data,**model_config}#Does not work in python 2.7

    with open(directories.modelDir+filename+".json", 'w') as outfile:
        json.dump(all_training_data, outfile)

    if not data_augmentation:
        print('Not using data augmentation.')
        history=model.fit(X_train, Y_train,
                  batch_size=batch_size,
                  epochs    =epochs,
                  validation_data=(X_validation, Y_validation),
                  shuffle=True,
    	          callbacks=callbacks)
    else:
        print('Using real-time data augmentation.')

        # this will do preprocessing and realtime data augmentation
        datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
            width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=False)  # randomly flip images
        # compute quantities required for featurewise normalization
        # (std, mean, and principal components if ZCA whitening is applied)
        datagen.fit(X_train)

        # fit the model on the batches generated by datagen.flow()
        history = model.fit_generator(datagen.flow(X_train, Y_train,shuffle=True),
                            steps_per_epoch=X_train.shape[0]//batch_size,
                            #nb_val_samples=X_validation.shape[0],
                            epochs=epochs,
                            validation_data=(X_validation, Y_validation), callbacks=callbacks,)
    f= open(results, 'w')
    writer = csv.writer(f)
    writer.writerows(zip(history.history['loss'],history.history['val_loss']))
    f.close()

if __name__ == "__main__":
    import uuid
    train_id = str(uuid.uuid4()) #Use the same for a single experiment

    imageDatasetName = directories.numpyDataset+"imageDataset_f_300_b_300_M2U00001.npy"
    audioDatasetName = directories.numpyDataset+"audioDataset_f_300_b_300_M2U00001.npy"
    sample     = 20000
    test_size  = 0.1
    not_pretrained = False
    SGDOptimizer  = "SGD"
    epoch_schedule = [15,25,45]
    lr_list = [0.01,0.001,0.0001,0.0001]
    data_augmentation = False
    epochs=1
    batch_size = 64
    experiment_name = "CodeTest"

    denseModelName  = "DenseNet"
    depth = 3*2+4 #number of layers inside a dense block
    denseNetConf = {}
    denseNetConf["depth"]= 3*2+4
    denseNetConf["growthRate"]= 8
    denseNetConf["n_dense_blocks"] = 3
    denseNetConf["feature_map_n_list"] = [denseNetConf["growthRate"]]*\
                                            int((denseNetConf["depth"] - 4) / 3)
    assert (denseNetConf["depth"] - 4) % 3 == 0, "Depth must be 3 N + 4"
    denseNetConf["n_filter_initial"] = 16
    denseNetConf["droprate"] = 0.2
    denseNetConf["bnL2norm"] = 0.0001
    denseNetConf["l2_dense_penalization"] = 5e-5
    denseNetConf["nb_classes"] = 1
    denseNetConf["finalActivation"] = 'linear'
    #configuration_dict["input_shape"] Adicionado na hora de gerar modelo dentro da train

    kwargs = {"imageDatasetName":imageDatasetName,
                        "audioDatasetName":audioDatasetName,
                        "sample":sample,
                        "test_size":test_size,
                        "modelName":denseModelName,
                        "pretrained":not_pretrained,
                        "optimizer":SGDOptimizer,
                        "epoch_schedule":epoch_schedule,
                        "lr_list":lr_list,
                        "data_augmentation":data_augmentation,
                        "epochs":epochs,
                        "batch_size":batch_size,
                        "config_dict":denseNetConf,
                        "train_id":train_id,
                        "experiment_name":experiment_name}

    denseSGDTrainArgs = copy.deepcopy(kwargs)

    VGG16ModelName = 'VGG16'
    pretrained=True
    lr_list_pretrained = [1e-5,1e-6,1e-7,1e-8]
    VGG16Conf = {}
    VGG16Conf["finalActivation"] = 'linear'
    VGG16Conf["lastLayerDenseSize"] = 128
    VGG16SGDTrainArgs = copy.deepcopy(kwargs)
    VGG16SGDTrainArgs["modelName"] = VGG16ModelName
    VGG16SGDTrainArgs["lr_list"] = lr_list_pretrained
    VGG16SGDTrainArgs["config_dict"] = VGG16Conf

    ResNet50ModelName = "ResNet50"
    ResNet50Conf = {}
    ResNet50Conf["finalActivation"] = 'linear'
    ResNet50Conf["lastLayerDenseSize"] = 128
    batch_size_resnet50 = 32
    ResNet50SGDTrainArgs = copy.deepcopy(kwargs)
    ResNet50SGDTrainArgs["modelName"] = ResNet50ModelName
    ResNet50SGDTrainArgs["lr_list"] = lr_list_pretrained
    ResNet50SGDTrainArgs["config_dict"] = ResNet50Conf
    ResNet50SGDTrainArgs["batch_size"] = batch_size_resnet50


    train_network(**denseSGDTrainArgs)
    # train_network(**VGG16SGDTrainArgs)
    # train_network(**ResNet50SGDTrainArgs)
