import skvideo.io
import directories
import dir_utils
import matplotlib.pyplot as plt
import numpy as np

video_filename = "M2U00001.MPG"
video_filename_full = directories.resampled_120x120+video_filename
np_extension= "npy"
audio_filename = "audioDataset_"+dir_utils.filenameNoExtension(video_filename)+'.'+np_extension
audio_filename_full = directories.numpyDataset+audio_filename
audio_array = np.load(audio_filename_full)
videogen = skvideo.io.vreader(video_filename_full)
metadata = skvideo.io.ffprobe(video_filename_full)



fig = plt.figure(1,figsize=(13,7))
plt.ion()
plt.show()
axImg = fig.add_subplot(121)
axAud = fig.add_subplot(122)
window_size = 50
begin_plot = 0
x_array = np.arange(begin_plot,begin_plot+window_size,1).astype(float)
draw_list = list()
i=0
line,=axAud.plot(range(i,len(draw_list)+i),draw_list,'ko-')
#ignore first frame
frame = next(videogen)
axImg.imshow(frame)
for frame in videogen:
    print("frame "+str(i))
    draw_list.append(audio_array[i])
    if len(draw_list)>window_size:
        del draw_list[0]
    # print(draw_list)
    axImg.imshow(frame)
    line.set_data(1/29.97*np.array(range(i,len(draw_list)+i)),draw_list)
    axAud.relim()
    axAud.autoscale_view(True,True,True)
    i+=1
    plt.draw()
    plt.pause(0.05)
