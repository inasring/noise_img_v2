import matplotlib.pyplot as plt
import argparse
import pandas as pd
import numpy as np
# Ex.:python3 plot_results.py --path=../results/noiseImg__size_20000_2018-07-16.04\:35\:53.results

parser = argparse.ArgumentParser(description='Results ')
parser.add_argument('--path', type=str, nargs=1,
                    help=".results csv file with ['loss']['val_loss']")
args = parser.parse_args()
filename = args.path[0]
print(filename)

results = pd.read_csv(filename,header=None)
print(results)
# exit()
epoch_n = len(results[0])
epochs = np.arange(epoch_n)
# Plot train and test loss
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(epochs, results[0], 'b',label='train')
ax1.plot(epochs, results[1], 'r',label='validation')
ax1.set_xlabel('epoch')
ax1.set_ylabel('Loss')
ax1.legend()

plt.show()
