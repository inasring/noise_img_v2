import directories
import dir_utils
import time
import matplotlib.pyplot as plt
import numpy as np

# video_filename = "M2U00001.MPG"
# video_filename_full = directories.resampled_120x120+video_filename
# audio_extension= "wav"
# audio_filename = directories.audio+\
#                 dir_utils.filenameNoExtension(video_filename)+\
#                 '.'+audio_extension
#
# videogen = skvideo.io.vreader(video_filename_full)
# metadata = skvideo.io.ffprobe(video_filename_full)
# print(metadata.keys())
# print("AUDIO METADATA")
# print(json.dumps(metadata["audio"], indent=4))
# print("VIDEO METADATA")
# print(json.dumps(metadata["video"], indent=4))

# spWav.read(audio_filename)
# imageDatasetName = "imageDataset_M2U00001.npy"
# imageDatasetName = "imageDataset_48000_M2U00001.npy"
# audioDatasetName = "audioDataset_48000_M2U00001.npy"
# imageDatasetName = "imageDataset_48000_M2U00002.npy"
# audioDatasetName = "audioDataset_48000_M2U00002.npy"
# imageDatasetName = "imageDataset_f_300_b_300_M2U00002.npy"
# audioDatasetName = "audioDataset_f_300_b_300_M2U00002.npy"
imageDatasetName = "imageDataset_f_300_b_300_M2U00001.npy"
audioDatasetName = "audioDataset_f_300_b_300_M2U00001.npy"
# "imageDataset_48000_M2U00001.npy"

audioDataset = np.load(directories.numpyDataset+audioDatasetName)
imageDataset = np.load(directories.numpyDataset+imageDatasetName,
                        mmap_mode='r')
print("Any nan in audio dataset:",np.isnan(audioDataset).any())
# np.isnan(imageDataset).any()


fig = plt.figure(1,figsize=(13,7))
plt.ion()
plt.show()
axImg = fig.add_subplot(121)
axAud = fig.add_subplot(122)
window_size = 500
begin_plot = 0
x_array = np.arange(begin_plot,begin_plot+window_size,1).astype(float)
draw_list = list()
i=0
line,=axAud.plot(range(i,len(draw_list)+i),draw_list,'ko-')
im   =axImg.imshow(imageDataset[0])
axAud.set_ylim(ymin=min(audioDataset),ymax=max(audioDataset))
# exit(1)
#ignore first frame
# axImg.imshow(frame)
# t = 1/29.97*np.array(range(i,len(draw_list)+i))
for i in range(len(imageDataset)):
    start = time.time()
    draw_list.append(audioDataset[i])
    if len(draw_list)>window_size:
        del draw_list[0]
    t = 1/29.97*np.array(range(i,len(draw_list)+i))
    # print(draw_list)
    im.set_data(imageDataset[i]/255)
    # im   =axImg.imshow(imageDataset[i])
    # line.set_data(t,draw_list)
    line.set_data(1/29.97*np.array(range(i,len(draw_list)+i)),draw_list)
    axAud.relim()
    # axAud.autoscale_view(True,True,True)
    # autoscale_view(tight=None, scalex=True, scaley=True)
    axAud.autoscale_view(tight=None, scalex=True)
    i+=1
    plt.draw()
    plt.pause(0.005)
    end = time.time()
    if i%100 == 0:
        print("plot time = ", end-start)
    # if i>100:
        # break
