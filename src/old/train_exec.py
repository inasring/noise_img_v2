import directories
execute_str           = "python3"
imageDataset          = "--imageDataset="+directories.numpyDataset
audioDataset          = "--audioDataset="+directories.numpyDataset
DenseNetFlag          = "--DenseNet=True"
ResNet50Flag          = "--ResNet50=True"
VGG16Flag             = "--VGG16=True"
SGDFlag               = "--SGD=True"
AdamFlag              = "--Adam=True"
Data_augmentationFlag = "--Data_augmentation=True"
nb_epochs = 1
epochs  = "--epochs="+str(nb_epochs)

imageDataset_1 =imageDataset+"imageDataset_f_300_b_300_M2U00001.npy"
audioDataset_1 =audioDataset+"audioDataset_f_300_b_300_M2U00001.npy"

execute_1 = execute_str+" "+\
            "train_generic.py"+" "+\
            imageDataset_1+" "+\
            audioDataset_1+" "+\
            DenseNetFlag+" "+\
            SGDFlag+" "+\
            epochs

execute_2 = execute_str+" "+\
            "train_generic.py"+" "+\
            imageDataset_1+" "+\
            audioDataset_1+" "+\
            VGG16Flag+" "+\
            SGDFlag+" "+\
            epochs
execute_3 = execute_str+" "+\
            "train_generic.py"+" "+\
            imageDataset_1+" "+\
            audioDataset_1+" "+\
            DenseNetFlag+" "+\
            AdamFlag+" "+\
            epochs
execute_4 = execute_str+" "+\
            "train_generic.py"+" "+\
            imageDataset_1+" "+\
            audioDataset_1+" "+\
            VGG16Flag+" "+\
            AdamFlag+" "+\
            epochs

import subprocess
# execution_list = [execute_1,execute_2,execute_3,execute_4]
execution_list = [execute_1,execute_2]
print("execute_1=",execute_1)
for i,execution in enumerate(execution_list):
        print("run:",i," Execution:",execution)
        subprocess.call(execution, shell=True)
