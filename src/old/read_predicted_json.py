import os
import sys
import numpy as np
import json
import directories
import dir_utils
import matplotlib.pyplot as plt
import scipy.signal as spsignal

# modelMetadataName = "noiseImg__size_20000_2018-07-16.05:41:46_predicted_2018-07-16.07:55:05.json"##ResNet50
# modelMetadataName = "noiseImg__size_20000_2018-07-16.08:04:52_predicted_2018-07-16.08:20:19.json"##DenseNet
# modelMetadataName = "noiseImg__size_20000_2018-07-22.14:22:39_predicted_2018-07-22.15:21:18.json"##ResNet50


# if not os.path.exists(sys.argv[1]):
#     sys.exit('ERROR: Database %s was not found!' % sys.argv[1])
#Example python3 test_model.py ../predicted/noiseImg__size_20000_2018-07-22.14:22:39_predicted_2018-07-22.15:21:18.json
# modelMetadataName = sys.argv[1]

# modelMetadataName = "noiseImg__size_20000_2018-07-22.20:53:01_predicted_2018-07-23.09:48:13.json" #Dense SGD
# modelMetadataName = "noiseImg__size_20000_2018-07-22.21:37:26_predicted_2018-07-23.09:51:11.json" #VGG16 SGD
# modelMetadataName = "noiseImg__size_20000_2018-07-22.23:07:58_predicted_2018-07-23.09:54:48.json" #Dense Adam
# modelMetadataName = "noiseImg__size_20000_2018-07-22.23:52:43_predicted_2018-07-23.09:57:43.json" #VGG16 Adam



#Testes Modelos
#50 epochs
modelMetadataName = "noiseImg__size_20000_2018-07-22.20:53:01_predicted.json" #Dense SGD
# modelMetadataName = "noiseImg__size_20000_2018-07-22.21:37:26_predicted.json" #VGG16 SGD
# modelMetadataName = "noiseImg__size_20000_2018-07-22.23:07:58_predicted.json" #Dense Adam
# modelMetadataName = "noiseImg__size_20000_2018-07-22.23:52:43_predicted.json" #VGG16 Adam

#2 epochs
# modelMetadataName = "noiseImg__size_20000_2018-07-23.14:31:09_predicted.json" #Dense SGD
# modelMetadataName = "noiseImg__size_20000_2018-07-23.14:33:11_predicted.json" #VGG16 SGD
# modelMetadataName = "noiseImg__size_20000_2018-07-23.14:37:05_predicted.json" #Dense Adam
# modelMetadataName = "noiseImg__size_20000_2018-07-23.14:39:10_predicted.json" #VGG16 Adam

expsList=["noiseImg__size_20000_2018-07-22.20:53:01",\
  "noiseImg__size_20000_2018-07-22.21:37:26",\
   "noiseImg__size_20000_2018-07-22.23:07:58",\
    "noiseImg__size_20000_2018-07-22.23:52:43",\
     "noiseImg__size_20000_2018-07-23.14:31:09",\
      "noiseImg__size_20000_2018-07-23.14:33:11",\
       "noiseImg__size_20000_2018-07-23.14:37:05",\
        "noiseImg__size_20000_2018-07-23.14:39:10"]
pre = """\\begin{frame}\\begin{figure}
	\includegraphics[width=\linewidth]{{./graphs/"""
pos ="""}
\\end{figure}\\end{frame}"""
l = [pre+i+"}.eps"+pos for i in expsList]
for i in l:
    print(i)

usingAdam = False
usingSGD  = False

if modelMetadataName ==  "noiseImg__size_20000_2018-07-22.23:07:58_predicted.json" or\
    modelMetadataName == "noiseImg__size_20000_2018-07-22.23:52:43_predicted.json" or\
    modelMetadataName == "noiseImg__size_20000_2018-07-23.14:37:05_predicted.json" or\
    modelMetadataName == "noiseImg__size_20000_2018-07-23.14:39:10_predicted.json":
    usingAdam=True

if modelMetadataName == "noiseImg__size_20000_2018-07-22.20:53:01_predicted.json" or\
    modelMetadataName == "noiseImg__size_20000_2018-07-22.21:37:26_predicted.json" or\
    modelMetadataName == "noiseImg__size_20000_2018-07-23.14:31:09_predicted.json" or\
    modelMetadataName == "noiseImg__size_20000_2018-07-23.14:33:11_predicted.json":
    usingSGD = True

def optToString(using_adam,using_sgd):
    if using_adam:
        return "Adam"
    if using_sgd:
        return "SGD"

with open(directories.predicted+modelMetadataName) as f:
    data = json.load(f)
    modelPath = data["modelPath"]
    modelJson = dir_utils.filenameNoExtension(modelPath)+'.json'
    print (data.keys())
    with open(modelJson) as fmodel:
        modelJson_dat = json.load(fmodel)
        print(modelJson_dat)
    print(data.keys())

networkType = None
# try:
#     if modelJson_dat["usingPretrained"]:
#             networkType = modelJson_dat["pretrainedName"]
#             print(networkType)
# except KeyError:
#     print("passing keyerror on pretrained")
try:
    if modelJson_dat["pretrainedName"] is not None:
            networkType = modelJson_dat["pretrainedName"]
            print(networkType)
except KeyError:
    print("passing keyerror on pretrained")


try:
    # if "usingDenseNet" in modelJson_dat.keys():
    if modelJson_dat["usingDenseNet"]:
            networkType = "DenseNet"
            print(networkType)
except KeyError:
    print("passing key error on dense")
    # if  "usingPretrained" in modelJson_dat.keys():
# print(data)
predTraining    = data["predictionsTraining"]
predValidation  = data["predictionsValidation"]
audioDatasetName= data["audioDatasetName"]
ValidationRangeBegin= data["ValidationRangeBegin"]
try:
    usingAdam       = modelJson_dat["usingAdam"]
except KeyError:
    print("passing usingAdam key error")
try:
    usingSGD       = modelJson_dat["usingSGD"]
except KeyError:
    print("passing usingSGD key error")

# results        =
audioDataset = np.load(audioDatasetName)

optimizer = optToString(usingAdam,usingSGD)

resultsFname = directories.resDir\
            +dir_utils.filenameNoPath(\
            dir_utils.filenameNoExtension(modelPath))+\
            ".results"
results = np.loadtxt(resultsFname,delimiter=',')
print(results.shape)


fig1 = plt.figure(figsize=(20, 7))
ax1 = fig1.add_subplot(121)
tTraining = np.arange(len(predTraining))
tAudio    = np.arange(len(audioDataset))
ax1.plot(tAudio,audioDataset,'k',label='Ground Trouth')

ax1.plot(tTraining,predTraining, 'b',label='train')

tValidation = np.arange(ValidationRangeBegin,
                        ValidationRangeBegin+len(predValidation))
ax1.plot(tValidation, predValidation, 'r',label='validation')

ax2 = fig1.add_subplot(122)
loss = results[:,0]
val_loss = results[:,1]
nepochs = len(loss)
epochs = range(nepochs)
ax2.plot(epochs,loss,label='loss')
ax2.plot(epochs,val_loss,label='val_loss')
ax2.legend()
ax2.set_xlabel('epoch')
ax2.set_title("Min val loss:"+("{0:.4f}".format(float(min(val_loss))))  )

# kernel_size=31
# filteredValidation = spsignal.medfilt(predValidation,kernel_size=kernel_size)
# filteredTraining   = spsignal.medfilt(predTraining,kernel_size=kernel_size)
# ax1.plot(tValidation, filteredValidation, 'r-',label='validation filtered')
# ax1.plot(tTraining,filteredTraining, 'b--',label='train filtered')

ax1.set_xlabel('sample')
ax1.legend()
ax1.set_title(networkType+" "+optimizer)

savefigDir = "../presentation/30062018/apresentacao/graphs/"
graph_name = dir_utils.filenameNoPath(\
                dir_utils.filenameNoExtension(modelPath))+\
                    ".eps"
graph_name_full = savefigDir+graph_name
saveFig=True
if saveFig:
    print("saving figure to:",graph_name_full)
    fig1.savefig(graph_name_full,bbox_inches='tight', pad_inches=0)

# plt.show()
