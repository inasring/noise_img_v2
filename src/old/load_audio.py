import numpy as np
import directories
import matplotlib.pyplot as plt

audioFile = "audioDataset_48000_M2U00001.npy"
audioFile2 = "audioDataset_48000_M2U00002.npy"
audioData = np.load(directories.numpyDataset+audioFile)
audioData_2 = np.load(directories.numpyDataset+audioFile2)
fig = plt.figure(1)
ax  = fig.add_subplot(111)
ax.plot(audioData)
ax.plot(audioData_2,'r')
plt.show()
