import json
import dir_utils
import directories
# training_data = {
#         "sample":sample,
#         "date":string_date,
#         "cutPosition":cutPosition,
#         "random_state":rand_state,
#         "imageDatasetName":imageDatasetName,
#         "audioDatasetName":audioDatasetName,
#         "meansTrainX":list(meansTrain),
#         "stdTrainX":list(stdsTrain),
#         "meansTrainY":yMean,
#         "stdTrainY":yStd,
#         "test_size":test_size,
#         "results":results,
#         "modelPath":filepath,
#         "data_augmentation":data_augmentation,
#         "epochs":nb_epoch,
#         "batch_size":batch_size,
#         "droprate":droprate,
#         "n_filter_initial":k0,
#         "depth":depth,
#         "growthRate":k,
#         "n_dense_blocks":n_dense_blocks
# }

# modelMetadataName = "noiseImg__size_20000_2018-07-16.04:35:53.json"
modelMetadataName = "noiseImg__size_20000_2018-07-16.08:04:52.json"
with open(directories.modelDir+modelMetadataName) as f:
    data = json.load(f)
    datasetJson = dir_utils.filenameNoExtension(data['audioDatasetName'])+'.json'
    datasetJson = directories.numpyDataset+datasetJson
    with open(datasetJson) as fdataset:
        datasetJson_dat = json.load(fdataset)
        print(datasetJson_dat)
# print(data)
