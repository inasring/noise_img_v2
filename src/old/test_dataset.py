import numpy as np
import directories
import matplotlib.pyplot as plt
import utils
from keras.models import load_model
import json
with open(directories.modelDir+'noiseImg__size_20000_2018-07-09.02:32:04.json') as f:
    data = json.load(f)
meanY = data["meansTrainY"]
stdY = data["stdTrainY"]

# the data, shuffled and split between train and test sets
# imageDatasetName = "imageDataset_48000_M2U00001.npy"
# audioDatasetName = "audioDataset_48000_M2U00001.npy"
# imageDatasetName = "imageDataset_48000_M2U00002.npy"
# audioDatasetName = "audioDataset_48000_M2U00002.npy"
# model_name = "noiseImg__size_20000_2018-07-08.23:50:58.h5"
model_name = "noiseImg__size_20000_2018-07-09.02:32:04.h5"
print("Loading dataset...")
audioDataset = np.load(directories.numpyDataset+audioDatasetName)
imageDataset = np.load(directories.numpyDataset+imageDatasetName,
                        mmap_mode='r')
imgData = np.copy(imageDataset[0:20000])
model = load_model(directories.modelDir+model_name)
mean =  np.array([118.801216, 112.43616,  121.68487 ])
std  =  np.array([56.079796, 50.14596,  53.820072])
print("Preprocessing...")
imageData      = utils.pre_proc(imgData, mean,
                       std, dim_ordering='tf')
print(imgData.shape)
print("Predicting...")
predictions = model.predict(imageData)
plt.plot(audioDataset-audioDataset.mean(),'k')
predProc = (predictions*stdY)+meanY
plt.plot(predProc-predProc.mean(),'r')

fig2 = plt.figure(2)
ax1  = fig2.add_subplot(111)
ax1.plot(audioDataset,'k')
ax1.plot(predProc,'r')

plt.show()
