import os
import sys
import numpy as np
import json
import directories
import dir_utils
import matplotlib.pyplot as plt
import scipy.signal as spsignal

# expsList=["noiseImg_size_20000_2018-08-31.15:00:05",
#             "noiseImg_size_20000_2018-08-31.15:01:56",
#             "noiseImg_size_20000_2018-08-31.15:02:59",
#             "noiseImg_size_20000_2018-08-31.15:04:53",
#             "noiseImg_size_20000_2018-08-31.15:14:13"]
expsList=["noiseImg_size_20000_2018-08-31.15:00:05",
            "noiseImg_size_20000_2018-08-31.15:01:56",
            "noiseImg_size_20000_2018-08-31.15:02:59",
            "noiseImg_size_20000_2018-08-31.15:04:53"]
            # "noiseImg_size_20000_2018-08-31.15:14:13"] BUG: Não gera .results quando chamado por test


dateStr = "10092018"
dateStr = "20082018"
pre = """\\begin{frame}\\begin{figure}
	\includegraphics[width=\linewidth]{{./graphs/"""+dateStr+"""/"""
pos ="""}
\\end{figure}\\end{frame}"""
l = [pre+i+"}.eps"+pos for i in expsList]
for i in l:
    print(i)

def optToString(using_adam,using_sgd):
    if using_adam:
        return "Adam"
    if using_sgd:
        return "SGD"

for modelMetadataName in expsList:
    modelMetadataName = modelMetadataName+"_predicted.json"
    with open(directories.predicted+modelMetadataName) as f:
        data = json.load(f)
        modelPath = data["modelPath"]
        modelJson = dir_utils.filenameNoExtension(modelPath)+'.json'
        print("Predicted Json")
        print (data.keys())
        with open(modelJson) as fmodel:
            print("Model Json")
            modelJson_dat = json.load(fmodel)
            print(modelJson_dat)
        print(data.keys())
    # exit(1)
    networkType = None
    # try:
    #     if modelJson_dat["usingPretrained"]:
    #             networkType = modelJson_dat["pretrainedName"]
    #             print(networkType)
    # except KeyError:
    #     print("passing keyerror on pretrained")
    networkType = modelJson_dat["modelName"]

        # if  "usingPretrained" in modelJson_dat.keys():
    # print(data)
    predTraining    = data["predictionsTraining"]
    predValidation  = data["predictionsValidation"]
    audioDatasetName= data["audioDatasetName"]
    ValidationRangeBegin= data["ValidationRangeBegin"]
    # results        =
    audioDataset = np.load(audioDatasetName)

    print(modelJson_dat["optimizer"])
    optimizer = modelJson_dat["optimizer"]

    resultsFname = directories.resDir\
                +dir_utils.filenameNoPath(\
                dir_utils.filenameNoExtension(modelPath))+\
                ".results"
    results = np.loadtxt(resultsFname,delimiter=',')
    print(results.shape)


    fig1 = plt.figure(figsize=(20, 7))
    ax1 = fig1.add_subplot(121)
    tTraining = np.arange(len(predTraining))
    tAudio    = np.arange(len(audioDataset))
    ax1.plot(tAudio,audioDataset,'k',label='Ground Trouth')

    ax1.plot(tTraining,predTraining, 'b',label='train')

    tValidation = np.arange(ValidationRangeBegin,
                            ValidationRangeBegin+len(predValidation))
    ax1.plot(tValidation, predValidation, 'r',label='validation')

    ax2 = fig1.add_subplot(122)
    loss = results[:,0]
    val_loss = results[:,1]
    nepochs = len(loss)
    epochs = range(nepochs)
    ax2.plot(epochs,np.log(loss),label='log loss')
    ax2.plot(epochs,np.log(val_loss),label='log val_loss')
    ax2.legend()
    ax2.set_xlabel('epoch')
    ax2.set_title("Min val loss:"+("{0:.4f}".format(float(min(val_loss))))  )

    # kernel_size=31
    # filteredValidation = spsignal.medfilt(predValidation,kernel_size=kernel_size)
    # filteredTraining   = spsignal.medfilt(predTraining,kernel_size=kernel_size)
    # ax1.plot(tValidation, filteredValidation, 'r-',label='validation filtered')
    # ax1.plot(tTraining,filteredTraining, 'b--',label='train filtered')

    ax1.set_xlabel('sample')
    ax1.legend()
    train_all = " Trained all"
    train_last= " onlyLastTrained"

    train_last_only=False
    if "train_last_only" in modelJson_dat.keys():
        if modelJson_dat["train_last_only"]:
            train_mode=train_last
            train_last_only = True
    if not train_last_only:
        train_mode = train_all

    ax1.set_title(networkType+" "+optimizer+" "+str(modelJson_dat["lastLayerDenseSize"])+ train_mode)

    savefigDir = "../presentation/30062018/apresentacao/graphs/"+dateStr+"/"
    dir_utils.make_dir(savefigDir)
    graph_name = dir_utils.filenameNoPath(\
                    dir_utils.filenameNoExtension(modelPath))+\
                        ".eps"
    graph_name_full = savefigDir+graph_name
    saveFig=True
    if saveFig:
        print("saving figure to:",graph_name_full)
        fig1.savefig(graph_name_full,bbox_inches='tight', pad_inches=0)
show = True
if show:
    plt.show()
