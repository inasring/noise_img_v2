import json
import directories
from pprint import pprint
#To run
# Go to directory src
# Run python3.5 -m json_test.print_json_ignore 

fname = "noiseImg_size_20000_2018-08-17.16:17:22_predicted.json"
fname = directories.predicted+fname

def load_json(name):
    with open(fname) as f:
        data = json.load(f)
    return data

def remove_entries(dic,keys,inplace=True):
    if inplace:
        r = dic
    else:
        r = dict(dic)
    for key in keys:
        del r[key]
    return r

dic = load_json(fname)
print (dic.keys())
remove_entries(dic,["predictionsValidation","predictionsTraining"])
pprint(dic)
