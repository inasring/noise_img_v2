import directories
import dir_utils
import time
import matplotlib.pyplot as plt
import numpy as np
from keras.models import load_model

# video_filename = "M2U00001.MPG"
# video_filename_full = directories.resampled_120x120+video_filename
# audio_extension= "wav"
# audio_filename = directories.audio+\
#                 dir_utils.filenameNoExtension(video_filename)+\
#                 '.'+audio_extension
#
# videogen = skvideo.io.vreader(video_filename_full)
# metadata = skvideo.io.ffprobe(video_filename_full)
# print(metadata.keys())
# print("AUDIO METADATA")
# print(json.dumps(metadata["audio"], indent=4))
# print("VIDEO METADATA")
# print(json.dumps(metadata["video"], indent=4))

# spWav.read(audio_filename)
# imageDatasetName = "imageDataset_M2U00001.npy"
imageDatasetName = "imageDataset_48000_M2U00001.npy"
audioDatasetName = "audioDataset_48000_M2U00001.npy"
#Criar json com meta dados
mean =  np.array([118.801216, 112.43616,  121.68487 ])
std  =  np.array([56.079796, 50.14596,  53.820072])
model_name = "noiseImg__size_20000_2018-07-08.23:50:58.h5"
model = load_model(directories.modelDir+model_name)
# imageDatasetName = "imageDataset_48000_M2U00002.npy"
# audioDatasetName = "audioDataset_48000_M2U00002.npy"
# "imageDataset_48000_M2U00001.npy"

audioDataset = np.load(directories.numpyDataset+audioDatasetName)
imageDataset = np.load(directories.numpyDataset+imageDatasetName,
                        mmap_mode='r')
print(imageDataset[0])
print(imageDataset[0].shape)
# exit()
# imageDataset = np.load(directories.numpyDataset+imageDatasetName)
print("Any nan in audio dataset:",np.isnan(audioDataset).any())
# np.isnan(imageDataset).any()


fig = plt.figure(1,figsize=(13,7))
plt.ion()
plt.show()
axImg = fig.add_subplot(121)
axAud = fig.add_subplot(122)
window_size = 500
begin_plot = 0
x_array = np.arange(begin_plot,begin_plot+window_size,1).astype(float)
draw_list = list()
predicted_draw_list = list()
i=0
line,=axAud.plot(range(i,len(draw_list)+i),draw_list,'ko-')
linePredicted,=axAud.plot(range(i,len(predicted_draw_list)+i),predicted_draw_list,'ro-')
im   =axImg.imshow(imageDataset[0])
#ignore first frame
# axImg.imshow(frame)
# t = 1/29.97*np.array(range(i,len(draw_list)+i))
for i in range(len(imageDataset)):
    start = time.time()
    tmp = np.copy(imageDataset[None,i,:,:,:])
    for channel in range(3):
        # imageDataset[i][:,:,channel]   =  imageDataset[i][:,:,channel]-mean[channel]
        # imageDataset[i][:,:,channel]   =  imageDataset[i][:,:,channel]/std[channel]
        tmp[None,:,:,channel]   =  tmp[None,:,:,channel]-mean[channel]
        tmp[None,:,:,channel]   =  tmp[None,:,:,channel]/std[channel]
    # imageDataset[i] =     np.expand_dims(imageDataset[i],axis=0)
    predictedAudio =  model.predict(tmp)

    draw_list.append(audioDataset[i])
    predicted_draw_list.append(predictedAudio)
    if len(draw_list)>window_size:
        del draw_list[0]
    if len(predicted_draw_list)>window_size:
        del predicted_draw_list[0]
    print("real:",draw_list)
    print("predicted:",predicted_draw_list)
    t = 1/29.97*np.array(range(i,len(draw_list)+i))
    # print(draw_list)
    im.set_data(imageDataset[i])
    # im   =axImg.imshow(imageDataset[i])
    # line.set_data(t,draw_list)
    line.set_data(1/29.97*np.array(range(i,len(draw_list)+i)),draw_list)
    linePredicted.set_data(1/29.97*np.array(range(i,len(predicted_draw_list)+i)),predicted_draw_list)
    axAud.relim()
    axAud.autoscale_view(True,True,True)
    i+=1
    plt.draw()
    plt.pause(0.005)
    end = time.time()
    if i%100 == 0:
        print("plot time = ", end-start)
    # if i>100:
        # break
