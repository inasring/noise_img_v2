import numpy as np
def pre_proc(img_array, means, stds, dim_ordering='tf'):
    # Normalize image array
    # Returns normalized array, mean and standard deviation
    # [par][ind][M][N][cor]

    img_dim = img_array.shape[1:]
    if dim_ordering == "tf":
        n_channels = img_dim[-1]
        for channel in range(n_channels):
            img_array[:, :, :, channel] = (img_array[:, :, :, channel] - means[channel]) / stds[channel]
    else:
        raise ValueError("Unknown dim ordering",dim_ordering)
    return img_array

def channels_MeanStd(img_array, dim_ordering='tf'):
        means = []
        stds = []
        img_dim = img_array.shape[1:]
        if dim_ordering == "tf":
            n_channels = img_dim[-1]
            for i in range(n_channels):
                mean = np.mean(img_array[:, :, :, i]) #using train only mean and std
                std = np.std(img_array[:, :, :, i])
                means.append(mean)
                stds.append(std)
        else:
            raise ValueError("Unknown dim ordering",dim_ordering)
        return np.array(means), np.array(stds)

def combine_means(mean_1,n_1,mean_2,n_2):
    return (mean_1*n_1+mean_2*n_2)/(n_1+n_2)

def combine_stds(std_1,mean_1,n_1,std_2,mean_2,n_2):
    mean_c = combine_means(mean_1,n_1,mean_2,n_2)
    var_1 = std_1**2
    var_2 = std_2**2
    factor_1 = n_1 * (var_1  + (mean_1 - mean_c)**2)
    factor_2 = n_2 * (var_2  + (mean_2 - mean_c)**2)
    var_c = (factor_1+factor_2)/(n_1+n_2)
    return np.sqrt(var_c)
