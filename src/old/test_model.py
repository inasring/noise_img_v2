import json
import utils
import numpy as np
import directories
import dir_utils
from keras.models import load_model
import matplotlib.pyplot as plt
# from datetime import datetime
# import sys
# import os


def predict_model(modelMetadataName,save=True):
    with open(directories.modelDir+modelMetadataName) as f:
        Metadata = json.load(f)
    audioDatasetName = Metadata["audioDatasetName"] # Change here for different datasets
    imageDatasetName = Metadata["imageDatasetName"] #
    sample           = Metadata["sample"]
    modelPath        = Metadata["modelPath"]

    dir_utils.make_dir(directories.predicted)
    predictedDataFname = dir_utils.filenameNoPath(dir_utils.filenameNoExtension(modelMetadataName))
    # string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
    # predictedDataFname=predictedDataFname+"_predicted_"+string_date+".json"
    predictedDataFname=predictedDataFname+"_predicted.json"
    # audioDataset = np.load(audioDatasetName)
    imageDataset = np.load(imageDatasetName,
                            mmap_mode='r')
    # nsamples = int(sample//100)
    nsamples = sample
    assert(nsamples<=sample)
    # model = load_model(directories.modelDir+model_name)
    print(Metadata)
    model = load_model(modelPath)

    trainRangeBegin = 0
    trainRangeEnd   = nsamples
    imgDataTraining     = np.copy(imageDataset[trainRangeBegin:trainRangeEnd])
    # audioDataTraining   = audioDataset[trainRangeBegin:trainRangeEnd]
    imgDataValidation   = np.copy(imageDataset[sample:sample+nsamples])
    # audioDataValidation = audioDataset[sample:sample+nsamples]

    print("Preprocessing...")
    meansTrainX = Metadata["meansTrainX"]
    stdTrainX   = Metadata["stdTrainX"]
    imgDataProcessedTraining   = utils.pre_proc(imgDataTraining, meansTrainX,
                                stdTrainX, dim_ordering='tf')
    del imgDataTraining
    predictionsTraining = model.predict(imgDataProcessedTraining)
    del imgDataProcessedTraining
    imgDataProcessedValidation = utils.pre_proc(imgDataValidation, meansTrainX,
                                stdTrainX, dim_ordering='tf')
    del imgDataValidation
    predictionsValidation = model.predict(imgDataProcessedValidation)
    del imgDataProcessedValidation
    tTraining = np.arange(0,nsamples)
    tValidation = np.arange(sample,sample+len(predictionsValidation))
    to_python_scalar = lambda x:x.item()
    predDataTraining      = list (map(to_python_scalar,predictionsTraining))
    predDataValidation = list (map(to_python_scalar,predictionsValidation))
    sample           = Metadata["sample"]
    modelPath        = Metadata["modelPath"]
    predictionMetadata = {
            "sample":sample,
            "nsamples":nsamples,
            "imageDatasetName":imageDatasetName,
            "audioDatasetName":audioDatasetName,
            "predictionsTraining":predDataTraining,
            "trainRangeBegin":trainRangeBegin,
            "trainRangeEnd":trainRangeEnd,
            "predictionsValidation":predDataValidation,
            "ValidationRangeBegin":sample,
            "ValidationRangeEnd":len(predictionsValidation),
            "modelPath":modelPath
    }
    if save:
        with open(directories.predicted+predictedDataFname, 'w') as outfile:
            json.dump(predictionMetadata, outfile)

    return tTraining,predictionsTraining,tValidation,predictionsValidation


if __name__ == '__main__':
    modelMetadataName = 'noiseImg_size_20000_2018-07-30.21:08:53.json'
    print("Predicting...")
    tTraining,predictionsTraining,tValidation,predictionsValidation = \
            predict_model(modelMetadataName,save=True)
    with open(directories.modelDir+modelMetadataName) as f:
        Metadata = json.load(f)
    audioDatasetName = Metadata["audioDatasetName"] # Change here for different datasets
    audioDataset = np.load(audioDatasetName)
    fig1 = plt.figure(1)
    ax1  = fig1.add_subplot(111)
    ax1.plot(tTraining,predictionsTraining,'r')
    print("tValidation shape:",tValidation.shape)
    print("predictionsValidation:",predictionsValidation)
    ax1.plot(tValidation,predictionsValidation,'b')
    ax1.plot(audioDataset,'k')

    fig2 = plt.figure(2)
    ax2  = fig2.add_subplot(111)
    ax2.plot(tValidation,predictionsValidation,'b')
    ax2.plot(tTraining,predictionsTraining,'r')

    plt.show()
