import json
import utils
import numpy as np
import directories
import dir_utils
from keras.models import load_model,Model
from keras.layers import Flatten,Input
# import matplotlib.pyplot as plt
# from datetime import datetime
# import sys
# import os
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)

def predict_model(modelMetadataName,savedir=directories.predicted,save=True):
    with open(directories.modelDir+modelMetadataName) as f:
        Metadata = json.load(f)
    audioDatasetName = Metadata["audioDatasetName"] # Change here for different datasets
    imageDatasetName = Metadata["imageDatasetName"] #
    sample           = Metadata["sample"]
    modelPath        = Metadata["modelPath"]

    dir_utils.make_dir(savedir)
    predictedDataFname = dir_utils.filenameNoPath(dir_utils.filenameNoExtension(modelMetadataName))
    # string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
    # predictedDataFname=predictedDataFname+"_predicted_"+string_date+".json"
    predictedDataFname=predictedDataFname+"_predicted.json"
    # audioDataset = np.load(audioDatasetName)
    imageDataset = np.load(imageDatasetName,
                            mmap_mode='r')
    # nsamples = int(sample//100)
    nsamples = sample
    assert(nsamples<=sample)
    # model = load_model(directories.modelDir+model_name)
    print(Metadata)
    model = load_model(modelPath)

    trainRangeBegin = 0
    trainRangeEnd   = nsamples
    imgDataTraining     = np.copy(imageDataset[trainRangeBegin:trainRangeEnd])
    # audioDataTraining   = audioDataset[trainRangeBegin:trainRangeEnd]
    imgDataValidation   = np.copy(imageDataset[sample:sample+nsamples])
    # audioDataValidation = audioDataset[sample:sample+nsamples]

    print("Preprocessing...")
    meansTrainX = Metadata["meansTrainX"]
    stdTrainX   = Metadata["stdTrainX"]
    imgDataProcessedTraining   = utils.pre_proc(imgDataTraining, meansTrainX,
                                stdTrainX, dim_ordering='tf')
    del imgDataTraining
    predictionsTraining = model.predict(imgDataProcessedTraining)
    del imgDataProcessedTraining
    imgDataProcessedValidation = utils.pre_proc(imgDataValidation, meansTrainX,
                                stdTrainX, dim_ordering='tf')
    del imgDataValidation
    predictionsValidation = model.predict(imgDataProcessedValidation)
    del imgDataProcessedValidation
    tTraining = np.arange(0,nsamples)
    tValidation = np.arange(sample,sample+len(predictionsValidation))
    to_python_scalar = lambda x:x.item()
    predDataTraining      = list (map(to_python_scalar,predictionsTraining))
    predDataValidation = list (map(to_python_scalar,predictionsValidation))
    sample           = Metadata["sample"]
    modelPath        = Metadata["modelPath"]
    predictionMetadata = {
            "sample":sample,
            "nsamples":nsamples,
            "imageDatasetName":imageDatasetName,
            "audioDatasetName":audioDatasetName,
            "predictionsTraining":predDataTraining,
            "trainRangeBegin":trainRangeBegin,
            "trainRangeEnd":trainRangeEnd,
            "predictionsValidation":predDataValidation,
            "ValidationRangeBegin":sample,
            "ValidationRangeEnd":len(predictionsValidation),
            "modelPath":modelPath
    }
    if save:
        with open(savedir+predictedDataFname, 'w') as outfile:
            json.dump(predictionMetadata, outfile)

    return tTraining,predictionsTraining,tValidation,predictionsValidation


def predict_model_ll(modelMetadataName,savedir,save=True):
    with open(directories.modelDir+modelMetadataName) as f:
        Metadata = json.load(f)
    audioDatasetName = Metadata["audioDatasetName"] # Change here for different datasets
    imageDatasetName = Metadata["imageDatasetName"] #
    sample           = Metadata["sample"]
    modelPath        = Metadata["modelPath"]

    model_name = Metadata["modelName"]
    if model_name=="VGG16":
        from keras.applications import VGG16
        orig_model = VGG16(include_top=False, weights='imagenet',input_shape=Metadata["input_shape"])
        predictions = Flatten()(orig_model.output)
        base_model = Model(inputs=orig_model.input,outputs=predictions)
    if model_name=="ResNet50":
        print("Entering ResNet50")
        from keras.applications import ResNet50
        from keras.layers       import UpSampling2D
        upscaleFactor  = 2
        upscaled_shape = tuple([Metadata["input_shape"][0]*upscaleFactor,
                                Metadata["input_shape"][1]*upscaleFactor,
                                Metadata["input_shape"][2]])
        orig_model = ResNet50(include_top=False, weights='imagenet', input_shape=upscaled_shape)
        input_layer = Input(shape=Metadata["input_shape"])
        x = UpSampling2D(size=(upscaleFactor,upscaleFactor))(input_layer)
        x = orig_model(x)
        predictions = Flatten()(x)
        base_model = Model(inputs=input_layer,outputs=predictions)

    dir_utils.make_dir(savedir)
    predictedDataFname = dir_utils.filenameNoPath(dir_utils.filenameNoExtension(modelMetadataName))
    predictedDataFname=predictedDataFname+"_predicted.json"
    imageDataset = np.load(imageDatasetName,
                            mmap_mode='r')
    # nsamples = int(sample//100)
    nsamples = sample
    assert(nsamples<=sample)
    # model = load_model(directories.modelDir+model_name)
    print(Metadata)
    model = load_model(modelPath)

    trainRangeBegin = 0
    trainRangeEnd   = nsamples
    imgDataTraining     = np.copy(imageDataset[trainRangeBegin:trainRangeEnd])
    # audioDataTraining   = audioDataset[trainRangeBegin:trainRangeEnd]
    imgDataValidation   = np.copy(imageDataset[sample:sample+nsamples])
    # audioDataValidation = audioDataset[sample:sample+nsamples]

    print("Preprocessing...")
    meansTrainX = Metadata["meansTrainX"]
    stdTrainX   = Metadata["stdTrainX"]
    imgDataProcessedTraining   = utils.pre_proc(imgDataTraining, meansTrainX,
                                stdTrainX, dim_ordering='tf')
    del imgDataTraining
    predictionsTraining = base_model.predict(imgDataProcessedTraining)
    predictionsTraining = model.predict(predictionsTraining)
    del imgDataProcessedTraining
    imgDataProcessedValidation = utils.pre_proc(imgDataValidation, meansTrainX,
                                stdTrainX, dim_ordering='tf')
    del imgDataValidation
    predictionsValidation = base_model.predict(imgDataProcessedValidation)
    predictionsValidation = model.predict(predictionsValidation)
    del imgDataProcessedValidation
    tTraining = np.arange(0,nsamples)
    tValidation = np.arange(sample,sample+len(predictionsValidation))
    to_python_scalar = lambda x:x.item()
    predDataTraining      = list (map(to_python_scalar,predictionsTraining))
    predDataValidation = list (map(to_python_scalar,predictionsValidation))
    sample           = Metadata["sample"]
    modelPath        = Metadata["modelPath"]
    predictionMetadata = {
            "sample":sample,
            "nsamples":nsamples,
            "imageDatasetName":imageDatasetName,
            "audioDatasetName":audioDatasetName,
            "predictionsTraining":predDataTraining,
            "trainRangeBegin":trainRangeBegin,
            "trainRangeEnd":trainRangeEnd,
            "predictionsValidation":predDataValidation,
            "ValidationRangeBegin":sample,
            "ValidationRangeEnd":len(predictionsValidation),
            "modelPath":modelPath
    }
    if save:
        with open(savedir+predictedDataFname, 'w') as outfile:
            json.dump(predictionMetadata, outfile)

    return tTraining,predictionsTraining,tValidation,predictionsValidation

def predict_many_ll(modelsJsons):
    for modelMetadataName in modelsJsons:
        print("Predicting...")
        tTraining,predictionsTraining,tValidation,predictionsValidation = \
                predict_model_ll(modelMetadataName,savedir=directories.predicted,save=True)
        with open(directories.modelDir+modelMetadataName) as f:
            Metadata = json.load(f)
        # audioDatasetName = Metadata["audioDatasetName"] # Change here for different datasets
        # audioDataset = np.load(audioDatasetName)

def predict_many(modelsJsons):
    for modelMetadataName in modelsJsons:
        print("Predicting...")
        tTraining,predictionsTraining,tValidation,predictionsValidation = \
                predict_model(modelMetadataName,savedir = directories.predicted,save=True)
        with open(directories.modelDir+modelMetadataName) as f:
            Metadata = json.load(f)
        # audioDatasetName = Metadata["audioDatasetName"] # Change here for different datasets
        # audioDataset = np.load(audioDatasetName)

def predict_many_both(modelsJsons):
    for modelMetadataName in modelsJsons:
        print("Predicting...")
        with open(directories.modelDir+modelMetadataName) as f:
            Metadata = json.load(f)
        train_last_only=False
        if "train_last_only" in Metadata.keys():
            if Metadata["train_last_only"]:
                tTraining,predictionsTraining,tValidation,predictionsValidation = \
                        predict_model_ll(modelMetadataName,savedir = directories.predicted,save=True)
                train_last_only = True
        if not train_last_only:
            tTraining,predictionsTraining,tValidation,predictionsValidation = \
                    predict_model(modelMetadataName,savedir=directories.predicted,save=True)
        # audioDatasetName = Metadata["audioDatasetName"] # Change here for different datasets
        # audioDataset = np.load(audioDatasetName)



if __name__ == '__main__':
    pass
    # modelsJsons = ["noiseImg_size_20000_2018-08-05.18:14:10.json",
    #                 "noiseImg_size_20000_2018-08-05.18:15:34.json",
    #                 "noiseImg_size_20000_2018-08-05.18:17:43.json",
    #                 "noiseImg_size_20000_2018-08-05.18:23:46.json",
    #                 "noiseImg_size_20000_2018-08-05.18:25:03.json",
    #                 "noiseImg_size_20000_2018-08-05.18:27:20.json",
    #                 "noiseImg_size_20000_2018-08-05.18:33:41.json",
    #                 "noiseImg_size_20000_2018-08-05.19:00:22.json",
    #                 "noiseImg_size_20000_2018-08-05.19:46:01.json",
    #                 "noiseImg_size_20000_2018-08-05.22:11:27.json",
    #                 "noiseImg_size_20000_2018-08-05.22:38:50.json",
    #                 "noiseImg_size_20000_2018-08-05.23:26:56.json"]
    # modelsJsons=["noiseImg_size_20000_2018-08-05.23:38:39.json",
    #             "noiseImg_size_20000_2018-08-05.23:41:48.json"]


        # fig1 = plt.figure(1)
        # ax1  = fig1.add_subplot(111)
        # ax1.plot(tTraining,predictionsTraining,'r')
        # print("tValidation shape:",tValidation.shape)
        # print("predictionsValidation:",predictionsValidation)
        # ax1.plot(tValidation,predictionsValidation,'b')
        # ax1.plot(audioDataset,'k')
        #
        # fig2 = plt.figure(2)
        # ax2  = fig2.add_subplot(111)
        # ax2.plot(tValidation,predictionsValidation,'b')
        # ax2.plot(tTraining,predictionsTraining,'r')
        # plt.show()
