from predict_funcs import predict_many,predict_model_ll,predict_many_both

#Already predicted
# Train All Layers
# modelsJsons =  ["noiseImg_size_20000_2018-08-17.16:17:22.json",
# "noiseImg_size_20000_2018-08-17.16:19:30.json",
# "noiseImg_size_20000_2018-08-17.16:25:34.json",
# "noiseImg_size_20000_2018-08-17.16:27:51.json",
# "noiseImg_size_20000_2018-08-17.16:34:16.json",
# "noiseImg_size_20000_2018-08-17.17:22:03.json",
# "noiseImg_size_20000_2018-08-17.19:49:23.json",
# "noiseImg_size_20000_2018-08-17.20:39:02.json",
# "noiseImg_size_20000_2018-08-17.23:12:11.json",
# "noiseImg_size_20000_2018-08-17.23:13:26.json",
# "noiseImg_size_20000_2018-08-17.23:15:35.json",
# "noiseImg_size_20000_2018-08-17.23:21:42.json",
# "noiseImg_size_20000_2018-08-17.23:23:00.json",
# "noiseImg_size_20000_2018-08-17.23:25:16.json",
# "noiseImg_size_20000_2018-08-17.23:31:40.json",
# "noiseImg_size_20000_2018-08-17.23:57:02.json",
# "noiseImg_size_20000_2018-08-18.00:44:55.json",
# "noiseImg_size_20000_2018-08-18.03:11:37.json",
# "noiseImg_size_20000_2018-08-18.03:38:56.json",
# "noiseImg_size_20000_2018-08-18.04:27:48.json"]

#Last Only
modelsJsons = ["noiseImg_size_20000_2018-08-20.02:58:30.json",
                "noiseImg_size_20000_2018-08-20.03:00:20.json",
                "noiseImg_size_20000_2018-08-20.03:01:23.json",
                "noiseImg_size_20000_2018-08-20.03:03:19.json",
                "noiseImg_size_20000_2018-08-20.03:04:35.json",
                "noiseImg_size_20000_2018-08-20.03:07:42.json",
                "noiseImg_size_20000_2018-08-20.14:17:50.json",
                "noiseImg_size_20000_2018-08-20.14:21:14.json"]


predict_many_both(modelsJsons)
