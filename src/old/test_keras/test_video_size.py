import cv2
import matplotlib.pyplot as plt

video_filename = "/home/mazza/gitlab/noise_img/dataset/240x240/M2U00001.MPG"
video = cv2.VideoCapture(video_filename)

fig = plt.figure(1)
ax  = fig.add_subplot(111)
ret,frame = video.read()
im  = ax.imshow(frame)

while video.isOpened():
    ret,frame = video.read()
    im.set_data(frame)
    # print(frame.shape)
    plt.pause(0.01)
    plt.draw()
