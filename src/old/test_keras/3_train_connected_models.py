from keras.optimizers import SGD
from keras.layers import Flatten,Dense,Input,Activation
from keras.models import Model
import numpy as np

def print_layers(model):
    for layer in model.layers:
        print (layer.get_weights())

x_dataset = np.array([[0,0],
                      [0,1],
                      [1,0],
                      [1,1]],dtype=np.float32)
y_dataset = np.array([[0],
                      [1],
                      [1],
                      [0]],dtype=np.float32)

input_shape = 2,
inp         = Input(shape=input_shape)
layer_1     = Dense(3)(inp)
non_1       = Activation("tanh")(layer_1)
layer_2     = Dense(1)(non_1)
non_2       = Activation("sigmoid")(layer_2)

mod_1 = Model(inputs=inp,outputs=non_1)
mod_2 = Model(inputs=inp,outputs=non_2)

print("Before")
print("Model 1")
print_layers(mod_1)
print("Model 2")
print_layers(mod_2)

mod_2.compile(SGD(lr=0.03,momentum=0.9),loss="mean_squared_error")


mod_2.fit(x_dataset,
          y_dataset,
          batch_size=1,
          epochs=100,verbose=False)
print (mod_2.predict(x_dataset)>0.5)
print (mod_2.predict(x_dataset))

print("After")
print("Model 1")
print_layers(mod_1)
print("Model 2")
print_layers(mod_2)
