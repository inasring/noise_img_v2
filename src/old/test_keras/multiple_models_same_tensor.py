from keras.applications import ResNet50
from keras.layers import Flatten,Dense
from keras.models import Model
import numpy as np

def mult(x):
    res=1
    for i in x:
        res*=i
    return res

input_shape = (240,240,3)
data = np.random.random((1,)+input_shape)

res50 = ResNet50(input_shape=input_shape,include_top=False)
print("resnet50 output without top")
print(res50.predict(data))

res50_flat = Flatten()(res50.output)
res50_top = Dense(1)(res50_flat)
model_res50_top = Model(inputs=res50.input,outputs=res50_top)
# model_res50_top.layers[-1].set_weights(np.ones(mult(res50_flat)))
print("resnet50 output with top")
print(model_res50_top.predict(data))
