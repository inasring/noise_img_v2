from keras.applications import ResNet50
from keras.layers import Flatten,Dense,Input
from keras.models import Model
import numpy as np

def print_layers(model):
    for layer in model.layers:
        print (layer.get_weights())

input_shape = 3,
inp = Input(shape=input_shape)
x = Dense(2)(inp)

pred_1 = x
pred_2 = Dense(1)(x)

mod_1 = Model(inputs = inp,outputs = pred_1)
mod_2 = Model(inputs = inp,outputs = pred_2)

print("Before:")
print("Model 1")
print_layers(mod_1)
print("Model 2")
print_layers(mod_2)
print("\n")


w1 = np.eye(3,2)
b1 = np.ones(2)

mod_1.layers[1].set_weights([w1,b1])
print("After:")
print("Model 1")
print_layers(mod_1)
print("Model 2")
print_layers(mod_2)
