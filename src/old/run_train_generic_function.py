import numpy as np
rand_state=1234
np.random.seed(rand_state)
import copy
import directories
import dir_utils
import sys
from train_generic_function import train_network
import uuid
#Add last layer only training
sys.setrecursionlimit(10000)
dir_utils.make_dir(directories.modelDir)
dir_utils.make_dir(directories.resDir)

train_id = str(uuid.uuid4()) #Use the same for a single experiment

# imageDatasetName = directories.numpyDataset+"imageDataset_f_300_b_300_M2U00001.npy"
imageDatasetName = directories.numpyDataset+"imageDataset_f_300_b_300_M2U00007.npy"
# audioDatasetName = directories.numpyDataset+"audioDataset_f_300_b_300_M2U00001.npy"
audioDatasetName = directories.numpyDataset+"audioDataset_f_300_b_300_M2U00007.npy"

sample     = 20000
test_size  = 0.1
not_pretrained = False
SGDOptimizer  = "SGD"
epoch_schedule = [15,25,45]
lr_list = [0.01,0.001,0.0001,0.0001]
data_augmentation = False
epochs=2
batch_size = 64
experiment_name = "Adam_SGD_DenseNet_ResNet50_VGG16_2epochs_50_epochs"

denseModelName  = "DenseNet"
depth = 3*2+4 #number of layers inside a dense block
denseNetConf = {}
denseNetConf["depth"]= 3*2+4
denseNetConf["growthRate"]= 8
denseNetConf["n_dense_blocks"] = 3
denseNetConf["feature_map_n_list"] = [denseNetConf["growthRate"]]*\
                                        int((denseNetConf["depth"] - 4) / 3)
assert (denseNetConf["depth"] - 4) % 3 == 0, "Depth must be 3 N + 4"
denseNetConf["n_filter_initial"] = 16
denseNetConf["droprate"] = 0.2
denseNetConf["bnL2norm"] = 0.0001
denseNetConf["l2_dense_penalization"] = 5e-5
denseNetConf["nb_classes"] = 1
denseNetConf["finalActivation"] = 'linear'
#configuration_dict["input_shape"] Adicionado na hora de gerar modelo dentro da train

kwargs = {"imageDatasetName":imageDatasetName,
                    "audioDatasetName":audioDatasetName,
                    "sample":sample,
                    "test_size":test_size,
                    "modelName":denseModelName,
                    "pretrained":not_pretrained,
                    "optimizer":SGDOptimizer,
                    "epoch_schedule":epoch_schedule,
                    "lr_list":lr_list,
                    "data_augmentation":data_augmentation,
                    "epochs":epochs,
                    "batch_size":batch_size,
                    "config_dict":denseNetConf,
                    "train_id":train_id,
                    "experiment_name":experiment_name}

denseSGDTrainArgs = copy.deepcopy(kwargs)

VGG16ModelName = 'VGG16'
pretrained=True
lr_list_pretrained = [1e-5,1e-6,1e-7,1e-8]
VGG16Conf = {}
VGG16Conf["finalActivation"] = 'linear'
VGG16Conf["lastLayerDenseSize"] = 128
VGG16SGDTrainArgs = copy.deepcopy(kwargs)
VGG16SGDTrainArgs["modelName"] = VGG16ModelName
VGG16SGDTrainArgs["lr_list"] = lr_list_pretrained
VGG16SGDTrainArgs["config_dict"] = VGG16Conf
VGG16SGDTrainArgs["pretrained"]  = pretrained

ResNet50ModelName = "ResNet50"
ResNet50Conf = {}
ResNet50Conf["finalActivation"] = 'linear'
ResNet50Conf["lastLayerDenseSize"] = 128
batch_size_resnet50 = 32
ResNet50SGDTrainArgs = copy.deepcopy(kwargs)
ResNet50SGDTrainArgs["modelName"] = ResNet50ModelName
ResNet50SGDTrainArgs["lr_list"] = lr_list_pretrained
ResNet50SGDTrainArgs["config_dict"] = ResNet50Conf
ResNet50SGDTrainArgs["batch_size"] = batch_size_resnet50
ResNet50SGDTrainArgs["pretrained"]  = pretrained

sgd_training_args  = [denseSGDTrainArgs,VGG16SGDTrainArgs,ResNet50SGDTrainArgs]
adam_training_args = []
for arguments in sgd_training_args:
    copy_arguments = copy.deepcopy(arguments)
    copy_arguments['optimizer'] = "Adam"
    adam_training_args.append(copy_arguments)
all_args = copy.deepcopy(sgd_training_args)+copy.deepcopy(adam_training_args)

n_args = len(all_args)
for argument_index in range(n_args):
    copy_arguments = copy.deepcopy(all_args[argument_index])
    copy_arguments['epochs'] = 50
    all_args.append(copy_arguments)

def table_args(argument_list,headers=["modelName","optimizer","epochs"]):
    table = []
    for argument_index in range(len(argument_list)):
        table.append([])
        for header in headers:
            table[argument_index].append(argument_list[argument_index][header])
    return table

from tabulate import tabulate
headers=["modelName","optimizer","epochs"]
tabulate_table = tabulate(table_args(all_args,headers),headers=headers)
print(tabulate_table)
for arguments in all_args:
    train_network(**arguments)
