import numpy as np
rand_state=1234
np.random.seed(rand_state)
import json
import keras
from keras.datasets import cifar10
from keras.layers import merge, Input
from keras.losses import mean_squared_error
from keras import metrics
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.optimizers import SGD,Adam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from dense_net import dense_net
from keras.backend import tf as ktf
import directories
import dir_utils
import utils
sys.setrecursionlimit(10000)

dir_utils.make_dir(directories.modelDir)
dir_utils.make_dir(directories.resDir)

# the data, shuffled and split between train and test sets
imageDatasetName = "imageDataset_f_300_b_300_M2U00001.npy"
audioDatasetName = "audioDataset_f_300_b_300_M2U00001.npy"
print("Loading dataset...")
audioDataset = np.load(directories.numpyDataset+audioDatasetName)
imageDataset = np.load(directories.numpyDataset+imageDatasetName,
                        mmap_mode='r')
sample = 20000
# sample = 20
test_size = 0.1
audioDataset = audioDataset[:sample]
imageDataset = imageDataset[:sample]
print("Dataset loaded.")
print("Image dataset shape =",imageDataset.shape)
print("Audio dataset shape =",audioDataset.shape)

print("Splitting dataset...")
# X_train, X_validation, Y_train, Y_validation = train_test_split(imageDataset, audioDataset,
#                                                                 test_size=test_size,random_state=rand_state)
cutPosition  = sample-int(sample*test_size)
X_train      = imageDataset[:cutPosition]
X_validation = imageDataset[cutPosition:]
Y_train      = audioDataset[:cutPosition]
Y_validation = audioDataset[cutPosition:]
print("Dataset split.")
del imageDataset

X_train = X_train.astype('float32')
X_validation = X_validation.astype('float32')
Y_train = Y_train.astype('float32')
Y_validation = Y_validation.astype('float32')
#pixelMean = np.mean(X_train, axis=0)

meansTrain,stdsTrain = utils.channels_MeanStd(X_train, dim_ordering='tf')
print(Y_train)
yStd  = Y_train.std()
yMean = Y_train.mean()
# Y_train-=yMean
# Y_train/=yStd
# Y_validation-=yMean
# Y_validation/=yStd
# Y_train = (Y_train-yMean)/yStd
# Y_validation = (Y_validation-yMean)/yStd
print("Preprocessing dataset...")
X_train      = utils.pre_proc(X_train, meansTrain,
                       stdsTrain, dim_ordering='tf')
print("mean = ",meansTrain)
print("std=   ",stdsTrain)
X_validation = utils.pre_proc(X_validation, meansTrain,
                        stdsTrain, dim_ordering='tf')

print("K.image_dim_ordering()=",K.image_dim_ordering())

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_validation.shape[0], 'test samples')


input_shape = X_train[0].shape
print("input_shape=",input_shape)
droprate=0.2
batch_size = 64
nb_epoch = 50
# data_augmentation = True
data_augmentation = False
n_dense_blocks = 3

# k = 12 #growth rate: number of feature maps at each convolution output inside a dense block
k = 8 #growth rate: number of feature maps at each convolution output inside a dense block
# depth = 40 #number of layers inside a dense block
depth = 3*2+4 #number of layers inside a dense block
assert (depth - 4) % 3 == 0, "Depth must be 3 N + 4"

# layers in each dense block
nb_layers = int((depth - 4) / 3)
k0= 16
n_filter = k0

feature_map_n_list = [k]*nb_layers
bnL2norm=0.0001


model = dense_net(n_dense_blocks,
                input_shape,
                feature_map_n_list,
                n_filter_initial=k0,
                droprate=droprate,
                bnL2norm=0.00005,
                l2_dense_penalization=0.00005,
                nb_classes=1,
                finalActivation='linear',
                summary=False)

# def lr_schedule(epoch):
#     if epoch < 150: rate = 0.1
#     elif epoch < 225: rate = 0.01
#     elif epoch < 300: rate = 0.001
#     else: rate = 0.0001
#     print (rate)
#     return rate
def lr_schedule(epoch):
    if epoch < 15: rate = 0.01
    elif epoch < 25: rate = 0.001
    elif epoch < 45: rate = 0.0001
    else: rate = 0.0001
    print (rate)
    return rate
lrate = LearningRateScheduler(lr_schedule)

class Histories(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

        def on_train_end(self, logs={}):
            return

        def on_epoch_begin(self, epoch, logs={}):
            return

        def on_epoch_end(self, epoch, logs={}):
            self.losses.append(logs.get('loss'))
            print(self.losses)
            return

        def on_batch_begin(self, batch, logs={}):
            return

        def on_batch_end(self, batch, logs={}):
            return

#exit(1)
model.compile(SGD(lr=0.01, momentum=0.9, decay=0.0, nesterov=True), loss='mean_squared_error',
              metrics=['mse'])
# model.compile(Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False),
#             loss=mean_squared_error,
#             metrics=[metrics.mse])

from datetime import datetime
string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
filename  = "noiseImg_"+"_size_"+str(sample)+'_'+string_date
filepath  = directories.modelDir+filename+".h5"
results   = directories.resDir+filename+".results"
model_checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
early_stopping   = EarlyStopping(monitor='val_loss', patience=10, verbose=1, mode='auto')
logging          = Histories()
# callbacks = [lrate,model_checkpoint,early_stopping]
callbacks = [lrate,model_checkpoint,early_stopping,logging]
# callbacks = [model_checkpoint,early_stopping]
# callbacks = [model_checkpoint]
to_python_scalar = lambda x:x.item()
meansTrain = list (map(to_python_scalar,meansTrain))
stdsTrain  = list (map(to_python_scalar,stdsTrain))
yMean      = to_python_scalar(yMean)
yStd       = to_python_scalar(yStd)
training_data = {
        "sample":sample,
        "date":string_date,
        "cutPosition":cutPosition,
        "random_state":rand_state,
        "imageDatasetName":imageDatasetName,
        "audioDatasetName":audioDatasetName,
        "meansTrainX":list(meansTrain),
        "stdTrainX":list(stdsTrain),
        "meansTrainY":yMean,
        "stdTrainY":yStd,
        "test_size":test_size,
        "modelPath":filepath,
        "data_augmentation":data_augmentation,
        "epochs":nb_epoch,
        "batch_size":batch_size,
        "droprate":droprate,
        "n_filter_initial":k0,
        "depth":depth,
        "growthRate":k,
        "n_dense_blocks":n_dense_blocks
}
with open(directories.modelDir+filename+".json", 'w') as outfile:
    json.dump(training_data, outfile)





if not data_augmentation:
    print('Not using data augmentation.')
    history=model.fit(X_train, Y_train,
              batch_size=batch_size,
              epochs    =nb_epoch,
              validation_data=(X_validation, Y_validation),
              shuffle=True,
	          callbacks=callbacks)
else:
    print('Using real-time data augmentation.')

    # this will do preprocessing and realtime data augmentation
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False)  # randomly flip images

    # compute quantities required for featurewise normalization
    # (std, mean, and principal components if ZCA whitening is applied)
    datagen.fit(X_train)

    # fit the model on the batches generated by datagen.flow()
    history = model.fit_generator(datagen.flow(X_train, Y_train,shuffle=True),
                        steps_per_epoch=X_train.shape[0]//batch_size,
                        #nb_val_samples=X_validation.shape[0],
                        epochs=nb_epoch,
                        validation_data=(X_validation, Y_validation), callbacks=callbacks,)


#
# training_data = {
#         "sample":sample,
#         "date":string_date,
#         "cutPosition":cutPosition,
#         "random_state":rand_state,
#         "imageDatasetName":imageDatasetName,
#         "audioDatasetName":audioDatasetName,
#         "meansTrainX":list(meansTrain),
#         "stdTrainX":list(stdsTrain),
#         "meansTrainY":yMean,
#         "stdTrainY":yStd,
#         "test_size":test_size,
#         "results":results,
#         "modelPath":filepath,
#         "data_augmentation":data_augmentation,
#         "epochs":nb_epoch,
#         "batch_size":batch_size,
#         "droprate":droprate,
#         "n_filter_initial":k0,
#         "depth":depth,
#         "growthRate":k,
#         "n_dense_blocks":n_dense_blocks
# }
# with open(directories.modelDir+filename+".json", 'w') as outfile:
#     json.dump(training_data, outfile)

f= open(results, 'w')
writer = csv.writer(f)
writer.writerows(zip(history.history['loss'],history.history['val_loss']))
f.close()
