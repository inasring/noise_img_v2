from __future__ import print_function
import numpy as np
rand_state=1234
np.random.seed(rand_state)

from keras.datasets import cifar10
from keras.layers import merge, Input
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.optimizers import SGD
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from dense_net import dense_net
from keras.backend import tf as ktf
import directories
import dir_utils
import utils
sys.setrecursionlimit(10000)

# from tensorflow.python import debug as tf_debug
# sess = K.get_session()
# sess = tf_debug.LocalCLIDebugWrapperSession(sess)
# K.set_session(sess)
dir_utils.make_dir(directories.modelDir)
dir_utils.make_dir(directories.resDir)

img_rows, img_cols = 32, 32
img_channels = 3
nb_classes = 10
penalization = 0.0001

# the data, shuffled and split between train and test sets
(X_train, y_train), (X_test, y_test) = cifar10.load_data()
X_train, X_validation, y_train, y_validation = train_test_split(X_train, y_train, test_size=0.1,random_state=rand_state)

X_train = X_train.astype('float32')
X_validation = X_validation.astype('float32')
#pixelMean = np.mean(X_train, axis=0)

# img_dim = X_train.shape[1:]
# if K.image_dim_ordering() == "th":
#     n_channels = img_dim[0]
#     for i in range(n_channels):
#         mean = np.mean(X_train[:, i, :, :]) #using train only mean and std
#         std = np.std(X_train[:, i, :, :])
#         X_train[:, i, :, :] = (X_train[:, i, :, :] - mean) / std
#         X_validation[:, i, :, :] = (X_validation[:, i, :, :] - mean) / std
#
# elif K.image_dim_ordering() == "tf":
#     n_channels = img_dim[-1]
#     for i in range(n_channels):
#         mean = np.mean(X_train[:, :, :, i]) #using train only mean and std
#         std = np.std(X_train[:, :, :, i])
#         X_train[:, :, :, i] = (X_train[:, :, :, i] - mean) / std
#         X_validation[:, :, :, i] = (X_validation[:, :, :, i] - mean) / std

meansTrain,stdsTrain = utils.channels_MeanStd(X_train, dim_ordering='tf')

X_train      = utils.pre_proc(X_train, meansTrain,
                       stdsTrain, dim_ordering='tf')
X_validation = utils.pre_proc(X_validation, meansTrain,
                        stdsTrain, dim_ordering='tf')

print("K.image_dim_ordering()=",K.image_dim_ordering())

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_validation.shape[0], 'test samples')

# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
y_validation = np_utils.to_categorical(y_validation, nb_classes)


input_shape = X_train[0].shape
print("input_shape=",input_shape)
droprate=0.2
batch_size = 64
nb_epoch = 300
data_augmentation = True
n_dense_blocks = 3

k = 12 #growth rate: number of feature maps at each convolution output inside a dense block
depth = 40 #number of layers inside a dense block
assert (depth - 4) % 3 == 0, "Depth must be 3 N + 4"

# layers in each dense block
nb_layers = int((depth - 4) / 3)
k0= 16
n_filter = k0

feature_map_n_list = [k]*nb_layers
bnL2norm=0.0001


model = dense_net(n_dense_blocks,
                input_shape,
                feature_map_n_list,
                n_filter_initial=16,
                droprate=0.2,
                bnL2norm=0.00005,
                l2_dense_penalization=0.00005,
                nb_classes=10,
                finalActivation='softmax',
                summary=False)


#paper
def lr_schedule(epoch):
    if epoch < 150: rate = 0.1
    elif epoch < 225: rate = 0.01
    elif epoch < 300: rate = 0.001
    else: rate = 0.0001
    print (rate)
    return rate

model.summary()
#exit(1)
model.compile(SGD(lr=0.1, momentum=0.9, decay=0.0, nesterov=True), loss='categorical_crossentropy',
              metrics=['accuracy'])

lrate = LearningRateScheduler(lr_schedule)

from datetime import datetime
string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
filename  = "dense_net_cifar10_"+string_date
filepath  = directories.modelDir+filename+".h5"
results   = directories.resDir+filename+".results"
model_checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
early_stopping   = EarlyStopping(monitor='val_acc', patience=100, verbose=1, mode='auto')
#callbacks = [lrate,model_checkpoint,early_stopping]
callbacks = [lrate,model_checkpoint]


if not data_augmentation:
    print('Not using data augmentation.')
    history=model.fit(X_train, Y_train,
              batch_size=batch_size,
              nb_epoch=nb_epoch,
              validation_data=(X_validation, y_validation),
              shuffle=True,
	      callbacks=callbacks)
else:
    print('Using real-time data augmentation.')

    # this will do preprocessing and realtime data augmentation
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False)  # randomly flip images

    # compute quantities required for featurewise normalization
    # (std, mean, and principal components if ZCA whitening is applied)
    datagen.fit(X_train)

    # fit the model on the batches generated by datagen.flow()
    history = model.fit_generator(datagen.flow(X_train, Y_train,shuffle=True),
                        steps_per_epoch=X_train.shape[0]//batch_size,
                        #nb_val_samples=X_validation.shape[0],
                        epochs=nb_epoch,
                        validation_data=(X_validation, y_validation), callbacks=callbacks,)
f= open(results, 'wb')
writer = csv.writer(f)
writer.writerows(zip( history.history['acc'],history.history['val_acc'],history.history['loss'],history.history['val_loss']))
f.close()
