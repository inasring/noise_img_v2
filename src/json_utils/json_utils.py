import json

def load_json(filename):
    with open(filename,"r") as handler:
        data = json.load(handler)
    return data

def save_json(filename,data):
    with open(filename, 'w') as outfile:
        json.dump(data, outfile)

if __name__ == "__main__":
    my_data = {'data':[1,2,3],"test":(1,2,3),"json":"aaa"}
    save_json("test.json",my_data)
