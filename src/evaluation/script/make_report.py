import os
from locations import directories as d
from dir_utils import dir_utils as du
from ..report import report_tables_figures
from ..report import report_latex
import argparse

# python3.5 -m evaluation.script.make_report --json /home/mazza/gitlab/noise_img_v2/model/sgd_adam_vgg_resnet_train_only_train_all_3585c247-c227-43b9-a3d6-4a983f74c4d1.json --texdir /home/mazza/gitlab/noise_img_v2/latex/latex_19112018_v3 --output latex_19112018_v3.tex

parser = argparse.ArgumentParser()
parser.add_argument("--json")
# parser.add_argument("--figdir")
parser.add_argument("--texdir")
parser.add_argument("--output")
args = parser.parse_args()

experiment_log_name = args.json
latex_dir           = args.texdir
save_name           = args.output
save_figure_dir     = os.path.join(os.sep,latex_dir,d.figures_dir)

summary_rename = {"Global Average Pooling":"GAP",
                    "Train FC only":"T_FC","epochs":"Ep",
                    "Optimizer":"Opt","minimum_loss":"t_loss",
                    "minimum_val_loss":"v_loss"}
summary_columns=["Network","Optimizer",
                "Global Average Pooling","Train FC only",
                "FC","params","epochs",
                "minimum_loss","minimum_val_loss",
                "v_MSE","v_MAE"]
du.make_dir(latex_dir)
du.make_dir(save_figure_dir)
report_tables = report_tables_figures(experiment_log_name,
                        summary_rename,
                        summary_columns,
                        figure_dir=save_figure_dir,
                        save_fig=True)
report_latex(report_tables,
                figure_dir=save_figure_dir,
                latex_dir=latex_dir,
                save_name=save_name,
                save_tex=True)
