import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import collections
import re
from json_utils import json_utils as jutils
from locations import directories as d
from dir_utils import dir_utils as du
##Jogar geração da tabela latex numa função
##Table of experiment details
#  Experiment info
#    # Experiment uuid
#    # Experiment name
#  Dataset info
#    #  videos used in training
#    #  samples from each video in training
#    #  videos used in validation
#    #  samples from each video in validation
#    #  subsampling in time for each video
#    Training with net_1 and config_1
#      #  Training info
#        #  Network name
#        #  learning rate schedule
#        #  lowest training error
#        #  lowest validation error
#        #  number training epochs
#      #  Plots
#        #  Training/Validation curves
#        #  Inference curves; training and validation difference by color
#    Training with net_2 and config_2
#    ...

def tex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
    }
    regex = re.compile('|'.join(re.escape(key) for key in sorted(conv.keys(), key = lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)
def encapsulate(text,before,after,endline="\n",tab="\t"):
    ret  = before+endline
    ret  +=text+endline
    ret  +=after+endline
    return ret
def tabular_config(config):
    ret = "{"+config+"}"
    return ret

def to_multi_index(key,l):
    key_l = [key]*len(l)
    return [np.array(key_l),np.array(l)]

def filename_last_dir(filename):
    last_dir = os.path.split(os.path.dirname(filename))[-1]
    filename_base = os.path.basename(filename)
    ret = os.path.join(last_dir,filename_base)
    return ret

def to_row_str(l,n_cols):
    ret = ''
    n_loop_string = len(l)
    add_spaces = n_cols-len(l)
    if n_cols<len(l):
        n_loop_string = n_cols
        add_spaces    = 0
    for idx in range(n_loop_string):
        ret+=l[idx]
        if idx<(n_loop_string-1):
            ret+=' & '
    ret+=' & '*add_spaces
    ret+='\\\\'
    return ret

def to_figure(figure_name,directory=''):
    before_graphics = "\includegraphics[width=\linewidth]{"
    after_graphics  = "}"
    before_figure   = "\\begin{figure}[h]\n"
    after_figure   = "\\end{figure}\n"
    fname_full = os.path.join(os.sep,directory,figure_name)
    fname_full = os.path.normpath(fname_full)
    ret = encapsulate(fname_full,before_graphics,after_graphics,endline='')
    ret = encapsulate(ret,before_figure,after_figure)
    return ret

# experiment_data_filename = "test_multi_dataset_version_93e09a32-e512-4f3d-95d4-d38c2016949b.json"
experiment_data_filename = "test_multi_dataset_version_5962ab28-8371-4191-abd1-c735c3174d78.json"
experiment_data_filename = os.path.join(os.sep,d.modelDir,experiment_data_filename)
print(experiment_data_filename)
experiment_data = jutils.load_json(experiment_data_filename)
print(experiment_data.keys())
training_data_list  = experiment_data["training_config_list"]
network_config_list = experiment_data["network_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
targets             = experiment_data["targets"]
prediction_list     = experiment_data["prediction_list"]
audioDataset = np.hstack((targets["training"],targets["validation"]))
print(dataset_dic)

description_elements    = ["\\textbf{Experimento}",
                        training_data_list[0]["string_date"],
                        # tex_escape(training_data_list[0]["experiment_name"]),
                        training_data_list[0]["train_id"]]
description = {key:[value] for value,key in zip(description_elements,
                                            range(len(description_elements)))}
print(description)
experiment_description = pd.DataFrame.from_dict(description,dtype=object)
experiment_description_tex = experiment_description.to_latex(escape=False,
                                                            index=None,
                                                            header=False)
experiment_description_tex = encapsulate(experiment_description_tex,
                                before="{\n\centering",after="}")
dataset_descr_dir = collections.OrderedDict()
dataset_descr_dir["Videos"] = [tex_escape(filename_last_dir(i)) \
                                for i in dataset_dic["videoFiles"]]
dataset_descr_dir["IMG/video"] = dataset_dic["n_video_samples"]
dataset_descr_dir["subsample"] = dataset_dic["subsampleTime"]
dataset_descr_dir["T\_B"] = [str(i) for i in dataset_dic["timeBackward"]]
dataset_descr_dir["T\_F"] = [str(i) for i in dataset_dic["timeForward"]]

dataset_descr_pd = pd.DataFrame.from_dict(dataset_descr_dir)
dataset_descr_tex= dataset_descr_pd.to_latex(escape=False,index=None)
dataset_descr_tex = encapsulate(dataset_descr_tex,
                                before="{\n\centering",after="}")
training_samples = "\\begin{tabular}{ll}\n"\
                    +"Training Samples & "+str(dataset_dic["cut"])+" \\\\ \n"\
                    +"\\end{tabular}\n"

# predict_data = True
# if predict_data:
#     from keras.models import load_model
#     print("Loading data...")
#     imageDataset_fname = dataset_dic["imageDatasetName"]+".npy"
#     audioDataset_fname = dataset_dic["audioDatasetName"]+".npy"
#     stdTrainX          = dataset_dic["stdTrainX"]
#     meansTrainX        = dataset_dic["meansTrainX"]
#     print("Loading audio data...")
#     audioDataset = np.load(audioDataset_fname)
#     print("Loading image data...")
#     imageDataset = np.load(imageDataset_fname)
#     print("Preprocessing...")
#     imageDataset -= meansTrainX
#     imageDataset /= stdTrainX

summary_df = pd.DataFrame()
network_descr_tex_list  = []
#Make a new pandas dataframe with all data
#Add new column at every new iteration using .transpose()
figure_name_list = []
figure_tex_list  = []
summary_df = pd.DataFrame()
for i,training_config_dic in enumerate(training_data_list):
    network_config_dic = network_config_list[i]
    model_name = training_config_dic["model_save_name"]
    model_dir  = training_config_dic["model_save_path"]
    network_descr_dic  = collections.OrderedDict()
    network_descr_dic["File"] = [training_config_dic["model_save_name"]]
    network_descr_dic["Network"] = [network_config_dic["network"]]
    network_descr_dic["Optimizer"] =[tex_escape(training_config_dic["optimizer"])]
    minimum_loss = min(training_config_dic["loss"])
    minimum_loss_str = "{:2.3f}".format(minimum_loss)
    network_descr_dic["min_loss"]=minimum_loss_str
    minimum_val_loss = min(training_config_dic["val_loss"])
    minimum_val_loss_str = "{:2.3f}".format(minimum_val_loss)
    network_descr_dic["min_val_loss"]=minimum_val_loss_str
    network_descr_dic["Global Average Pooling"]=network_config_dic["global_average_pooling"]
    network_descr_dic["Train FC only"] = network_config_dic["train_fc_only"]
    network_descr_dic["FC"] = network_config_dic["hidden_dense"]
    network_descr_dic["epochs"] = training_config_dic["epochs"]
    network_descr_dic["lr_list"]= str(training_config_dic["lr_list"])
    network_descr_dic["epoch_schedule"]= str(training_config_dic["epoch_schedule"])
    network_descr_pd  = pd.DataFrame.from_dict(network_descr_dic)
    # print(network_descr_pd.transpose())

    summary_df=summary_df.append(network_descr_pd,ignore_index=True)
    # print("summary:",summary_df.transpose())
    # print(network_descr_pd)
    # print("network_descr_pd:",network_descr_pd.transpose()[0])
    network_descr_tex = network_descr_pd.transpose().to_latex(header=False)
    network_descr_tex_list.append(network_descr_tex)
    ##Figures
    val_loss = training_config_dic['val_loss']
    loss = training_config_dic['loss']
    epochs_range = range(training_config_dic["epochs"])
    fig = plt.figure(figsize=(15,5))
    ax1  = fig.add_subplot(1,2,1)
    ax1.plot(epochs_range,val_loss,label="validation")
    ax1.plot(epochs_range,loss,label="training")
    ax1.set_xlabel("Epoch")
    ax1.legend()
    ax2  = fig.add_subplot(1,2,2)
    audio_range = range(len(audioDataset))
    predictions_val = prediction_list[i]["validation"]
    predictions_training = prediction_list[i]["training"]
    ax2.plot(audio_range,audioDataset,color='k',label="target")
    ax2.plot(audio_range[:dataset_dic["cut"]],predictions_training,label="Training prediction")
    ax2.plot(audio_range[dataset_dic["cut"]:],predictions_val,label="Validation prediction")
    ax2.legend()
    figname = "trainPredictCurves_"+str(i)+".eps"
    figure_name_list.append(figname)
    figure_tex_list.append(to_figure(figname,d.latex_test_figures))
    fig.savefig(os.path.join(os.sep,d.latex_test_figures,figname),
                 bbox_inches = 'tight',pad_inches = 0)
summary_df = summary_df[["Network",
                        "Optimizer",
                        "Global Average Pooling",
                        "Train FC only",
                        "FC",
                        "epochs",
                        "min_loss",
                        "min_val_loss"]]
summary_df.rename(columns={"Global Average Pooling":"GAP",
                    "Train FC only":"TFC"},inplace=True)

print("summary:\n",summary_df)
summary_df_str = summary_df.to_latex(index=False)
summary_df_str = encapsulate(summary_df_str,"{\centering\n","\n}")

latex = ''
latex+=experiment_description_tex
latex+=training_samples
latex+=dataset_descr_tex
for i in range(len(network_descr_tex_list)):
    latex+=network_descr_tex_list[i]
    latex+="\n"
    latex+=figure_tex_list[i]
    latex+="\n"
latex += summary_df_str
fname         = "example_pandas.tex"
fname_full    = os.path.join(os.sep,d.latex_test,fname)
du.save_text(fname_full,latex)
