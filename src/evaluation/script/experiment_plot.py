import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import re
from json_utils import json_utils as jutils
from locations import directories as d
from dir_utils import dir_utils as du
##Table of experiment details
#  Experiment info
#    # Experiment uuid
#    # Experiment name
#  Dataset info
#    #  videos used in training
#    #  samples from each video in training
#    #  videos used in validation
#    #  samples from each video in validation
#    #  subsampling in time for each video
#    Training with net_1 and config_1
#      #  Training info
#        #  Network name
#        #  learning rate schedule
#        #  lowest training error
#        #  lowest validation error
#        #  number training epochs
#      #  Plots
#        #  Training/Validation curves
#        #  Inference curves; training and validation difference by color
#    Training with net_2 and config_2
#    ...

def tex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
    }
    regex = re.compile('|'.join(re.escape(key) for key in sorted(conv.keys(), key = lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)
def encapsulate(text,before,after,endline="\n",tab="\t"):
    ret  = before+endline
    ret  +=text+endline
    ret  +=after+endline
    return ret
def tabular_config(config):
    ret = "{"+config+"}"
    return ret

def to_multi_index(key,l):
    key_l = [key]*len(l)
    return [np.array(key_l),np.array(l)]

def filename_last_dir(filename):
    last_dir = os.path.split(os.path.dirname(filename))[-1]
    filename_base = os.path.basename(filename)
    ret = os.path.join(last_dir,filename_base)
    return ret

def to_row_str(l,n_cols):
    ret = ''
    n_loop_string = len(l)
    add_spaces = n_cols-len(l)
    if n_cols<len(l):
        n_loop_string = n_cols
        add_spaces    = 0
    for idx in range(n_loop_string):
        ret+=l[idx]
        if idx<(n_loop_string-1):
            ret+=' & '
    ret+=' & '*add_spaces
    ret+='\\\\'
    return ret

def to_figure(figure_name,directory=''):
    before_graphics = "\includegraphics[width=\linewidth]{"
    after_graphics  = "}"
    before_figure   = "\\begin{figure}[h]\n"
    after_figure   = "\\end{figure}\n"
    fname_full = os.path.join(os.sep,directory,figure_name)
    fname_full = os.path.normpath(fname_full)
    ret = encapsulate(fname_full,before_graphics,after_graphics,endline='')
    ret = encapsulate(ret,before_figure,after_figure)
    return ret

experiment_data_filename = "test_multi_dataset_version_93e09a32-e512-4f3d-95d4-d38c2016949b.json"
experiment_data_filename = os.path.join(os.sep,d.modelDir,experiment_data_filename)
print(experiment_data_filename)
experiment_data = jutils.load_json(experiment_data_filename)
print(experiment_data.keys())
training_data_list  = experiment_data["training_config_list"]
network_config_list = experiment_data["network_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
print(dataset_dic)

begin_tabular  = '\\begin{tabular}'
end_tabular    = '\\end{tabular}'
begin_frame    = '\\begin{frame}'
end_frame      = '\\end{frame}'
latex = ''


experiment = ['\\textbf{Experimento}',
                tex_escape(training_data_list[0]['string_date']),
                tex_escape(training_data_list[0]['experiment_name'])]
# experiment = ['Experiment',
#                 tex_escape(training_data_list[0]['string_date']),
#                 'test']
ncols_exp = len(experiment)
videos_training = "Videos"
dataset_videos = [videos_training,dataset_dic['videoFiles'],dataset_dic['n_video_samples']]



# print("remove path: ",os.path.basename(dataset_dic["videoFiles"][0]))
# print("remove path: ",os.path.split(os.path.dirname(dataset_dic["videoFiles"][0])))
print("remove path: ",filename_last_dir(dataset_dic["videoFiles"][0]))

# dataset_dic["videoFiles"]  = [tex_escape(filename_last_dir(i)) for i in dataset_dic["videoFiles"]]
# dataset_videos_matrix[:,1] = dataset_dic['videoFiles']
# dataset_dic["n_video_samples"]  = [str(i) for i in dataset_dic["n_video_samples"]]
# dataset_videos_matrix[0,2] = "IMG/video"
# dataset_videos_matrix[:,3] = dataset_dic['n_video_samples']
# dataset_videos_matrix[0,4] = "subsample"
# dataset_videos_matrix[:,5] = [str(i) for i in dataset_dic["subsampleTime"]]
ndataset_rows = len(dataset_dic['videoFiles'])+1
ncols_dataset = len(dataset_videos)+3
dataset_videos_matrix = np.empty((ndataset_rows,ncols_dataset),
                            dtype=object)
dataset_videos_matrix[:,:] = ''
dataset_videos_matrix[0,0] = videos_training

dataset_dic["videoFiles"]  = [tex_escape(filename_last_dir(i)) for i in dataset_dic["videoFiles"]]
dataset_videos_matrix[1:,0] = dataset_dic['videoFiles']
dataset_dic["n_video_samples"]  = [str(i) for i in dataset_dic["n_video_samples"]]
dataset_videos_matrix[0,1] = "IMG/video"
dataset_videos_matrix[1:,1] = dataset_dic['n_video_samples']
dataset_videos_matrix[0,2] = "subsample"
dataset_videos_matrix[1:,2] = [str(i) for i in dataset_dic["subsampleTime"]]
dataset_videos_matrix[0,3] = "$T_B$"
dataset_videos_matrix[1:,3] = [str(i) for i in dataset_dic["timeBackward"]]
dataset_videos_matrix[0,4] = "$T_F$"
dataset_videos_matrix[1:,4] = [str(i) for i in dataset_dic["timeForward"]]

# dataset_videos_matrix[:,2] = dataset_dic['n_video_samples']
training_samples = np.array(["Training Samples",dataset_dic["cut"]])
ncols_training_samples = len(training_samples)

du.make_dir(d.latex_test_figures)

network_arrays_list = []
figure_list         = []
ncols_network_max   = 0
predict_data        = True
if predict_data:
    from keras.models import load_model
    print("Loading data...")
    imageDataset_fname = dataset_dic["imageDatasetName"]+".npy"
    audioDataset_fname = dataset_dic["audioDatasetName"]+".npy"
    stdTrainX          = dataset_dic["stdTrainX"]
    meansTrainX        = dataset_dic["meansTrainX"]
    print("Loading audio data...")
    audioDataset = np.load(audioDataset_fname)
    print("Loading image data...")
    imageDataset = np.load(imageDataset_fname)
    print("Preprocessing...")
    imageDataset -= meansTrainX
    imageDataset /= stdTrainX
for i,training_data_dic in enumerate(training_data_list):
    network_arrays_list.append([])
    network_config_dic = network_config_list[i]
    filename   = np.array(["File",tex_escape(training_data_dic["model_save_name"])])
    network_arrays_list[i].append(filename)
    model_name = np.array(["Network",network_config_dic["network"]])
    network_arrays_list[i].append(model_name)
    optimizer   = np.array(["Optimizer",tex_escape(training_data_dic["optimizer"])])
    network_arrays_list[i].append(optimizer)
    minimum_loss = min(training_data_dic["loss"])
    minimum_loss_str = "{:2.3f}".format(minimum_loss)
    min_loss = np.array([tex_escape("min_loss"),minimum_loss_str])
    network_arrays_list[i].append(min_loss)
    minimum_val_loss = min(training_data_dic["val_loss"])
    minimum_val_loss_str = "{:2.3f}".format(minimum_val_loss)
    min_val_loss = np.array([tex_escape("min_val_loss"),minimum_val_loss_str])
    network_arrays_list[i].append(min_val_loss)
    batch_size = np.array(["Batch size",training_data_dic["batch_size"]])
    network_arrays_list[i].append(batch_size)
    gap        = np.array(["Global Average pooling",
                        network_config_dic["global_average_pooling"]])
    network_arrays_list[i].append(gap)
    train_fc_only = np.array(["Train FC only",
                        network_config_dic["train_fc_only"]])
    network_arrays_list[i].append(train_fc_only)
    epochs = np.array(["epochs",
                        training_data_dic["epochs"]])
    network_arrays_list[i].append(epochs)
    lr_list = np.array(["lr\_list",
                        str(training_data_dic["lr_list"])])
    network_arrays_list[i].append(lr_list)
    val_loss = training_data_dic['val_loss']
    loss = training_data_dic['loss']
    epochs_range = range(training_data_dic["epochs"])
    ##Mudar nome do path no save path?
    model_name = training_data_dic["model_save_name"]
    model_dir  = training_data_dic["model_save_path"]
    if predict_data:
        model_name_full = os.path.join(model_dir,model_name)
        print("Loading model...")
        model = load_model(model_name_full)
        predictions = model.predict(imageDataset)
    fig = plt.figure(figsize=(15,5))
    ax1  = fig.add_subplot(1,2,1)
    ax1.plot(epochs_range,val_loss,label="validation")
    ax1.plot(epochs_range,loss,label="training")
    ax1.set_xlabel("Epoch")
    ax1.legend()
    ax2  = fig.add_subplot(1,2,2)
    if predict_data:
        audio_range = range(len(audioDataset))
        ax2.plot(audio_range,audioDataset,color='k',label="target")
        ax2.plot(audio_range[:dataset_dic["cut"]],predictions[:dataset_dic["cut"]],label="Training prediction")
        ax2.plot(audio_range[dataset_dic["cut"]:],predictions[dataset_dic["cut"]:],label="Validation prediction")
        ax2.legend()
    figname = "trainPredictCurves_"+str(i)+".eps"
    figure_list.append(figname)
    fig.savefig(os.path.join(os.sep,d.latex_test_figures,figname),
                 bbox_inches = 'tight',pad_inches = 0)


ncols_list = [ncols_exp,
            ncols_dataset,
            ncols_training_samples,
            ncols_network_max]
ncol_max = max(ncols_list)
#### Text
# experiment_str = to_row_str(experiment,ncols_exp)
experiment_str = to_row_str(experiment,ncol_max)
experiment_str += "[1ex]"
experiment_str = encapsulate(experiment_str,'\\hline','')
dataset_videos_str_list = [to_row_str(dataset_videos_matrix[0,:],ncol_max)]
dataset_videos_str_list += ['\\hline']
dataset_videos_str_list += [to_row_str(dataset_videos_matrix[i,:],ncol_max)\
                            for i in range(1,dataset_videos_matrix.shape[0])]
dataset_videos_str = '\n'.join(dataset_videos_str_list)
dataset_videos_str = encapsulate(dataset_videos_str,'\\hline','')

training_samples_str = to_row_str(training_samples,ncol_max)

network_arrays_string_list = []
for i in range(len(network_arrays_list)):
    for j in range(len(network_arrays_list[i])):
        network_arrays_string_list.append(\
                to_row_str(network_arrays_list[i][j],ncol_max))
    network_arrays_string_list.append("\\hline")
# print(experiment_str)
dataset_table = ''
dataset_table += experiment_str
dataset_table += "\\hline\n"
dataset_table += dataset_videos_str
dataset_table += "\\hline\n"
dataset_table += training_samples_str
dataset_table += "\\hline\n"
dataset_table = encapsulate(dataset_table,
                begin_tabular+tabular_config('c'*ncol_max),
                end_tabular)
dataset_table = encapsulate(dataset_table,
                "{\\centering",
                "}")

network_table = "\\hline\n"
network_table += "\n".join(network_arrays_string_list)
network_table += "\\hline\n"

network_table = encapsulate(network_table,
                begin_tabular+tabular_config('c'*ncol_max),
                end_tabular)

network_table = encapsulate(network_table,
                "{\\centering",
                "}")


tables = dataset_table
tables += network_table

figures_str = "\n".join([to_figure(i,d.latex_test_figures) for i in figure_list])

latex = tables
latex +="\n"
latex +=figures_str
# latex +="\\hline"
# latex = encapsulate(latex,
#             begin_frame,
#             end_frame)
print(latex)
# begin_document = '\\begin{document}'
# end_document   = '\\end{document}'
# beamer_class   = '\\documentclass{beamer}'
# latex = beamer_class+"\n"
# table = "Experimento "+" & "
# table += training_data_list[0]["train_id"]+" & "
# table += training_data_list[0]["string_date"]+" \\\\"
# ncols = table.count("&")+1
# config = tabular_config(ncols*"c")
# table_environ    = encapsulate(table,begin_tabular+config,end_tabular)
# # document_environ = encapsulate(frame_environ,begin_document,end_document)
# latex+=frame_environ
# experiment = {0:"Experimento",1:(training_data_list[0]["train_id"])}
# print(type(training_data_list[0]["train_id"]))
# print(training_data_list[0]["train_id"])
# experiment = np.array([
                        # [training_data_list[0]["experiment_name"]],
                        # [training_data_list[0]["string_date"]]
#                         ],
#                         dtype=object)
# experiment = experiment.T
# experiment =   {"Experimento":}
# table = pd.DataFrame(columns=[0,1,2],dtype=object)
# table.loc["Experimento"] = [training_data_list[0]["experiment_name"],
#                             training_data_list[0]["string_date"],'']
# dataset = np.array([["Dataset info"]
#                     []],
#                     dtype=object)
# videoFiles = "videoFiles"
# video_names = to_multi_index(videoFiles,experiment_data["dataset_dic"][videoFiles])
# # print (video_names)
# # mi_video_names = pd.MultiIndex.from_arrays(video_names)
# df_video_names = pd.DataFrame(data=video_names[1],index=video_names[0],columns=["dataset"])
# # print ("multiindex:",mi_video_names)
# print("Before")
# print ("dataframe:",df_video_names)
# df_video_names.set_index("dataset", inplace=True)
# print("After")
# print ("dataframe:",df_video_names)
# print("df_video_names:",df_video_names.to_latex())
# # table.loc[-1]=
# # print(table)
#
# # print(table.to_latex())
#
# # exit(1)
# table_tex = table.to_latex(index=True,header=False)
# frame_environ = encapsulate(table_tex,begin_frame,end_frame)
# latex+=frame_environ
# print (latex)
fname         = "example.tex"
fname_full    = os.path.join(os.sep,d.latex_test,fname)
du.save_text(fname_full,latex)
