import os
import argparse
import matplotlib.pyplot as plt
import matplotlib
# matplotlib.use("GTK3Agg")
# ['GTK3Agg', 'GTK3Cairo', 'MacOSX',
# 'nbAgg', 'Qt4Agg', 'Qt4Cairo', 'Qt5Agg',
# 'Qt5Cairo', 'TkAgg', 'TkCairo', 'WebAgg',
# 'WX', 'WXAgg', 'WXCairo', 'agg', 'cairo',
# 'pdf', 'pgf', 'ps', 'svg', 'template']
import numpy as np
from locations import directories as d
from dir_utils import dir_utils as du
from json_utils import json_utils as jutils
from ..report import figures
from scipy import ndimage
import scipy.stats as sps
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/sgd_adam_vgg_resnet_train_only_train_all_3585c247-c227-43b9-a3d6-4a983f74c4d1.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-27.01:56:56_3_2018-11-27.01:56:56.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-27.00:57:44_2_2018-11-27.00:57:44.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-27.00:00:56_1_2018-11-27.00:00:56.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-26.23:01:49_0_2018-11-26.23:01:49.json
# python -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json
# multiple_lr_3b9eeeb4-b36e-480f-8d13-ad9210c85284_2018-12-06.17:47:25_3.json
# /home/mazza/gitlab/noise_img_v2/model/multiple_lr_43bb5b4e-44fe-487e-b6f1-4382126748fa_2018-12-12.23:21:25_0.json
# /home/mazza/gitlab/noise_img_v2/model/multiple_lr_0c9c28e3-8fa7-4a51-94b2-d200d00da0ff_2018-12-11.09:24:51_0.json
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_d9a87421-6466-4f56-a8d1-650458f230c0_2018-12-14.17:18:23_0.json
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json"
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_52342efa-16c2-4fdb-ab91-9d262279ad8e_2019-04-16.01:57:21_0.json
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_e1fa3fa4-ce36-41e5-be8e-8e0ce344061a_2019-04-22.18:18:11_0.json


def figure_predictions(audio_dataset,predictions_val,
            predictions_training,cut,suptitle):
    epochs_range = range(len(loss))
    fig = plt.figure(figsize=(15,5))
    fig.suptitle(suptitle)
    ax2  = fig.add_subplot(1,1,1)
    audio_range = range(len(audio_dataset))
    ax2.plot(audio_range,audio_dataset,color='k',label="True $S_t$")
    ax2.plot(audio_range[:cut],predictions_training,color='b',label="Training $S_t$ prediction")
    ax2.plot(audio_range[cut:],predictions_val,color='r',label="Validation $S_t$ prediction")
    ax2.set_xlabel("Frame")
    ax2.set_ylabel("Noise intensity")
    ax2.legend()
    fig.tight_layout()
    return fig


parser = argparse.ArgumentParser()
parser.add_argument("--json")
args = parser.parse_args()

experiment_log_name = args.json
experiment_data = jutils.load_json(experiment_log_name)
print(experiment_data.keys())
res = []
# Paper
# Relatorio 
#  experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json
# 17 FC-512-128-1     > Maiores correlacoes
# 25 FC-512-1
#
# 19 FC-512-128-1     > Menores MSE  > melhores visualmente?
# 27 FC-512-1
# 35 FC-512-128-1  um pouco pior que 19 > explicar diferentes hiperparametros
index = 3
training_data_list  = experiment_data["training_config_list"]
network_config_list = experiment_data["network_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
targets             = experiment_data["targets"]
prediction_list     = experiment_data["prediction_list"]

training_config_dic = training_data_list[index]
val_loss = training_config_dic['val_loss']
loss = training_config_dic['loss']
predictions_val = prediction_list[index]["validation"]
predictions_training = prediction_list[index]["training"]
audio_dataset = np.hstack((targets["training"],targets["validation"]))
cut = dataset_dic["cut"]

fig_predictions = figure_predictions(audio_dataset,predictions_val,
                    predictions_training,cut,suptitle="")
save_figure = True
if save_figure:
    samples_directory = "samples/"
    du.make_dir(samples_directory)
    fname = "sample_index_"+str(index)+".eps"
    full_fname = os.path.join(samples_directory,fname)
    fig_predictions.savefig(full_fname)
# predictions_val -= np.array(dataset_dic["meansTrainY"])
# predictions_val*=np.array(dataset_dic["stdTrainY"])
# predictions_training-= np.array(dataset_dic["meansTrainY"])
# predictions_training*=np.array(dataset_dic["stdTrainY"])
# print("Before filtering")
# rho,pval = sps.spearmanr(predictions_val,audio_dataset[cut:])
# rho_train,pval_train = sps.spearmanr(predictions_training,audio_dataset[:cut])
# print("Validation: ",rho,pval)
# print("Training: ",rho_train,pval_train)


# print("After filtering")
# rho,pval = sps.spearmanr(predictions_val,audio_dataset[cut:])
# rho_train,pval_train = sps.spearmanr(predictions_training,audio_dataset[:cut])
# print("Validation: ",rho,pval)
# print("Training: ",rho_train,pval_train)
filtered_predictions_val = ndimage.filters.median_filter(predictions_val,size=3)

logloss = np.log10(loss)
logvalloss = np.log10(val_loss)
fig = figures(logloss,logvalloss,
            audio_dataset,predictions_val,
            predictions_training,cut,suptitle="")

f = plt.figure(figsize=(10,4))
ax = f.add_subplot(1,1,1)
rang = range(len(audio_dataset))
ax.plot(rang[cut:],audio_dataset[cut:],color='k',label="True $S_t$")
ax.plot(rang[cut:],predictions_val,color='r',label="Predicted $S_t$")
ax.set_ylabel("Noise intensity")
ax.set_xlabel("Frame")
ax.legend()
# f.suptitle(experiment_log_name+" Index:"+str(index))
f.tight_layout()

f_curves = plt.figure(figsize=(10,4))
ax_curves = f_curves.add_subplot(1,1,1)
rang_curves = range(len(logloss))
ax_curves.plot(rang_curves,logloss,color='b',label="train")
ax_curves.plot(rang_curves,logvalloss,color='r',label="validation")
ax_curves.set_ylabel("Log loss")
ax_curves.set_xlabel("Epoch")
ax_curves.legend()
# f_curves.suptitle(experiment_log_name+" Index:"+str(index))
f_curves.tight_layout()

# ax.plot(rang[cut:],filtered_predictions_val,'g-*')
# ax.plot(rang[cut:],filtered_predictions_val,'g--')
# ax.plot(rang[cut:],filtered_predictions_val,'g-')
# ax.plot(rang[:cut],predictions_training,color='b')

mse_original = ((predictions_val-audio_dataset[cut:])**2).mean()
mse_filtered = ((filtered_predictions_val-audio_dataset[cut:])**2).mean()

corr_original = sps.pearsonr(audio_dataset[cut:],predictions_val)
corr_filtered = sps.pearsonr(audio_dataset[cut:],filtered_predictions_val)

significant_audio = 12
significant_audio_locs = np.where(audio_dataset[cut:]>significant_audio)
# print((audio_dataset[cut:])[significant_audio_locs])
# print(np.array(predictions_val)[significant_audio_locs])
corr_significant_audio = sps.pearsonr((audio_dataset[cut:])[significant_audio_locs],np.array(predictions_val)[significant_audio_locs])
print("corr_significant_audio:",corr_significant_audio)
print("Pearson correlation before filtering:",corr_original)
# res.append(corr_original[0])
print("MSE before filtering:",mse_original)
print("Pearson correlation after filtering:",corr_filtered)
print("MSE after filtering:",mse_filtered)

plt.show()
# array = np.array(res)
# print(np.argsort(array))