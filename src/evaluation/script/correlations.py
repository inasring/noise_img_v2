import os
import argparse
import matplotlib.pyplot as plt
import matplotlib
# matplotlib.use("GTK3Agg")
# ['GTK3Agg', 'GTK3Cairo', 'MacOSX',
# 'nbAgg', 'Qt4Agg', 'Qt4Cairo', 'Qt5Agg',
# 'Qt5Cairo', 'TkAgg', 'TkCairo', 'WebAgg',
# 'WX', 'WXAgg', 'WXCairo', 'agg', 'cairo',
# 'pdf', 'pgf', 'ps', 'svg', 'template']
import numpy as np
from locations import directories as d
from dir_utils import dir_utils as du
from json_utils import json_utils as jutils
from ..report import figures
from scipy import ndimage
import scipy.stats as sps
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/sgd_adam_vgg_resnet_train_only_train_all_3585c247-c227-43b9-a3d6-4a983f74c4d1.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-27.01:56:56_3_2018-11-27.01:56:56.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-27.00:57:44_2_2018-11-27.00:57:44.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-27.00:00:56_1_2018-11-27.00:00:56.json
# python3.5 -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/multiple_lr_7bc516f3-ee5b-445f-9c74-f81a999028b0_2018-11-26.23:01:49_0_2018-11-26.23:01:49.json
# python -m evaluation.script.display_figure --json /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json
# multiple_lr_3b9eeeb4-b36e-480f-8d13-ad9210c85284_2018-12-06.17:47:25_3.json
# /home/mazza/gitlab/noise_img_v2/model/multiple_lr_43bb5b4e-44fe-487e-b6f1-4382126748fa_2018-12-12.23:21:25_0.json
# /home/mazza/gitlab/noise_img_v2/model/multiple_lr_0c9c28e3-8fa7-4a51-94b2-d200d00da0ff_2018-12-11.09:24:51_0.json
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_d9a87421-6466-4f56-a8d1-650458f230c0_2018-12-14.17:18:23_0.json
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json"
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_52342efa-16c2-4fdb-ab91-9d262279ad8e_2019-04-16.01:57:21_0.json
# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_e1fa3fa4-ce36-41e5-be8e-8e0ce344061a_2019-04-22.18:18:11_0.json

parser = argparse.ArgumentParser()
parser.add_argument("--json")
args = parser.parse_args()

experiment_log_name = args.json
experiment_data = jutils.load_json(experiment_log_name)
print(experiment_data.keys())
res = []
for index in range(64):
    training_data_list  = experiment_data["training_config_list"]
    network_config_list = experiment_data["network_config_list"]
    dataset_dic         = experiment_data["dataset_dic"]
    targets             = experiment_data["targets"]
    prediction_list     = experiment_data["prediction_list"]

    training_config_dic = training_data_list[index]
    val_loss = training_config_dic['val_loss']
    loss = training_config_dic['loss']
    predictions_val = prediction_list[index]["validation"]
    predictions_training = prediction_list[index]["training"]
    audio_dataset = np.hstack((targets["training"],targets["validation"]))
    cut = dataset_dic["cut"]

    mse_original = ((predictions_val-audio_dataset[cut:])**2).mean()
    corr_original = sps.pearsonr(audio_dataset[cut:],predictions_val)

    print("Pearson correlation before filtering:",corr_original)
    res.append(corr_original[0])
    # print("MSE before filtering:",mse_original)
array = np.array(res)
argsort = np.argsort(array)[::-1]
print(argsort)
print(array[argsort])