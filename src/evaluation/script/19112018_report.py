import os
from locations import directories as d
from ..report import report_tables_figures
from ..report import report_latex

experiment_data_filename = "sgd_adam_vgg_resnet_train_only_train_all_596325d2-83d2-4d9b-a20a-aeac5a4ff443.json"
experiment_data_filename = os.path.join(os.sep,d.modelDir,experiment_data_filename)
print(experiment_data_filename)
save_name = "experiment_19112018.tex"
summary_rename = {"Global Average Pooling":"GAP",
                    "Train FC only":"T_FC_O"}
summary_columns=["Network","Optimizer",
                "Global Average Pooling","Train FC only",
                "FC","param_count","epochs",
                "min_loss","min_val_loss"]
save_figure_dir  = d.latex_19112018_figures

report_tables = report_tables_figures(experiment_data_filename,
                        summary_rename,
                        summary_columns,
                        figure_dir=save_figure_dir,
                        save_fig=True)
report_latex(report_tables,
                figure_dir=save_figure_dir,
                latex_dir=d.latex_19112018,
                save_name=save_name,
                save_tex=True)
