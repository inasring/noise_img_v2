import uuid
import os
from copy import deepcopy
from datetime import datetime
import numpy as np
import pandas as pd
import telepot
from locations import directories as d
from json_utils import json_utils
from dir_utils import dir_utils as du
from evaluation import report
from telegram import bot_utils


if __name__ == "__main__":

    # Small Dataset
    # audio_dataset_name  = d.numpyDataset+"small_audio_dataset_2018-11-05.15:54:33.npy"
    # dataset_json_name   = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.json"
    # image_dataset_name  = d.numpyDataset+"small_image_dataset_2018-11-05.15:54:33.npy"

    # Real Dataset
    # audio_dataset_name = d.numpyDataset+"audio_experiment_videos_1638_2018-11-22.13:56:07.npy"
    # dataset_json_name  = d.numpyDataset+"image_experiment_videos_1638_2018-11-22.13:56:07.json"
    # image_dataset_name = d.numpyDataset+"image_experiment_videos_1638_2018-11-22.13:56:07.npy"
    save_figure_dir  = d.latex_26112018_v4_figures
    latex_dir = d.latex_26112018_v4
    clipnorm = 2
    save_name = "experiment_lr_26112018_v4_clipnorm_"+str(clipnorm)+"_repetition_loadmodel"
    experiment_logs = ["multiple_lr_3b9eeeb4-b36e-480f-8d13-ad9210c85284_2018-12-06.15:21:47_0.json",
                        "multiple_lr_3b9eeeb4-b36e-480f-8d13-ad9210c85284_2018-12-06.16:10:46_1.json",
                        "multiple_lr_3b9eeeb4-b36e-480f-8d13-ad9210c85284_2018-12-06.16:58:00_2.json",
                        "multiple_lr_3b9eeeb4-b36e-480f-8d13-ad9210c85284_2018-12-06.17:47:25_3.json"]
    experiment_logs = [d.modelDir+filename for filename in experiment_logs]
    all_summary_df = pd.DataFrame()
    latex_filenames = []
    for experiment_index in range(4):
        experiment_log_name = experiment_logs[experiment_index]
        experiment_name = "multiple_lr"
        save_name_many = save_name+"_"+str(experiment_index)+".tex"
        save_name_many_full = os.path.join(os.sep,latex_dir,save_name_many)
        latex_filenames.append(save_name_many_full)
        string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
        ### Gen Latex
        summary_rename = {"Global Average Pooling":"GAP",
                            "Train FC only":"T_FC","epochs":"Ep",
                            "Optimizer":"Opt","minimum_loss":"t_loss",
                            "minimum_val_loss":"v_loss"}
        summary_columns=["Network","Optimizer",
                        "Global Average Pooling","Train FC only",
                        "FC","lr_list","params","epochs",
                        "minimum_loss","minimum_val_loss",
                        "v_MSE","v_MAE"]
        du.make_dir(latex_dir)
        du.make_dir(save_figure_dir)
        print("Using experiment log file:", experiment_log_name)
        report_tables = report.report_tables_figures(experiment_log_name,
                                summary_rename,
                                summary_columns,
                                figure_dir=save_figure_dir,
                                max_loss=1e3,
                                save_fig=True)
        print("report_tables['figure_name_list']=",report_tables["figure_name_list"])
        report.report_latex(report_tables,
                        figure_dir=save_figure_dir,
                        latex_dir=latex_dir,
                        save_name=save_name_many,
                        save_tex=True,
                        add_summary=False)
        all_summary_df = all_summary_df.append(report_tables["summary"],ignore_index=True)

    all_summary_df_str= all_summary_df.to_latex(index=True)
    adjust_begin = '\\begin{adjustwidth}{-4cm}{}\n'
    adjust_end   = '\\end{adjustwidth}'
    all_summary_df_str = report.encapsulate(all_summary_df_str,adjust_begin,adjust_end)
    summary_save_name="summary.tex"
    summary_fname_full    = os.path.join(os.sep,latex_dir,summary_save_name)
    latex_filenames.append(summary_fname_full)
    du.save_text(summary_fname_full,all_summary_df_str)

    report_name     = "report"+"_"+string_date
    report_tex_name= report_name+".tex"
    report_pdf_name= report_name+".pdf"
    report_fname_full    = os.path.join(os.sep,latex_dir,report_tex_name)
    report_fname_full_pdf= os.path.join(os.sep,latex_dir,report_pdf_name)
    tex_main_report = report.main_report(latex_filenames,latex_dir)
    du.save_text(report_fname_full,tex_main_report)
    report_fname_full=os.path.realpath(report_fname_full)
    report_fname_full_pdf=os.path.realpath(report_fname_full_pdf)
    latex_dir_real   = os.path.realpath(latex_dir)
    report.pdflatex(report_fname_full,output_directory=latex_dir_real,verbose = True)
