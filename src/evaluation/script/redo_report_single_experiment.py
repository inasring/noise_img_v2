import os
import pandas as pd
from datetime import datetime
from locations import directories as d
from evaluation import report
# from json_utils import json_utils
from dir_utils import dir_utils as du
# experiment_log_name = "../model/multiple_lr_0c9c28e3-8fa7-4a51-94b2-d200d00da0ff_2018-12-11.09:24:51_0.json"
# experiment_log_name = "experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_d9a87421-6466-4f56-a8d1-650458f230c0_2018-12-14.17:18:23_0.json"
experiment_log_name = "experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_1cd786b0-8a82-477b-afc8-deba96579519_2019-05-15.06:27:55_0.json"
experiment_log_name_full = "../model/"+experiment_log_name
clipnorm=1
#string_date = "2018-12-14.17:18:23"+"_redo_"
string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
save_figure_dir  = d.latex_09122018_figures
latex_dir = d.latex_09122018
latex_filenames = []
# save_name = "experiment_lr_09122018_v1_clipnorm_"+str(clipnorm)+"_repetition_loadmodel_redo"
save_name = os.path.splitext(experiment_log_name)[0]+".tex"
save_name_full = os.path.join(os.sep,latex_dir,save_name)
latex_filenames.append(save_name_full)
summary_rename = {"Global Average Pooling":"GAP",
                    "Train FC only":"T_FC","epochs":"Ep",
                    "Optimizer":"Opt","minimum_loss":"t_loss",
                    "minimum_val_loss":"v_loss"}
summary_columns=["Network","Optimizer",
                "Global Average Pooling","Train FC only",
                "FC","lr_list","params","epochs",
                "minimum_loss","minimum_val_loss",
                "v_MSE","v_MAE"]
print("Using experiment log file:", experiment_log_name_full)
report_tables = report.report_tables_figures(experiment_log_name_full,
                        summary_rename,
                        summary_columns,
                        figure_dir=save_figure_dir,
                        max_loss=1e3,
                        save_fig=True)
report.report_latex(report_tables,
                figure_dir=save_figure_dir,
                latex_dir=latex_dir,
                save_name=save_name,
                save_tex=True,
                add_summary=False)
all_summary_df = pd.DataFrame()
all_summary_df = all_summary_df.append(report_tables["summary"],ignore_index=True)

all_summary_df_str= all_summary_df.to_latex(index=True)
adjust_begin = '\\begin{adjustwidth}{-4cm}{}\n'
adjust_end   = '\\end{adjustwidth}'
all_summary_df_str = report.encapsulate(all_summary_df_str,adjust_begin,adjust_end)
summary_save_name="summary.tex"
summary_fname_full    = os.path.join(os.sep,latex_dir,summary_save_name)
latex_filenames.append(summary_fname_full)
du.save_text(summary_fname_full,all_summary_df_str)

report_name     = "report"+"_"+string_date
report_tex_name= report_name+".tex"
report_pdf_name= report_name+".pdf"
report_fname_full    = os.path.join(os.sep,latex_dir,report_tex_name)
report_fname_full_pdf= os.path.join(os.sep,latex_dir,report_pdf_name)
tex_main_report = report.main_report(latex_filenames,latex_dir)
du.save_text(report_fname_full,tex_main_report)
report_fname_full=os.path.realpath(report_fname_full)
report_fname_full_pdf=os.path.realpath(report_fname_full_pdf)
latex_dir_real   = os.path.realpath(latex_dir)
report.pdflatex(report_fname_full,output_directory=latex_dir_real,verbose = True)
