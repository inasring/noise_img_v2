import os
from locations import directories as d
from ..report import report_tables_figures
from ..report import report_latex

experiment_data_filename = "test_multi_dataset_version_5962ab28-8371-4191-abd1-c735c3174d78.json"
experiment_data_filename = os.path.join(os.sep,d.modelDir,experiment_data_filename)
print(experiment_data_filename)

summary_rename = {"Global Average Pooling":"GAP",
                    "Train FC only":"T_FC","epochs":"Ep",
                    "Optimizer":"Opt","minimum_loss":"t_loss",
                    "minimum_val_loss":"v_loss"}
summary_columns=["Network","Optimizer",
                "Global Average Pooling","Train FC only",
                "FC","params","epochs",
                "minimum_loss","minimum_val_loss",
                "v_MSE","v_MAE"]
save_figure_dir  = d.latex_test_figures

report_tables = report_tables_figures(experiment_data_filename,
                        summary_rename,
                        summary_columns,
                        figure_dir=save_figure_dir,
                        save_fig=True)
report_latex(report_tables,
                figure_dir=save_figure_dir,
                latex_dir=d.latex_test,
                save_name=save_name,
                save_tex=True)
