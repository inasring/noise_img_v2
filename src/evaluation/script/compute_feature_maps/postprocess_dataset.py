import os

import numpy as np
from keras.models import load_model,Model

from locations import directories as d
from dir_utils import dir_utils as du
from json_utils import json_utils as jutils
import definitions as defs

# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json"
# python -m evaluation.script.compute_feature_maps.preprocess_dataset
experiment_log_name = os.path.join(os.sep,d.modelDir,"experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json")
experiment_data = jutils.load_json(experiment_log_name)
print(experiment_data.keys())
index = 27 # Best model with linear regression output

training_data_list  = experiment_data["training_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
training_config_dic = training_data_list[index]
model_filename_path = os.path.join(d.modelDir,training_config_dic["model_save_name"])
cut  = dataset_dic["cut"]

network_intermediate_output_filename = os.path.join(d.preprocessed_outputs,"network_intermediate_output.npy")
network_output_filename = os.path.join(d.preprocessed_outputs,"network_output.npy")

network_intermediate_output = np.load(network_intermediate_output_filename,mmap_mode='r')
network_output = np.load(network_output_filename,mmap_mode='r')

sample_size = 20

sample_intermediate_output = network_intermediate_output[:sample_size]
sample_output = network_output[:sample_size]

model_filename_path = os.path.join(d.modelDir,training_config_dic["model_save_name"])
model = load_model(model_filename_path)
model.summary()

dense_layer = model.get_layer("dense_1")
weights,bias = dense_layer.get_weights()
weights = weights.squeeze()
# print("dense_layer.get_weights()=",dense_layer.get_weights())
print("weights.shape=",weights.shape)
print("bias=",bias)
print("sample_output=\n",sample_output)


#GLOBAL AVERAGE POOLING >>>>>> LEMBRAR DA PARTE DO AVERAGE
manual_output_frames_sample = np.einsum("mijk,k->mij",sample_intermediate_output,weights)

print (manual_output_frames_sample.shape)
# print (manual_output_sample)
manual_output_sample = np.einsum("mij->m",manual_output_frames_sample)
reduce_dim = sample_intermediate_output.shape[1]*sample_intermediate_output.shape[2]
manual_output_sample = manual_output_sample/reduce_dim+bias
print (manual_output_sample.shape)
print (manual_output_sample-sample_output.squeeze())

manual_output_sample_full_einstein = np.einsum("mijk,k->m",sample_intermediate_output,weights)
manual_output_sample_full_einstein/=reduce_dim
manual_output_sample_full_einstein+=bias



manual_output_full_einstein = np.einsum("mijk,k->m",network_intermediate_output[:cut],weights)
manual_output_full_einstein/=reduce_dim
manual_output_full_einstein+=bias

error_vec = manual_output_full_einstein-network_output[:cut].squeeze()
print(error_vec)
print(np.sqrt(error_vec.dot(error_vec)/len(error_vec)))


# print(manual_output_full_einstein-sample_output.squeeze())