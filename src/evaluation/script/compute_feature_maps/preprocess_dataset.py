import os

import numpy as np
from keras.models import load_model,Model

from locations import directories as d
from dir_utils import dir_utils as du
from json_utils import json_utils as jutils
import definitions as defs

# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json"
# python -m evaluation.script.compute_feature_maps.preprocess_dataset
experiment_log_name = os.path.join(os.sep,d.modelDir,"experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json")
experiment_data = jutils.load_json(experiment_log_name)
print(experiment_data.keys())
index = 27 # Best model with linear regression output

training_data_list  = experiment_data["training_config_list"]
network_config_list = experiment_data["network_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
targets             = experiment_data["targets"]
prediction_list     = experiment_data["prediction_list"]


predictions_val = prediction_list[index]["validation"]
predictions_training = prediction_list[index]["training"]
training_config_dic = training_data_list[index]
print("dataset_dic",dataset_dic.keys())
print("dataset numpy: ",dataset_dic["imageDatasetName"])
separator = ".."
dataset_filename = separator+dataset_dic["imageDatasetName"].split(separator)[-1]
print("dataset_filename: ",dataset_filename)
dataset_log = jutils.load_json(dataset_filename+".json")
print("dataset_log.keys(): ",dataset_log.keys())
dataset_numpy_filename = dataset_log["imageDatasetName"]+".npy"
print("dataset_numpy_filename: ",dataset_numpy_filename)
print("model_save_name=",training_config_dic["model_save_name"])
model_filename_path = os.path.join(d.modelDir,training_config_dic["model_save_name"])

meansTrainX = dataset_dic["meansTrainX"]
stdTrainX   = dataset_dic["stdTrainX"]
cut  = dataset_dic["cut"]


val_loss = training_config_dic['val_loss']
loss = training_config_dic['loss']
print("val_loss: ",val_loss)
print("loss: ",loss)


print("Loading network...")
model = load_model(model_filename_path)
model.summary()
intermediate_layer_name = "block5_pool"
intermediate_layer = model.get_layer(intermediate_layer_name).output
intermediate_model = Model(inputs = [model.input],
                          outputs=[intermediate_layer])

print("Loading Data...")
dataset = np.load(dataset_numpy_filename)
dataset-=meansTrainX
dataset/=stdTrainX

directory = "preprocessed_outputs/"
network_output = model.predict(dataset)
network_output_filename_full = os.path.join(directory,"network_output")
np.save(network_output_filename_full,network_output)
intermediate_network_output_filename_full = os.path.join(directory,"network_intermediate_output")
network_intermediate_output = intermediate_model.predict(dataset)
np.save(intermediate_network_output_filename_full,network_intermediate_output)



