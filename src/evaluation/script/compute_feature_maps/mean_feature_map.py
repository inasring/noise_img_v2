import os

import numpy as np
from keras.models import load_model,Model
import cv2
from PIL import Image

from locations import directories as d
from dir_utils import dir_utils as du
from json_utils import json_utils as jutils
import definitions as defs

def upsample_array(array,target_shape_tuple):
    ret = np.array(Image.fromarray(array.astype(np.float32)).resize(target_shape_tuple))
    return ret

# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json"
# python -m evaluation.script.compute_feature_maps.preprocess_dataset
experiment_log_name = os.path.join(os.sep,d.modelDir,"experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json")
experiment_data = jutils.load_json(experiment_log_name)
print(experiment_data.keys())
index = 27 # Best model with linear regression output

training_data_list  = experiment_data["training_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
training_config_dic = training_data_list[index]
model_filename_path = os.path.join(d.modelDir,training_config_dic["model_save_name"])
cut  = dataset_dic["cut"]
meansTrainX = dataset_dic["meansTrainX"]
stdTrainX   = dataset_dic["stdTrainX"]

network_intermediate_output_filename = os.path.join(d.preprocessed_outputs,"network_intermediate_output.npy")
network_output_filename = os.path.join(d.preprocessed_outputs,"network_output.npy")

network_intermediate_output = np.load(network_intermediate_output_filename,mmap_mode='r')
network_output = np.load(network_output_filename,mmap_mode='r')


model_filename_path = os.path.join(d.modelDir,training_config_dic["model_save_name"])
model = load_model(model_filename_path)
model.summary()
intermediate_layer_name = "block5_pool"
intermediate_layer = model.get_layer(intermediate_layer_name).output
intermediate_model = Model(inputs = [model.input],
                          outputs=[intermediate_layer])

dense_layer = model.get_layer("dense_1")
weights,bias = dense_layer.get_weights()
weights = weights.squeeze()
# print("dense_layer.get_weights()=",dense_layer.get_weights())
print("weights.shape=",weights.shape)
print("bias=",bias)

reduce_dim = network_intermediate_output.shape[1]*network_intermediate_output.shape[2]
#GLOBAL AVERAGE POOLING >>>>>> LEMBRAR DA PARTE DO AVERAGE
manual_output_full_einstein = np.einsum("mijk,k->m",network_intermediate_output[:cut],weights)
manual_output_full_einstein/=reduce_dim
manual_output_full_einstein+=bias

manual_feature_map = np.einsum("mijk,k->mij",network_intermediate_output[:cut],weights)
print("manual_feature_map.shape",manual_feature_map.shape)

error_vec = manual_output_full_einstein-network_output[:cut].squeeze()
print(error_vec)
print(np.sqrt(error_vec.dot(error_vec)/len(error_vec)))

video_path = "../dataset/240x240/M2U00003.MPG"
cap = cv2.VideoCapture(video_path)
length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
n_samples_for_mean = 8000
ret,frame = cap.read()



frame_number = 0
while frame_number<n_samples_for_mean:
    ret,frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    ## Preprocessing
    frame_float = np.array(frame,dtype=np.float32)
    input_data = np.copy(frame_float[np.newaxis,:,:,:])
    input_data-= meansTrainX
    input_data/= stdTrainX
    intermediate_out = intermediate_model.predict(input_data)
    complete_out     = model.predict(input_data)
    
    ## Weighted feature map
    manual_feature_map = np.einsum("mijk,k->mij",intermediate_out,weights)
    nrows = manual_feature_map.shape[-2]
    ncols = manual_feature_map.shape[-1]
    if frame_number == 0:
        running_mean = np.zeros((nrows,ncols))
    # print(nrows,ncols)
    manual_feature_map /= (ncols*nrows)
    manual_feature_map = manual_feature_map.squeeze()
    
    running_mean = ((frame_number+1)*running_mean+manual_feature_map)/(frame_number+2)
    manual_output      = manual_feature_map.sum()+bias
    
    frame_number+=1
    if frame_number%30==0:
        print("frame_number:",frame_number," length:",length)
        print("running_mean=\n",running_mean)
print("running_mean:",running_mean)
np.save("mean_feature_map_matrix",running_mean)
# print(manual_output_full_einstein-sample_output.squeeze())


