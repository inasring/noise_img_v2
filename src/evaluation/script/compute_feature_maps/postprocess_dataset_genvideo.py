import os

import numpy as np
from keras.models import load_model,Model
import cv2
import PIL
from PIL import Image

from locations import directories as d
from dir_utils import dir_utils as du
from json_utils import json_utils as jutils
import definitions as defs

def upsample_array(array,target_shape_tuple):
    # ret = np.array(Image.fromarray(array.astype(np.float32)).resize(target_shape_tuple,resample=PIL.Image.BICUBIC))
    ret = np.array(Image.fromarray(array.astype(np.float32)).resize(target_shape_tuple))
    return ret


# /home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json"
# python -m evaluation.script.compute_feature_maps.preprocess_dataset
experiment_log_name = os.path.join(os.sep,d.modelDir,"experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_cbc9eaf9-28c6-492b-ad8c-adcc23931cc3_2018-12-16.00:41:41_0.json")
experiment_data = jutils.load_json(experiment_log_name)
print(experiment_data.keys())
index = 27 # Best model with linear regression output

training_data_list  = experiment_data["training_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
training_config_dic = training_data_list[index]
model_filename_path = os.path.join(d.modelDir,training_config_dic["model_save_name"])
cut  = dataset_dic["cut"]
meansTrainX = dataset_dic["meansTrainX"]
stdTrainX   = dataset_dic["stdTrainX"]

network_intermediate_output_filename = os.path.join(d.preprocessed_outputs,"network_intermediate_output.npy")
network_output_filename = os.path.join(d.preprocessed_outputs,"network_output.npy")

network_intermediate_output = np.load(network_intermediate_output_filename,mmap_mode='r')
network_output = np.load(network_output_filename,mmap_mode='r')


model_filename_path = os.path.join(d.modelDir,training_config_dic["model_save_name"])
model = load_model(model_filename_path)
model.summary()
intermediate_layer_name = "block5_pool"
intermediate_layer = model.get_layer(intermediate_layer_name).output
intermediate_model = Model(inputs = [model.input],
                          outputs=[intermediate_layer])

dense_layer = model.get_layer("dense_1")
weights,bias = dense_layer.get_weights()
weights = weights.squeeze()
# print("dense_layer.get_weights()=",dense_layer.get_weights())
print("weights.shape=",weights.shape)
print("bias=",bias)

reduce_dim = network_intermediate_output.shape[1]*network_intermediate_output.shape[2]
#GLOBAL AVERAGE POOLING >>>>>> LEMBRAR DA PARTE DO AVERAGE
manual_output_full_einstein = np.einsum("mijk,k->m",network_intermediate_output[:cut],weights)
manual_output_full_einstein/=reduce_dim
manual_output_full_einstein+=bias

manual_feature_map = np.einsum("mijk,k->mij",network_intermediate_output[:cut],weights)
print("manual_feature_map.shape",manual_feature_map.shape)

error_vec = manual_output_full_einstein-network_output[:cut].squeeze()
print(error_vec)
print(np.sqrt(error_vec.dot(error_vec)/len(error_vec)))

import matplotlib.pyplot as plt
fig = plt.figure(0,figsize=(15,7))
# ax2  = fig.add_subplot(111)
ax  = fig.add_subplot(131)
ax2  = fig.add_subplot(132)
ax3  = fig.add_subplot(133)
# ax  = fig.add_subplot(221)
# ax2  = fig.add_subplot(224)

mean_feature_map = np.load("mean_feature_map.npy")
###Validar no video 5
# video_path = "../dataset/240x240/M2U00005.MPG"
# video_path = "../dataset/240x240/M2U00006.MPG"
# video_path = "../dataset/240x240/M2U00003.MPG"
video_path = "../dataset/240x240/M2U00008.MPG"
du.make_dir(d.demos)
du.make_dir(d.demo_figures)
video_name = du.filenameNoPath(du.filenameNoExtension(video_path))
video_name_full = os.path.join(d.demos,video_name)+"_demo.mp4"

cap = cv2.VideoCapture(video_path)
first_frame = 3900
cap.set(cv2.CAP_PROP_POS_FRAMES, first_frame)
length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
frame_rate = cap.get(cv2.CAP_PROP_FPS)
ret,frame = cap.read()
frame_shape = frame.shape
im  = ax.imshow(frame)
# im2 = ax2.imshow(np.zeros((240,240),dtype=np.uint8),cmap="viridis",vmin=0.,vmax=1.)
im2 = ax2.imshow(np.zeros((240,240),dtype=np.uint8),cmap="viridis",vmin=0.,vmax=0.5)
ax2.set_title("Class Activation Map")
im3 = ax3.imshow(np.zeros((240,240),dtype=np.float32))
ax3.set_title("Filtered CAM")
# im2 = ax2.imshow(np.zeros((240,240),dtype=np.float32),cmap="viridis",vmin=0.,vmax=1.)

from matplotlib.animation import FFMpegWriter
writer = FFMpegWriter(fps=frame_rate)

frame_number = first_frame
# online_mean_feature_map = np.copy(mean_feature_map)
online_mean_feature_map = np.zeros_like(mean_feature_map)
manual_feature_map_km1  = np.zeros_like(mean_feature_map)
# with writer.saving(fig,video_name_full,100):
while frame_number<length:
    ret,frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    ## Preprocessing
    frame_float = np.array(frame,dtype=np.float32)
    input_data = np.copy(frame_float[np.newaxis,:,:,:])
    input_data-= meansTrainX
    input_data/= stdTrainX
    intermediate_out = intermediate_model.predict(input_data)
    complete_out     = model.predict(input_data)
    
    ## Weighted feature map
    manual_feature_map = np.einsum("mijk,k->mij",intermediate_out,weights)
    nrows = manual_feature_map.shape[-2]
    ncols = manual_feature_map.shape[-1]
    # print(nrows,ncols)
    manual_feature_map /= (ncols*nrows)
    manual_feature_map = manual_feature_map.squeeze()

    ## Mask
    flattened          = np.copy(manual_feature_map)
    flattened          = flattened.flatten()
    flattened          = np.sort(flattened)
    selected_number    = len(flattened)//5
    threshold_position = len(flattened)-selected_number
    threshold = flattened[threshold_position]
    threshold = 0.32
    # print(threshold)
    manual_feature_map_upsampled = upsample_array(manual_feature_map,frame_shape[:-1])
    # mask = manual_feature_map_upsampled>threshold
    # nonzero = np.count_nonzero(mask)
    # threshold_map = np.where(mask)
    # mask = mask.astype(np.float32)
    # # frame[threshold_map,1] = 255
    manual_output      = manual_feature_map.sum()+bias
    im.set_data(frame)
    # im2.set_data(frame)
    # im2.set_data(mask.astype(np.uint8))
    # rand_data = np.random.random((240,240)).astype(np.float32)
    # im2.set_data(mask)
    im2.set_data(manual_feature_map_upsampled)
    

    ## Merge
    # percent_image = 0.2
    # print(np.max(manual_feature_map_upsampled))
    # print(np.min(manual_feature_map_upsampled))
    # color_tensor = (plt.cm.get_cmap('viridis')(manual_feature_map_upsampled))[:,:,:3]
    # merge = frame_float/255*percent_image+(1-percent_image)*color_tensor
    # im3.set_data(merge)
    manual_feature_map_display = np.copy(manual_feature_map)
    online_keep = 0.95
    online_change = 1-online_keep
    online_mean_feature_map = online_mean_feature_map*online_keep+manual_feature_map_km1*online_change
    manual_feature_map_display-= online_mean_feature_map
    manual_feature_map_display*=10
    manual_feature_map_display = np.maximum(np.minimum(manual_feature_map_display,1),0)

    # manual_feature_map_display+=0.2
    # print("manual_feature_map_display: \n",np.min(manual_feature_map_display))
    # print("manual_feature_map_display: \n",np.max(manual_feature_map_display))
    manual_feature_map_display_upsampled = upsample_array(manual_feature_map_display,frame_shape[:-1])
    frame_float = frame_float/255
    frame_float[:,:,1] = manual_feature_map_display_upsampled
    # print("np.argmax(frame_float):",frame_float.argmax())
    # print(np.where(frame_float==np.max(frame_float)))
    im3.set_data(frame_float)
    ax.set_title("Frame:"+str(frame_number))

    if frame_number%40 == 0:
        print("Frame: ",str(frame_number),"complete_out=",complete_out," manual_output=",manual_output, "error=",complete_out-manual_output)
        # print("np.max rand_data=",np.max(rand_data))
        # print("np.min rand_data=",np.min(rand_data))
    plt.draw()
    plt.pause(0.0001)
    if frame_number>0:
        f_name = video_name+"_"+str(frame_number).zfill(5)+".png"
        f_name_full = os.path.join(d.demo_figures,f_name)
        fig.tight_layout()
        # fig.savefig(f_name_full)
    manual_feature_map_km1 = manual_feature_map
    frame_number+=1
        # writer.grab_frame()



# print(manual_output_full_einstein-sample_output.squeeze())


