from keras.models import load_model
filename = "/home/mazza/gitlab/noise_img_v2/model/noiseimg_Adam_ad3964ce-2f6b-49b5-8a63-ba341324e6e5_VGG16_2018-12-14.17:18:23.hdf5"
model = load_model(filename)
model.summary()
dataset_filename = "/home/mazza/gitlab/noise_img_v2/dataset/numpyDataset/image_experiment_videos_1638_2018-10-25.19:04:55.npy"
import numpy as np
dataset = np.load(dataset_filename)
print(model.get_layer("block5_conv3").output)

from keras.models import Model
intermediate_model = Model(inputs=[model.input],outputs=model.get_layer("block5_conv3").output)
cut = 3750
train_inputs       = dataset[:cut]
validation_inputs  = dataset[cut:]
#preprocessing> data = (data-media)/desvio
training_mean = train_inputs.mean(axis=(0,1,2))
training_std  = train_inputs.std(axis=(0,1,2))
train_inputs -= training_mean
train_inputs /= training_std
validation_inputs -= training_mean
validation_inputs /= training_std

network_outputs = intermediate_model.predict(validation_inputs)

weights,bias = model.get_layer("dense_1").get_weights()
weights = weights.squeeze()
print(network_outputs.shape)
network_outputs = np.average(network_outputs,axis=3,weights=weights)
print(network_outputs.shape)

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation, rc
from PIL import Image
matplotlib.rcParams['animation.embed_limit'] = 2**128
cmap = plt.get_cmap("viridis")
from IPython.display import HTML
fig = plt.figure(figsize=(20,5))
current_image = 40
ax  = fig.add_subplot(121)
dataset_image = (validation_inputs[current_image]*training_std)+training_mean
print ("network_outputs.shape=",network_outputs.shape)
print("dataset_image.shape=",dataset_image.shape)
network_outputs = np.diff(network_outputs,axis=0)
minimum = np.min(network_outputs)
maximum = np.max(network_outputs)
network_outputs -= minimum
network_outputs /=(maximum-minimum)
network_image = network_outputs[current_image]
print("value range network outs:",np.min(network_outputs),np.max(network_outputs))
print("value range images:",np.min((validation_inputs*training_std+training_mean)/255),np.max((validation_inputs*training_std+training_mean)/255))
print ("network_image.shape=",network_image.shape)
im  = ax.imshow(dataset_image.squeeze()/255)
#ax2 = fig.add_subplot(132)
#im2 = ax2.imshow(network_image)
ax3 = fig.add_subplot(122)
blend_img = 0.2
blend_data= 1-blend_img
network_blend_image = Image.fromarray(network_image*blend_data)
network_blend_image = np.array(network_blend_image.resize(dataset_image.shape[:-1]))
network_blend_image = cmap(network_blend_image)[:,:,:-1]
blended_image = dataset_image/255*blend_img+blend_data*network_blend_image
im3 = ax3.imshow(blended_image)

def animate(i):
    current_image = i
    dataset_image = (validation_inputs[current_image]*training_std)+training_mean
    dataset_image = dataset_image.squeeze()/255
    im.set_data(dataset_image)
    network_image = network_outputs[current_image]
    #im2.set_data(network_image)
    network_blend_image = Image.fromarray(network_image*blend_data)
    network_blend_image = np.array(network_blend_image.resize(dataset_image.shape[:-1]))
    network_blend_image = cmap(network_blend_image)[:,:,:-1]
    blended_image = dataset_image*blend_img+blend_data*network_blend_image
    im3.set_data(blended_image)
#    print("value range network_image outs:",np.min(network_image),np.max(network_image))
#    print("value range dataset_image:",np.min(dataset_image),np.max(dataset_image))
#    print("value range blended_image:",np.min(blended_image),np.max(blended_image))
    #return (im,im2,im3)
    return (im,im3)

# anim = animation.FuncAnimation(fig, animate, init_func=None,
#                                frames=1000, interval=None, 
#                                blit=True)
anim = animation.FuncAnimation(fig, animate, init_func=None,
                               frames=1000, 
                               blit=True)                               
anim.save("visual.mp4",fps=29.97)
