import numpy as np
import cv2
from PIL import Image
from matplotlib import animation
import matplotlib.pyplot as plt

def process(cap,model,training_mean,training_std,
            frame_shape,out_shape,n_frames,weights,batch_size=32):
    shape = (n_frames,)+out_shape
    ret = np.empty(shape)
    # n_batches = n_frames//batch_size
    # residual  = n_frames%batch_size
    # accumulator_batch = np.empty((batch_size,)+frame_shape)
    # accumulator_residual=np.empty((residual,)+frame_shape)
    # for i in range(n_batches):
    #     for batch in range(batch_size):
    #         _,dataset_image = cap.read()
    #         # preproc_dataset_image = (dataset_image-training_mean)/training_std
    #         # accumulator_batch[batch] = preproc_dataset_image
    #         accumulator_batch[batch] = dataset_image
    #     accumulator_batch[batch]-=training_mean
    #     accumulator_batch[batch]/=training_std
    #     network_image = model.predict(accumulator_batch,batch_size=batch_size)
    #     network_image = np.average(network_image,axis=3,weights=weights)
    #     ret[batch_size*i:batch_size*(i+1)] = network_image
        
    # for batch in range(residual):
    #     _,dataset_image = cap.read()
    #     accumulator_residual[batch] = dataset_image
    # accumulator_residual-=training_mean
    # accumulator_residual/=training_std
    # network_image = model.predict(accumulator_residual)
    # network_image = np.average(network_image,axis=3,weights=weights)
    # if residual !=0:
    #     ret[batch_size*n_batches:] = network_image
    # # print("network_image.shape=",network_image.shape)
    # network_image = network_image.squeeze()
    for i in range(n_frames):
        _,dataset_image = cap.read()
        preproc_dataset_image = (dataset_image-training_mean)/training_std
        network_image = model.predict(preproc_dataset_image[np.newaxis,:,:,:])
        network_image = np.average(network_image,axis=3,weights=weights)
        ret[i] = np.copy(network_image)[0]
    return ret

class Visualization_animation():
    def __init__(self,cap,model,weights,
                training_mean,training_std,
                first_frame = 0,
                n_frames=1000,
                save_name="demo.mp4",
                save=False,
                blend_img=0.5):
        self.cap = cap
        self.model = model
        self.weights = weights
        self.training_mean = training_mean
        self.training_std = training_std
        self.first_frame = first_frame
        self.current_frame = first_frame
        self.save=save
        self.save_name=save_name
        self.save_fps = cap.get(cv2.CAP_PROP_FPS)
        self.n_frames = n_frames
        self.fig = plt.figure(figsize=(20,5))
        self.ax1 = self.fig.add_subplot(121)
        self.ax2 = self.fig.add_subplot(122)
        self.im1 = self.ax1.imshow(np.zeros((1,1,3)))
        self.im2 = self.ax2.imshow(np.zeros((1,1,3)))
        _,frame = self.cap.read()
        self.cap.set(cv2.CAP_PROP_POS_FRAMES,self.first_frame)
        self.frame_shape = frame.shape
        self.old_network_image = np.zeros((15,15))
        self.blend_img = blend_img
        self.blend_data = 1-blend_img
        self.cmap = plt.get_cmap("viridis")
        out_shape = (15,15) ####HARDCODED
        print("Preprocessing...")
        self.processed_data = process(self.cap,self.model,self.training_mean,self.training_std,
                                self.frame_shape,out_shape,self.n_frames,self.weights,batch_size=32)
        self.cap.set(cv2.CAP_PROP_POS_FRAMES,self.first_frame)
        self.processed_data = np.diff(self.processed_data,axis=0)
        minimum = np.min(self.processed_data)
        maximum = np.max(self.processed_data)
        self.processed_data-=minimum
        self.processed_data/=(maximum-minimum)
        self.index_counting = 0
    def animate_step(self):
        if self.current_frame%40 == 0:
            print("Frame:",self.current_frame)
        _, dataset_image = self.cap.read()
        dataset_image = np.float32(dataset_image)
        # preproc_dataset_image = (dataset_image-self.training_mean)/self.training_std
        # network_image = self.model.predict(preproc_dataset_image[np.newaxis,:,:,:])
        # # print("network_image.shape=",network_image.shape)
        # network_image = np.average(network_image,axis=3,weights=self.weights)
        # network_image = network_image.squeeze()
        dataset_image /= 255.
        self.im1.set_data(dataset_image)

        # network_image_plot = network_image-img_old
        # img_old = network_image
        network_image = self.processed_data[self.index_counting]
        # network_image_plot = network_image-self.old_network_image
        network_image_plot = network_image
        print(network_image)
        # self.old_network_image = network_image
        # print("network_image.shape=",network_image.shape)
        #im2.set_data(network_image)
        network_blend_image = Image.fromarray(network_image_plot*self.blend_data)
        network_blend_image = np.array(network_blend_image.resize(dataset_image.shape[:-1]))
        network_blend_image = self.cmap(network_blend_image)[:,:,:-1]
        blended_image = dataset_image*self.blend_img+self.blend_data*network_blend_image
        self.im2.set_data(blended_image)
    #    print("value range network_image outs:",np.min(network_image),np.max(network_image))
    #    print("value range dataset_image:",np.min(dataset_image),np.max(dataset_image))
    #    print("value range blended_image:",np.min(blended_image),np.max(blended_image))
        #return (im,im2,im3)
        self.current_frame+=1
        self.index_counting+=1
        return (self.im1,self.im2)
    def animation(self):
        def run_step_animate(frame):
            artists = self.animate_step()
            return artists
        anim = animation.FuncAnimation(self.fig, run_step_animate, init_func=None,
                                    frames=self.n_frames-1,
                                    blit=True)
        if self.save:
            anim.save(self.save_name,fps=self.save_fps)
        else:
            plt.show()
