import numpy as np
import cv2
from keras.models import load_model
from json_utils import json_utils as jutils
filename = "/home/mazza/gitlab/noise_img_v2/model/noiseimg_Adam_ad3964ce-2f6b-49b5-8a63-ba341324e6e5_VGG16_2018-12-14.17:18:23.hdf5"
model = load_model(filename)
model.summary()
# dataset_filename = "/home/mazza/gitlab/noise_img_v2/dataset/numpyDataset/image_experiment_videos_1638_2018-10-25.19:04:55.npy"
experiment_log_filename = "/home/mazza/gitlab/noise_img_v2/model/experiment_lr_09122018_v1_clipnorm_0.1_repetition_loadmodel_d9a87421-6466-4f56-a8d1-650458f230c0_2018-12-14.17:18:23_0.json"
experiment_data = jutils.load_json(experiment_log_filename)
training_data_list  = experiment_data["training_config_list"]
network_config_list = experiment_data["network_config_list"]
dataset_dic         = experiment_data["dataset_dic"]
prediction_list     = experiment_data["prediction_list"]

training_mean       = dataset_dic["meansTrainX"]
training_std        = dataset_dic["stdTrainX"]
cut = dataset_dic["cut"]


print(model.get_layer("block5_conv3").output)

from keras.models import Model
intermediate_model = Model(inputs=[model.input],outputs=model.get_layer("block5_conv3").output)
# train_inputs       = dataset[:cut]
# validation_inputs  = dataset[cut:]
#preprocessing> data = (data-media)/desvio

# network_outputs = intermediate_model.predict(validation_inputs)

weights,bias = model.get_layer("dense_1").get_weights()
weights = weights.squeeze()
# print(network_outputs.shape)
# network_outputs = np.average(network_outputs,axis=3,weights=weights)
# print(network_outputs.shape)

import matplotlib.pyplot as plt
from matplotlib import animation
from PIL import Image
# matplotlib.rcParams['animation.embed_limit'] = 2**128
# cmap = plt.get_cmap("viridis")
# fig = plt.figure(figsize=(20,5))
# ax  = fig.add_subplot(121)
# placeholder = np.zeros((240,240,3))
# dataset_image = placeholder
# print ("network_outputs.shape=",network_outputs.shape)
# print("dataset_image.shape=",dataset_image.shape)
# network_outputs = np.diff(network_outputs,axis=0)
# minimum = np.min(network_outputs)
# maximum = np.max(network_outputs)
# network_outputs -= minimum
# network_outputs /=(maximum-minimum)
# network_image = network_outputs[current_image]
# print("value range network outs:",np.min(network_outputs),np.max(network_outputs))
# print("value range images:",np.min((validation_inputs*training_std+training_mean)/255),np.max((validation_inputs*training_std+training_mean)/255))
# print ("network_image.shape=",network_image.shape)
# im  = ax.imshow(dataset_image.squeeze()/255)
#ax2 = fig.add_subplot(132)
#im2 = ax2.imshow(network_image)
# ax3 = fig.add_subplot(122)
# blend_img = 0.2
# blend_data= 1-blend_img
# network_blend_image = Image.fromarray(network_image*blend_data)
# network_blend_image = np.array(network_blend_image.resize(dataset_image.shape[:-1]))
# network_blend_image = cmap(network_blend_image)[:,:,:-1]
# blended_image = dataset_image/255*blend_img+blend_data*network_blend_image
# im3 = ax3.imshow(placeholder)

video_filename = "/home/mazza/gitlab/noise_img_v2/dataset/240x240/M2U00008.MPG"
cap = cv2.VideoCapture(video_filename) #Change for video generato?
# cap.set(cv2.CAP_PROP_POS_FRAMES,0)
# save_fps = cap.get(cv2.CAP_PROP_FPS)
# print("save_fps=",save_fps)
# img_old = np.zeros((15,15))


from .vis_anim import Visualization_animation

anim = Visualization_animation(cap=cap,model=intermediate_model,weights=weights,
                                training_mean=training_mean,
                                training_std=training_std,
                                first_frame = 10000,
                                n_frames=1000,
                                save_name="demo.mp4",
                                save=False,
                                blend_img=0.2)
anim.animation()