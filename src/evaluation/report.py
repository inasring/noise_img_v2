import os
import subprocess
from decimal import Decimal
import pandas as pd
import matplotlib
#remove this to display plot
# matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import collections
import uuid
import re
from json_utils import json_utils as jutils
from locations import directories as d
from dir_utils import dir_utils as du
##Jogar geração da tabela latex numa função
##Table of experiment details
#  Experiment info
#    # Experiment uuid
#    # Experiment name
#  Dataset info
#    #  videos used in training
#    #  samples from each video in training
#    #  videos used in validation
#    #  samples from each video in validation
#    #  subsampling in time for each video
#    Training with net_1 and config_1
#      #  Training info
#        #  Network name
#        #  learning rate schedule
#        #  lowest training error
#        #  lowest validation error
#        #  number training epochs
#      #  Plots
#        #  Training/Validation curves
#        #  Inference curves; training and validation difference by color
#    Training with net_2 and config_2
#    ...
def format_list(l,format_code):
    ret = []
    for i in l:
        string = format_code.format(i)
        ret.append(float(string))
    return ret

def figures(loss,val_loss,
            audio_dataset,predictions_val,
            predictions_training,cut,suptitle):
    epochs_range = range(len(loss))
    fig = plt.figure(figsize=(15,5))
    fig.suptitle(suptitle)
    ax1  = fig.add_subplot(1,2,1)
    ax1.plot(epochs_range,loss,color='b',label="training")
    ax1.plot(epochs_range,val_loss,color='r',label="validation")
    ax1.set_xlabel("Epoch")
    ax1.legend()
    ax2  = fig.add_subplot(1,2,2)
    audio_range = range(len(audio_dataset))
    ax2.plot(audio_range,audio_dataset,color='k',label="target")
    ax2.plot(audio_range[:cut],predictions_training,color='b',label="Training prediction")
    ax2.plot(audio_range[cut:],predictions_val,color='r',label="Validation prediction")
    ax2.set_xlabel("Frame")
    ax2.set_ylabel("Noise intensity")
    ax2.legend()
    return fig



def make_name(optimizer,network,string_date,model_unique_name):
    name = "noiseimg_"+optimizer+\
            "_"+model_unique_name+"_"+\
            network+"_"+string_date+".hdf5"
    return name

def tex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
    }
    regex = re.compile('|'.join(re.escape(key) for key in sorted(conv.keys(), key = lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)
def encapsulate(text,before,after,endline="\n",tab="\t"):
    ret  = before+endline
    ret  +=text+endline
    ret  +=after+endline
    return ret
def tabular_config(config):
    ret = "{"+config+"}"
    return ret

def to_multi_index(key,l):
    key_l = [key]*len(l)
    return [np.array(key_l),np.array(l)]

def filename_last_dir(filename):
    last_dir = os.path.split(os.path.dirname(filename))[-1]
    filename_base = os.path.basename(filename)
    ret = os.path.join(last_dir,filename_base)
    return ret

def to_row_str(l,n_cols):
    ret = ''
    n_loop_string = len(l)
    add_spaces = n_cols-len(l)
    if n_cols<len(l):
        n_loop_string = n_cols
        add_spaces    = 0
    for idx in range(n_loop_string):
        ret+=l[idx]
        if idx<(n_loop_string-1):
            ret+=' & '
    ret+=' & '*add_spaces
    ret+='\\\\'
    return ret

def to_figure(figure_name,directory=''):
    before_graphics = "\includegraphics[width=\linewidth]{"
    after_graphics  = "}"
    before_figure   = "\\begin{figure}[h]\n"
    after_figure   = "\\end{figure}\n"
    fname_full = os.path.join(os.sep,directory,figure_name)
    fname_full = os.path.normpath(fname_full)
    ret = encapsulate(fname_full,before_graphics,after_graphics,endline='')
    ret = encapsulate(ret,before_figure,after_figure)
    return ret

def report_tables_figures(experiment_data_filename,
                    summary_rename,
                    summary_columns,
                    figure_dir=d.latex_test_figures,
                    max_loss=1e3,
                    save_fig=True):
    #Returns dict of pandas tables and figure names and
    # data for latex generation
    max_loss_str = "gt"+"{:.0E}".format(Decimal(max_loss))
    report_dict = {}
    experiment_data = jutils.load_json(experiment_data_filename)
    print(experiment_data.keys())
    training_data_list  = experiment_data["training_config_list"]
    network_config_list = experiment_data["network_config_list"]
    dataset_dic         = experiment_data["dataset_dic"]
    targets             = experiment_data["targets"]
    prediction_list     = experiment_data["prediction_list"]
    report_dict["experiment_log_name"] = experiment_data["experiment_log_name"]
    report_dict["message"] = experiment_data["message"]
    audioDataset = np.hstack((targets["training"],targets["validation"]))
    print(dataset_dic)
    train_id = training_data_list[0]["train_id"]
    string_date = training_data_list[0]["string_date"]
    description_elements    = ["\\textbf{Experimento}",
                            string_date,
                            # tex_escape(training_data_list[0]["experiment_name"]),
                            train_id]
    description = {key:[value] for value,key in zip(description_elements,
                                                range(len(description_elements)))}
    print(description)
    experiment_description = pd.DataFrame.from_dict(description,dtype=object)
    report_dict["experiment_description"]=experiment_description
    report_dict["training_samples"] = dataset_dic["cut"]
    ####Tex
    dataset_descr_dir = collections.OrderedDict()
    dataset_descr_dir["Videos"] = [tex_escape(filename_last_dir(i)) \
                                    for i in dataset_dic["videoFiles"]]
    dataset_descr_dir["IMG/video"] = dataset_dic["n_video_samples"]
    dataset_descr_dir["subsample"] = dataset_dic["subsampleTime"]
    dataset_descr_dir["T\_B"] = [str(i) for i in dataset_dic["timeBackward"]]
    dataset_descr_dir["T\_F"] = [str(i) for i in dataset_dic["timeForward"]]

    dataset_descr_pd = pd.DataFrame.from_dict(dataset_descr_dir)
    report_dict["dataset_descr"] = dataset_descr_pd

    summary_df = pd.DataFrame()
    report_dict["network_descr_list"] = []
    report_dict["figure_name_list"]   = []
    #Make a new pandas dataframe with all data
    #Add new column at every new iteration using .transpose()
    summary_df = pd.DataFrame()
    for i,training_config_dic in enumerate(training_data_list):
        network_config_dic = network_config_list[i]
        # model_name = training_config_dic["model_save_name"]
        # model_dir  = training_config_dic["model_save_path"]
        network_descr_dic  = collections.OrderedDict()
        network_descr_dic["File"] = [training_config_dic["model_save_name"]]
        network_descr_dic["Network"] = [network_config_dic["network"]]
        network_descr_dic["Optimizer"] =[tex_escape(training_config_dic["optimizer"])]

        min_val_argmin   = np.argmin(training_config_dic["val_loss"])
        minimum_val_loss = training_config_dic["val_loss"][min_val_argmin]
        minimum_val_loss_str = "{:2.3f}".format(minimum_val_loss)
        if minimum_val_loss>max_loss:
            network_descr_dic["minimum_val_loss"]=max_loss_str
        else:
            network_descr_dic["minimum_val_loss"]=minimum_val_loss_str
        minimum_loss = training_config_dic["loss"][min_val_argmin]
        minimum_loss_str = "{:2.3f}".format(minimum_loss)
        if minimum_loss>max_loss:
            network_descr_dic["minimum_loss"]=max_loss_str
        else:
            network_descr_dic["minimum_loss"]=minimum_loss_str


        network_descr_dic["Global Average Pooling"]=network_config_dic["global_average_pooling"]
        network_descr_dic["Train FC only"] = network_config_dic["train_fc_only"]
        network_descr_dic["FC"] = network_config_dic["hidden_dense"]
        nparams = network_config_dic["param_count"]
        str_nparams = "{:.0E}".format(Decimal(nparams))
        network_descr_dic["params"] =str_nparams
        network_descr_dic["epochs"] = training_config_dic["epochs"]
        lr_list_formatted = format_list(training_config_dic["lr_list"],"{:.6f}")
        # network_descr_dic["lr_list"]= str(training_config_dic["lr_list"])
        network_descr_dic["lr_list"]= str(lr_list_formatted)
        network_descr_dic["epoch_schedule"]= str(training_config_dic["epoch_schedule"])
        try:
            if training_config_dic["v_mae"]>max_loss:
                network_descr_dic["v_MAE"] = max_loss_str
            else:
                network_descr_dic["v_MAE"]= "{:2.3f}".format(training_config_dic["v_mae"])
            if training_config_dic["v_mse"]>max_loss:
                network_descr_dic["v_MSE"] = max_loss_str
            else:
                network_descr_dic["v_MSE"]= "{:2.3f}".format(training_config_dic["v_mse"])
            if training_config_dic["t_mae"]>max_loss:
                network_descr_dic["t_MAE"] = max_loss_str
            else:
                network_descr_dic["t_MAE"]= "{:2.3f}".format(training_config_dic["t_mae"])
            if training_config_dic["t_mse"]>max_loss:
                network_descr_dic["t_MSE"] = max_loss_str
            else:
                network_descr_dic["t_MSE"]= "{:2.3f}".format(training_config_dic["t_mse"])
        except KeyError:
            print("Key error in report: possibly running older json?")
        network_descr_pd  = pd.DataFrame.from_dict(network_descr_dic)
        # print(network_descr_pd.transpose())
        report_dict["network_descr_list"].append(network_descr_pd)
        summary_df=summary_df.append(network_descr_pd,ignore_index=True)

        ##Figures
        val_loss = training_config_dic['val_loss']
        loss = training_config_dic['loss']
        # epochs_range = range(training_config_dic["epochs"])
        predictions_val = prediction_list[i]["validation"]
        predictions_training = prediction_list[i]["training"]
        fig = figures(loss,val_loss,
                    audioDataset,predictions_val,
                    predictions_training,dataset_dic["cut"],
                    training_config_dic['model_save_name'])
        #Erro
        # Sobrescrevendo todas as figuras com o mesmo train_id
        # Proximas figuras terão mesmo train_id e irão sobrescrever as anteriores
        # Solução: novos nomes únicos
        # unique_name = str(uuid.uuid4())
        figname = "trainPredictCurves_"+train_id+"_"+training_config_dic["model_id"]+".eps"
        # figname = "trainPredictCurves_"+train_id+"_"+unique_name+".eps"
        report_dict["figure_name_list"].append(figname)

        if save_fig:
            du.make_dir(figure_dir)
            fig.savefig(os.path.join(os.sep,figure_dir,figname),
                         bbox_inches = 'tight',pad_inches = 0)
    summary_df = summary_df[summary_columns]
    summary_df.rename(columns=summary_rename,inplace=True)
    report_dict["summary"] = summary_df
    print("summary:\n",summary_df)

    return report_dict

def report_latex(report_tables,
                figure_dir,
                latex_dir,
                save_name,
                save_tex=True,
                add_summary=True):
    experiment_description_tex = report_tables["experiment_description"].to_latex(escape=False,
                                                                index=None,
                                                                header=False)
    experiment_description_tex = encapsulate(experiment_description_tex,
                                    before="{\n\centering",after="}")
    experiment_log_name ="\\begin{tabularx}{\\textwidth}{lX}\n"\
                        +"Log: & "+str(tex_escape(report_tables["experiment_log_name"]))+" \\\\ \n"\
                        +"\\end{tabularx}\n"
    message         = "\\begin{tabularx}{\\textwidth}{lX}\n"\
                        +"Message: & "+str(tex_escape(report_tables["message"]))+" \\\\ \n"\
                        +"\\end{tabularx}\n"
    training_samples = "\\begin{tabular}{ll}\n"\
                        +"Training Samples & "+str(report_tables["training_samples"])+" \\\\ \n"\
                        +"\\end{tabular}\n"
    dataset_descr = report_tables["dataset_descr"]
    # network_descr_tex_list = report_tables["network_descr_list"]
    # figure_tex_list = report_tables["figure_name_list"]
    summary_df_str = report_tables["summary"].to_latex(index=True)
    summary_df_str = encapsulate(summary_df_str,"{\centering\n","\n}")
    dataset_descr_tex= dataset_descr.to_latex(escape=False,index=None)
    dataset_descr_tex = encapsulate(dataset_descr_tex,
                                    before="{\n\centering",after="}")
    latex = ''
    latex+=experiment_description_tex
    latex+=experiment_log_name
    latex+=message
    latex+=training_samples
    latex+=dataset_descr_tex
    for i in range(len(report_tables["network_descr_list"])):
        latex+=report_tables["network_descr_list"][i]\
                                        .transpose()\
                                        .to_latex(header=False)
        latex+="\n"
        latex+=to_figure(report_tables["figure_name_list"][i],
                        figure_dir)
        latex+="\n"
    if add_summary:
        latex += summary_df_str
    fname_full    = os.path.join(os.sep,latex_dir,save_name)
    if save_tex:
        du.save_text(fname_full,latex)
    return latex


def main_report(tex_file_list,output_directory):
    report_template =\
    '''\\documentclass{article}
    \\usepackage{booktabs}
    \\usepackage{multirow}
    \\usepackage{graphicx}
    \\usepackage{epstopdf}
    \\usepackage{changepage}
    \\usepackage{tabularx}
    '''
    report_template+='\n'
    report_template+='\\begin{document}'
    for fname in tex_file_list:
        report_template+= '\\input{'
        report_template+=fname
        report_template+= '}\n'
    report_template+='\\end{document}'
    return report_template


def pdflatex(filename,output_directory='.',verbose = True,mock = False,print_command=True):
    mute_option = ''
    if not verbose:
        mute_option = ' &> /dev/null'
    command = 'pdflatex '\
                + '--shell-escape '\
                +'-output-directory '+output_directory+' '\
                +filename+' '\
                +mute_option
    if not mock:
        subprocess.call(command,shell=True)
    if print_command:
        print(command)
