import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegWriter
import numpy as np
import time

class DisplayWindow(object):
    """
    display_data_list is a list of lists.
    [[first_axis_graph_1,first_axis_graph_2],[second_axis_graph_1,second_axis_graph_2]]
    display_data_labels is a list of lists.
    [[first_axis_label_1,first_axis_label_2],[second_axis_label_1,second_axis_label_2]]
    """
    def __init__(self,
                 video_tensor,
                 display_data_list,
                 display_data_labels=None,
                 firstShownFrame=0,
                 maximumDrawnPoints=100,
                 saveFPS         = 30,
                 saveVideo       = False,
                 display_data_colors= None,
                 saveName        = "demo.mp4"):
                #  show            = True):
        ##Receive generator for image instead of video_tensor?
        self.video_tensor      = video_tensor
        self.display_data_list   = display_data_list
        self.display_data_labels = display_data_labels
        self.display_data_colors = display_data_colors
        self.nrows               = len(display_data_list)
        self.video_cols          = 2
        self.data_cols           = 2
        self.firstShownFrame     = firstShownFrame
        self.ncols               = self.video_cols+self.data_cols
        self.drawing             = True
        self.dummy_list          = list()
        self.fig                 = plt.figure(1,figsize=(13,7))
        self.saveFPS             = saveFPS
        self.saveVideo           = saveVideo
        self.saveName            = saveName
        self.writer              = None
        self.stop                = "stop"
        # self.show                = show
        if self.saveVideo:
            self.writer = FFMpegWriter(fps=self.saveFPS)
        plt.ion()  ##Global??
        # if self.show:
        #     # plt.show() ##Global??
        #     pass
        self.grid                = plt.GridSpec(self.nrows, self.ncols, hspace=0.2, wspace=0.2)
        self.imgAxis             = self.fig.add_subplot(self.grid[:,:self.video_cols])
        self.iterCounter         = 0
        self.currentFrame        = self.firstShownFrame
        self.plotBeginFrame      = self.firstShownFrame
        self.plotRangeBegin      = self.currentFrame
        self.plotRangeEnd        = self.currentFrame
        self.maximumDrawnPoints  = maximumDrawnPoints
        #Read first frame for shape spec
        self.im                  = self.imgAxis.imshow(self.video_tensor[0]/255.)
        #Set first frame
        self.data_axis_list      = []
        self.data_lines_list     = []  #List of lists with lines


        #Setup axis grids and data lines
        for data_axis in range(self.nrows):
            #Implies one row per axis
            self.data_axis_list.append(self.fig.add_subplot(self.grid[data_axis,self.video_cols:]))
            self.data_lines_list.append([])
            maxs = []
            mins = []
            for data_line in range(len(self.display_data_list[data_axis])):
                # print("len(self.display_data_list[data_axis])=",len(self.display_data_list[data_axis]))
                if self.display_data_colors is not None:
                    color = self.display_data_colors[data_axis][data_line]
                else:
                    color = None
                if self.display_data_labels is not None:
                    labels= self.display_data_labels[data_axis][data_line]
                else:
                    labels=None
                plot_kwargs = {"color":color,"label":labels}
                    # line, = self.data_axis_list[data_axis].plot(self.dummy_list,
                    #                                         self.dummy_list,
                    #                                         label=self.display_data_labels[data_axis][data_line],
                    #                                         color=self.display_data_colors[data_axis][data_line])
                line, = self.data_axis_list[data_axis].plot(self.dummy_list,
                                                            self.dummy_list,
                                                            **plot_kwargs)
                maxs.append(max(self.display_data_list[data_axis][data_line]))
                mins.append(min(self.display_data_list[data_axis][data_line]))

                self.data_lines_list[data_axis].append(line)
            self.data_axis_list[data_axis].set_ylim(ymin=min(mins),ymax=max(maxs))
            if display_data_labels is not None:
                self.data_axis_list[data_axis].legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
                            ncol=3, fancybox=True, shadow=True)
        def quit_figure(event):
            if event.key == 'q':
                plt.close(event.canvas.figure)
                self.drawing=False
        def handle_close(event):
            self.drawing = False
        self.quit_figure  = quit_figure
        self.handle_close = handle_close
        self.fig.canvas.mpl_connect('key_press_event', self.quit_figure)
        self.fig.canvas.mpl_connect('close_event', self.handle_close)

    def run_step(self):
        #To remove self.itercounter == 0 checking -> Add a first loop setup to find frame size
        start = time.time()
        self.plotRangeEnd+=1
        if (self.plotRangeEnd-self.plotRangeBegin)>self.maximumDrawnPoints:
            self.plotRangeBegin+=1
            self.plotBeginFrame+=1
        if self.currentFrame >= len(self.video_tensor):
            return self.stop
        frame = self.video_tensor[self.currentFrame]/255.
        self.im.set_data(frame)
        frameN = np.arange(self.display_data_list[0][0]\
                                [self.plotRangeBegin:self.plotRangeEnd].shape[0])+self.plotBeginFrame
        for data_axis in range(len(self.display_data_list)):
            for data_line in range(len(self.display_data_list[data_axis])):
                self.data_lines_list[data_axis]\
                                    [data_line].set_data(frameN,
                                                        self.display_data_list[data_axis][data_line]\
                                                        [self.plotRangeBegin:self.plotRangeEnd])
            self.data_axis_list[data_axis].relim()
            self.data_axis_list[data_axis].autoscale_view(tight=None, scalex=True)
        plt.draw()
        # end = time.time()
        self.currentFrame+=1
        self.iterCounter+=1
        plt.pause(0.0001)
        if not self.drawing:
            # break
            return self.stop
        loopEnd = time.time()
        if self.iterCounter%40 == 0:
            print("currentFrame=",self.currentFrame)
            looptime=loopEnd-start
            print("looptime=",looptime)
            # framRate = 1/looptime
            # print("frameRate=",framRate)
    def main_loop(self):
        while self.currentFrame < len(self.video_tensor):
            ret = self.run_step()
            if self.saveVideo:
                self.writer.grab_frame()
            if ret == self.stop:
                break
    def run(self):
        if self.saveVideo:
            with self.writer.saving(self.fig, self.saveName, 100):
                self.main_loop()
        else:
            self.main_loop()
