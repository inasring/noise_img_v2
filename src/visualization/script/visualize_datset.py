from visualization.lib.display_window import DisplayWindow
from locations import directories
import numpy as np
import json

# audio_name    = directories.numpyDataset+"audio_experiment_videos_122018-10-15.11:05:34.npy"
# metadata_name = directories.numpyDataset+"image_experiment_videos_122018-10-15.11:05:34.json"
# video_name    = directories.numpyDataset+"image_experiment_videos_122018-10-15.11:05:34.npy"
dataset = "experiment_videos_1638_2018-10-15.15:58:54"

audio_name    = directories.numpyDataset+"audio_"+dataset+'.npy'
video_name    = directories.numpyDataset+"image_"+dataset+'.npy'
metadata_name = directories.numpyDataset+"image_"+dataset+'.json'
handler  = open(metadata_name,'r')
metadata = json.load(handler)
print (metadata["n_video_samples"])
print (np.cumsum(metadata["n_video_samples"]))
print (metadata["n_video_samples"][::-1])
print (np.cumsum(metadata["n_video_samples"][::-1]))
video = np.load(video_name,mmap_mode='r')
audio = np.load(audio_name)
print("videoLength=",len(video))
print("videoshape=",video.shape)
print("audioLength=",len(audio))
# exit()
display_data_list = [[audio]]
firstShownFrame = 1000
np.set_printoptions(threshold=np.inf)
# print (audio[np.newaxis].T[5:,:])

window = DisplayWindow(video,
                        display_data_list,
                        display_data_labels=None,
                        firstShownFrame=firstShownFrame,
                        maximumDrawnPoints=100,
                        saveFPS         = 30,
                        saveVideo       = False,
                        display_data_colors= None,
                        saveName        = "demo.mp4")
window.run()
