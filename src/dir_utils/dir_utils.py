import os
def make_dir(directory):
    try:
        os.makedirs(directory)
    except FileExistsError:
        pass

def filenameNoExtension(filename,separator='.'):
    return separator.join(filename.split(separator)[:-1])

def filenameNoPath(filename):
    return filename.split(os.path.sep)[-1]

def save_text(filename,text):
    with open(filename, "w") as file_handler:
        file_handler.write(text)

if __name__ == "__main__":
    fname_1 = "test1.mp3"
    fname_2 = "test2-mp3"
    fname_3 = "test3@mp3"
    print (filenameNoExtension(fname_1,separator='.'))
    print (filenameNoExtension(fname_2,separator='-'))
    print (filenameNoExtension(fname_3,separator='@'))
